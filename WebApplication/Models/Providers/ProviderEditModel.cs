﻿using System.ComponentModel.DataAnnotations;
using WebApplication.Models.Shared;

namespace WebApplication.Models.Providers
{
    /// <summary> Модель данных для изменения информации о поставщике </summary>
    public class ProviderEditModel
    {
        /// <summary> Номер поставщика</summary>
        public int Id { get; set; }

        /// <summary> Старое имя поставщика </summary>
        public string OldName { get; set; }

        /// <summary> Старый комментарий о поставщике </summary>
        public string OldComment { get; set; }

        /// <summary> Новое имя поставщика </summary>
        [Required(ErrorMessage = "Не введено новое имя поставщика")]
        public string NewName { get; set; }

        /// <summary> Новый комментарий о поставщике </summary>
        public string NewComment { get; set; }

        /// <summary> Идентификатор строки в БД </summary>
        public DbRecordIdentity RecordIdentity { get; set; }
    }
}