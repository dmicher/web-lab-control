﻿using System.Collections.Generic;

namespace WebApplication.Models.Providers
{
    /// <summary> Модель данных для посмотра и фильтрации поставщиков реактивов </summary>
    public class ProviderViewFilterModel
    {
        /// <summary> Выбранные поставщики реактивов </summary>
        public IEnumerable<ProviderIndexModel> Providers { get; set; }

        /// <summary> Фильтр поставщика по названию </summary>
        public string FilterName { get; set; }

        /// <summary> Фильтр поставщика по содержанию в комментарии </summary>
        public string FilterComment { get; set; }
    }
}