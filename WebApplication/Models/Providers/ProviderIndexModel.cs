﻿namespace WebApplication.Models.Providers
{
    /// <summary> Краткая информация о поставщике для просмотра </summary>
    public class ProviderIndexModel
    {
        /// <summary> Номер поставщика </summary>
        public int Id { get; set; }
        
        /// <summary> Имя организации </summary>
        public string Name { get; set; }

        /// <summary> Комментарий </summary>
        public string Comment { get; set; }
    }
}