﻿using System.Collections.Generic;

namespace WebApplication.Models.Specifications
{
    /// <summary> Модель "Фильтр-данные" для просмотра спецификаций </summary>
    public class SpecificationViewFilterModel
    {
        /// <summary> Результаты поиска данных </summary>
        public IEnumerable<SpecificationIndexModel> Specifications { get; set; }

        /// <summary> Фильтр по номеру </summary>
        public string NumberFilter { get; set; }

        /// <summary> Фильтр по названию </summary>
        public string NameFilter { get; set; }

    }
}