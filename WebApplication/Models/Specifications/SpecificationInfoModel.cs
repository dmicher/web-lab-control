﻿using System.ComponentModel.DataAnnotations;
using WebApplication.Models.Shared;

namespace WebApplication.Models.Specifications
{
    /// <summary> Спецификация на реактив или расходный материал </summary>
    public class SpecificationInfoModel
    {
        /// <summary> Номер </summary>
        [Required(ErrorMessage = "Не указан номер спецификации")]
        public string Number { get; set; }

        /// <summary> Название </summary>
        [Required(ErrorMessage = "Не указано название спецификации")]
        public string Name { get; set; }

        /// <summary> Общий идентификатор строки в БД </summary>
        public DbRecordIdentity RecordIdentity { get; set; }
    }
}