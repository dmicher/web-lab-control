﻿using System.ComponentModel.DataAnnotations;
using WebApplication.Models.Shared;

namespace WebApplication.Models.Specifications
{
    /// <summary> Модель данных для изменения спецификации </summary>
    public class SpecificationEditModel
    {
        /// <summary> Значение "Номер" в базе данных </summary>
        public string OldNumber { get; set; }

        /// <summary> Значение "Название" в базе данных </summary>
        public string OldName { get; set; }

        /// <summary> Общий идентификатор записи в базе данных </summary>
        public DbRecordIdentity RecordIdentity { get; set; }

        /// <summary> Новое значение для поля "Номер" </summary>
        [Required(ErrorMessage = "Новый номер не введён")]
        public string NewNumber { get; set; }

        /// <summary> Новое значение для поля "Название" </summary>
        [Required(ErrorMessage = "Новое имя не введено")]
        public string NewName { get; set; }
    }
}