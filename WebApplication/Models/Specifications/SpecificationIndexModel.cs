﻿namespace WebApplication.Models.Specifications
{
    /// <summary> Модель данных для отображения базовой информации о спецификации</summary>
    public class SpecificationIndexModel
    {
        /// <summary> Номер спецификации числом</summary>
        public int Number { get; set; }

        /// <summary> Номер спецификации в строке </summary>
        public string NumString 
        {
            get => Number.ToString("D3");
            set => Number = int.TryParse(value?.Trim(), out var i) ? i : -1;
        }

        /// <summary> Название спецификации </summary>
        public string Name { get; set; }
    }
}