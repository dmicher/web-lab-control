﻿using System;
using System.Collections.Generic;

namespace WebApplication.Models.ReactivesRemainings
{
    public class RemainingsIndexResultRowModel : IEquatable<RemainingsIndexResultRowModel>
    {
        /// <summary> Номер спецификации </summary>
        public int? SpecificationNumber { get; set; }

        /// <summary> Название спецификации </summary>
        public string SpecificationName { get; set; }

        /// <summary> Структура "Остаток" </summary>
        public struct RemainingCount
        {
            /// <summary> Числовое значение остатка </summary>
            public decimal? Value { get; set; }

            /// <summary> Единица измерения </summary>
            public string MesUnit { get; set; }
        }

        /// <summary> Остатки указанной спецификации по единицам измерения </summary>
        public IEnumerable<RemainingCount> Counts { get; set; }

        public bool Equals(RemainingsIndexResultRowModel other)
            => SpecificationNumber == other?.SpecificationNumber;

        public override bool Equals(object obj)
            => obj is RemainingsIndexResultRowModel other && Equals(other);

        public override int GetHashCode() 
            => HashCode.Combine(SpecificationNumber);
    }
}