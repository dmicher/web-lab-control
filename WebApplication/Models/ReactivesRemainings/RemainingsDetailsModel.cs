﻿using System.Collections.Generic;

namespace WebApplication.Models.ReactivesRemainings
{
    /// <summary> Модель данных для детализации по остаткам </summary>
    public class RemainingsDetailsModel
    {
        /// <summary> Номер специяфикации </summary>
        public int SpecificationId { get; set; }

        /// <summary> Название спецификации </summary>
        public string SpecificationName { get; set; }

        /// <summary> Структура данных, описывающая поставку</summary>
        public struct ArrivalInfo
        {
            /// <summary> Номер серии ЛКК</summary>
            public string Lkksn { get; set; }

            /// <summary> Количество статков в серии</summary>
            public decimal? Count { get; set; }

            /// <summary> Единицы измерения в серии</summary>
            public string MesUnits { get; set; }

            /// <summary> Место хранения поставки </summary>
            public string StoredAt { get; set; }

            /// <summary> Статус списания</summary>
            public bool IsWrittenOff { get; set; }
        }

        /// <summary> Перечисление поставок по указанной спецификации</summary>
        public IEnumerable<ArrivalInfo> Arrivals { get; set; }
    }
}