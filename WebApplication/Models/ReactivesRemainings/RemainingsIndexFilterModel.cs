﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.ReactivesRemainings
{
    /// <summary> Модель данных для поиска и представления результатов по остаткам </summary>
    public class RemainingsIndexFilterModel
    {
        /// <summary> Строки обнаруженных остатков для вывода в качестве результатов поиска </summary>
        public IEnumerable<RemainingsIndexResultRowModel> Rows { get; set; }

        /// <summary> Перечисление атрибутов в базе данных: код-название </summary>
        public Dictionary<char, string> Attributes { get; set; }

        /// <summary> выбранные атрибуты для одбавление по правилу "только с атрибутом" </summary>
        public Dictionary<char, bool> SelectedAttributesOn { get; set; }

        /// <summary> Выбранные атрибуты для добавления по правилу "только без атрибута" </summary>
        public Dictionary<char, bool> SelectedAttributesOff { get; set; }

        /// <summary> Спецификации базы данных </summary>
        public IEnumerable<SelectListItem> Specifications { get; set; }

        /// <summary> Фильтр по выбранной спейификации </summary>
        public int? SelectedSpecificationFilter { get; set; }

        /// <summary> Фильтр по введённому значению в имени спецификации </summary>
        public string InsertedSpecificationFilter { get; set; }

        /// <summary> Пункты выбора "списан ли" </summary>
        public IEnumerable<SelectListItem> IsWrittenOffFilterItems => new List<SelectListItem>
        {
            new SelectListItem("не списанные", "no", true),
            new SelectListItem("списанные", "yes", false),
            new SelectListItem("все", "any", false)
        };

        /// <summary> Выбранный пункт из вариантов "списан ли" </summary>
        public string IsWrittenOffSelectedFilter { get; set; } = "no";

        /// <summary> Ограничение по количеству выводимых данных</summary>
        public int Limit { get; set; } = 25;
    }
}