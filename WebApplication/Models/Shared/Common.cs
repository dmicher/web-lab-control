﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.Shared
{
    /// <summary> Общие элементы для моделей</summary>
    public static class Common
    {
        /// <summary> Месяцы года </summary>
        public static IEnumerable<SelectListItem> Months => new List<SelectListItem>
        {
            new SelectListItem("", "0", true),
            new SelectListItem("январь", "1"),
            new SelectListItem("февраль", "2"),
            new SelectListItem("март", "3"),
            new SelectListItem("апрель", "4"),
            new SelectListItem("май", "5"),
            new SelectListItem("июнь", "6"),
            new SelectListItem("июль", "7"),
            new SelectListItem("август", "8"),
            new SelectListItem("сентябрь", "9"),
            new SelectListItem("октябрь", "10"),
            new SelectListItem("ноябрь", "11"),
            new SelectListItem("декабрь", "12")
        };

    }
}