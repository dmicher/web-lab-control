﻿using System;
using System.Data;
using WebApplication.Tools.Project;

namespace WebApplication.Models.Shared
{
    /// <summary> Общая модель данных для записей в БД, предусматривающих идентификацию изменений </summary>
    public class DbRecordIdentity
    {
        /// <summary> Беспараметрический конструктор </summary>
        public DbRecordIdentity() { }

        /// <summary> Конструктор из строки базы данных </summary>
        /// <param name="row"><see cref="DataRow"/> строгого формата</param>
        /// <exception cref="ArgumentException">Если строка данных не соответствует формату</exception>
        public DbRecordIdentity(DataRow row)
        {
            if (!row.Table.Columns.Contains("Id") && !row.Table.Columns.Contains("code"))
                throw new ArgumentException("Строка данных не содержит номера или кода строки");
            IdCode = row.GetValue(row.Table.Columns.Contains("Id") 
                ? "Id" 
                : "code");

            if (!row.Table.Columns.Contains("created_at_date")
                || !row.Table.Columns.Contains("created_by")
                || !row.Table.Columns.Contains("changed_at_date")
                || !row.Table.Columns.Contains("changed_by"))
                throw new ArgumentException("Строка данных не содержит одного или нескольких обязательных столбцов");
            CreatedAtDate = DateTime.TryParse(row.GetValue("created_at_date"), out var date)
                ? (DateTime?) date
                : null;
            CreatedBy = row.GetValue("created_by");
            ChangedAtDate = DateTime.TryParse(row.GetValue("changed_at_date"), out date)
                ? (DateTime?) date
                : null;
            ChangedBy = row.GetValue("changed_by");
        }

        /// <summary> Номер или код записи в строковом представлении </summary>
        public string IdCode { get; set; }

        /// <summary> Дата создания записи в БД </summary>
        public DateTime? CreatedAtDate { get; set; }

        /// <summary> Пользователь, создавший запись в БД (если есть) </summary>
        public string CreatedBy { get; set; }

        /// <summary> Возвращает стандартную "Создано" по формату "Кем, когда"</summary>
        public string CreatedString
        {
            get
            {
                if (string.IsNullOrWhiteSpace(CreatedBy) && CreatedAtDate == null)
                    return null;
                return $"{CreatedBy}, {CreatedAtDate?.ToDateTimeString()}";
            }
        }

        /// <summary> Дата последнего изменения записи в БД </summary>
        public DateTime? ChangedAtDate { get; set; }

        /// <summary> Пользователь, внёсший последнее изменение в запись в БД (если есть) </summary>
        public string ChangedBy { get; set; }
        
        /// <summary> Возвращает стандартную "Изменено" по формату "Кем, когда"</summary>
        public string ChangedString
        {
            get
            {
                if (string.IsNullOrWhiteSpace(ChangedBy) && ChangedAtDate == null)
                    return null;
                return $"{ChangedBy}, {ChangedAtDate?.ToDateTimeString()}";
            }
        }


    }
}