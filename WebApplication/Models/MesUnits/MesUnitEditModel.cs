﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.MesUnits
{
    /// <summary> Модель данных для изменения единицы измерения </summary>
    public class MesUnitEditModel
    {
        /// <summary>Старое имя</summary>
        public string OldName { get; set; }

        /// <summary>Старое сокращение</summary>
        public string OldShort { get; set; }

        /// <summary>Старая позиция </summary>
        public int OldPosition { get; set; }

        /// <summary>Новое имя</summary>
        [Required(ErrorMessage = "Новое имя единицы измерения не заполнено")]
        public string NewName { get; set; }

        /// <summary>Новое сокращение</summary>
        [Required(ErrorMessage = "Новое сокращение единицы измерения не заполнено")]
        [MaxLength(5, ErrorMessage = "Сокращение не может превышать 5 символов")]
        public string NewShort { get; set; }

        /// <summary>Новая позиция</summary>
        public int NewPosition { get; set; }
    }
}