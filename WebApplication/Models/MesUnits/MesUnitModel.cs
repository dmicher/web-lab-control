﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.MesUnits
{
    /// <summary> Модель данных единицы измерения </summary>
    public class MesUnitModel
    {
        /// <summary> Полное имя </summary>
        [Required(ErrorMessage = "Имя единицы измерения не введено")]
        public string Name { get; set; }

        /// <summary> Сокращение </summary>
        [Required(ErrorMessage = "Сокращение для единицы измерения не введено")]
        [MaxLength(5, ErrorMessage = "Введённое значение слишком длинное для поля \"Сокращение\". Введите не более 5 символов.")]
        public string Short { get; set; }

        /// <summary> Позиция в выпадающем списке при выводе </summary>
        public int Position { get; set; }

        /// <summary> Доступно ли удаление для единицы измерения? </summary>
        public bool CanDelete => UseCount == 0;

        /// <summary> Количество использований единицы измерения на текущий момент </summary>
        public int UseCount { get; set; }

    }
}