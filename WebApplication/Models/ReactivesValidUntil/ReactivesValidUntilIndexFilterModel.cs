﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.ReactivesValidUntil
{
    /// <summary> Модель данных для просмотра и фильтрации данных в сроке годности реактивов </summary>
    public class ReactivesValidUntilIndexFilterModel
    {
        /// <summary> Варианты фильтра по списания</summary>
        public IEnumerable<SelectListItem> States => new SelectListItem[]
        {
            new SelectListItem("не списан", "no", true), 
            new SelectListItem("списан", "yes", false), 
            new SelectListItem("все", "all", false), 
        };

        /// <summary> Выбранный фильтр по статусу</summary>
        public string StatesFilter { get; set; } = "no";

        /// <summary> Количество дней вперёд для просмотра данных </summary>
        public int? DaysForward { get; set; } = 40;

        /// <summary> Результаты просмотра </summary>
        public IEnumerable<ReactivesValidUntilResultRowModel> Rows { get; set; }
    }
}