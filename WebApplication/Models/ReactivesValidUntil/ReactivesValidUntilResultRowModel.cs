﻿using System;

namespace WebApplication.Models.ReactivesValidUntil
{
    /// <summary> Строка данных для посмотра срока годности </summary>
    public class ReactivesValidUntilResultRowModel
    {
        /// <summary> Номер спецификации</summary>
        public int? SpecificationId { get; set; }

        /// <summary> Название спецификации </summary>
        public string SpecificationName { get; set; }

        /// <summary> Поставка - серия ЛКК</summary>
        public string Lkksn { get; set; }

        /// <summary> Текущие остатки в поставке </summary>
        public decimal? Count { get; set; }

        /// <summary> Учётные единицы измерения </summary>
        public string MesUnits { get; set; }

        /// <summary> Срок годности</summary>
        public DateTime? ValidUntil { get; set; }
    }
}