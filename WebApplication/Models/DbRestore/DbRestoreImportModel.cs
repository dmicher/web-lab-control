﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель импортируемых данных из старой программы </summary>
    public class DbRestoreImportModel
    {
        /// <summary> Пароль технического пользователя </summary>
        [Required(ErrorMessage = "Пароль технического пользователя не введён.")]
        public string RootPassword { get; set; }

        /// <summary> Импортируемый файл </summary>
        [Required(ErrorMessage = "Загружаемый файл не выбран")]
        public IFormFile UsersFile { get; set; }
    }
}