﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель данных для создания резервной копии </summary>
    public class DbRestoreCreateModel
    {
        /// <summary> Пароль конревого пользователя </summary>
        [Required(ErrorMessage = "Пароль технического пользователя не заполнен")]
        public string RootPassword { get; set; }

        /// <summary> (опционально) Комментарий к резервной копии </summary>
        public string Comment { get; set; }
    }
}