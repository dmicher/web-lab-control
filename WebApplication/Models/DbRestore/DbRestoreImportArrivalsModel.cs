﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WebApplication.Tools.Project;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель данных для одной поставки по формату старого Лабконтроль</summary>
    public class DbRestoreImportArrivalsModel
    {
        /// <summary>Строго один конструктор, который требует путь к файлу для считывания</summary>
        /// <param name="path">Путь к файлу данных о поставке старого Лабконтроль</param>
        public DbRestoreImportArrivalsModel(string path)
        {
            var file = new FileInfo(path);
            if (!file.Exists)
                throw new Exception($"Файл [{path}] не существует.");

            var lkksn = file.Name?.Remove(file?.Name?.IndexOf(".csv") ?? 0).Split("_");
            if (lkksn.Length != 3)
                throw new Exception("Неверный формат имени файла");

            Lkksn_Specification = int.TryParse(lkksn[0].Trim(), out var i) ? i : -1;
            if (Lkksn_Specification <= 0)
                throw new Exception("Номер спецификации должен быть числом больше нуля");

            Lkksn_DeliveryNum = int.TryParse(lkksn[1].Trim(), out i) ? i : -1;
            if (Lkksn_DeliveryNum <= 0)
                throw new Exception("Номер доставки должен быть числом больше нуля");

            if (DateTime.TryParse(lkksn[2].Trim(), out var date))
                Lkksn_Date = date;
            else throw new Exception("Дата поставки должен имеет неверный формат");

            Rows = new List<DbRestoreImportArrivalsRowModel>();
            foreach (var line in File.ReadAllLines(file.FullName).Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                var values = line.Split(";");
                if (values.Length != 15)
                    throw new Exception($"Одна из строк данных в файле [{file.Name}] имеет не верное количество значений = {values.Length}: [{line}]" );

                var validForever = string.IsNullOrWhiteSpace(values[11]) | !DateTime.TryParse(values[11], out var validDate);
                Rows.Add(new DbRestoreImportArrivalsRowModel
                {
                    Lkksn_Specification = int.TryParse(values[0], out i) 
                        ? i
                        : throw new Exception($"Номер спецификации в одной из строк данных файла [{file.Name}] не является числом."),
                    Lkksn_DeliveryNumber = int.TryParse(values[1], out i)
                        ? i
                        : throw new Exception($"Номер поставки в одной из строк данных файла [{file.Name}] не является числом."),
                    Lkksn_Date = DateTime.TryParse(values[2], out date)
                        ? date
                        : throw new Exception($"Дата поставки в одной из строк данных файла [{file.Name}] не является датой."),
                    ProviderReactiveName = values[3].Trim(),
                    CurrentCount = values[4].ToDecimal() ?? throw new Exception($"Текущее значение остатков в одной из строк данных файла [{file.Name}] не является числом"),
                    MeasurementUnits = values[5].Trim(),
                    UserName = values[6].Trim(),
                    EditDateTime = DateTime.TryParse(values[7], out date) ? date : throw new Exception($"Дата изменения данных в одной из строк файла [{file.Name}] не является датой."),
                    ProviderName = values[9].Trim(),
                    ProducerReactiveSn = values[10].Trim(),
                    IsValidForever = validForever,
                    ValidUntil = validForever ? null : (DateTime?) validDate ,
                    StoredAt = values[12].Trim(),
                    Comment = values[13].Trim(),
                    IsWrittenOff = values[14].Trim().ToUpper().Contains("FALSE") // списан = false по старой базе
                });
            }

        }

        /// <summary> Номер спецификации </summary>
        public int Lkksn_Specification { get; }

        /// <summary> Номер поставки </summary>
        public int Lkksn_DeliveryNum { get; }

        /// <summary> Дата поставки </summary>
        public DateTime Lkksn_Date { get; }

        /// <summary> Сериф ЛКК </summary>
        public string Lkksn
            => Lkksn_Specification.ToString("D3") + "/" +
               Lkksn_DeliveryNum.ToString("D2") + "/" +
               Lkksn_Date.ToString("ddMMyy");

        /// <summary> Содержимое строк данных </summary>
        public List<DbRestoreImportArrivalsRowModel> Rows { get; }

    }
}