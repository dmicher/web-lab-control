﻿using System;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель данных для редактирования комментария к резервной копии</summary>
    public class DbRestoreReCommentModel
    {
        /// <summary> Номер резервной копии</summary>
        public int Id { get; set; }

        /// <summary> Имя резервной копии </summary>
        public string Name { get; set; }

        /// <summary> Дата создания резервной копии</summary>
        public DateTime CreatedAt { get; set; }

        /// <summary> Текущий комментарий </summary>
        public string OldComment { get; set; }

        /// <summary> Новый комментарий</summary>
        public string NewComment { get; set; }
    }
}