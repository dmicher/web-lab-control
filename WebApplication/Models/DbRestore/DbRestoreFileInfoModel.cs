﻿using System;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель данных для просмотра информации об имеющихся резервных копиях </summary>
    public class DbRestoreFileInfoModel
    {
        /// <summary> Идентификатор записи в базе данных </summary>
        public int Id { get; set; }
        
        /// <summary> Название файла (без пути к нему) </summary>
        public string Name { get; set; }

        /// <summary> Размер файла на жёстком диске </summary>
        public uint Size { get; set;  }

        /// <summary> Дата создания резервной копии </summary>
        public DateTime CreationDateTime { get; set; }

        /// <summary> Комментарий к резервной копии </summary>
        public string Comment { get; set; }

    }
}