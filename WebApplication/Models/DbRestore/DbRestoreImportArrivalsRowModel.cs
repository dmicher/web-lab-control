﻿using System;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель строки в истории изменений данных в старой Лабконтроль</summary>
    public class DbRestoreImportArrivalsRowModel
    {
        /// <summary> Сериф ЛКК </summary>
        public string Lkksn
            => Lkksn_Specification.ToString("D3") + "/" +
               Lkksn_DeliveryNumber.ToString("D2") + "/" +
               Lkksn_Date.ToString("ddMMyy");

        /// <summary> value[0] - серия ЛКК: номер спецификации</summary>
        public int Lkksn_Specification { get; set; }

        /// <summary> value[1] - серия ЛКК: номер поставки</summary>
        public int Lkksn_DeliveryNumber { get; set; }

        /// <summary> value[2] - серия ЛКК: дата поставки</summary>
        public DateTime Lkksn_Date { get; set; }

        /// <summary> value[3] - Название реактива по данным производителя</summary>
        public string ProviderReactiveName { get; set; }

        /// <summary> value[4] - текущие остатки</summary>
        public decimal CurrentCount { get; set; }

        /// <summary> value[5] - учётные единицы измерения</summary>
        public string MeasurementUnits { get; set; }

        /// <summary> value[6] - имя пользователя, добавившуго запись</summary>
        public string UserName { get; set; }

        /// <summary> value[7] - дата добавления записи</summary>
        public DateTime EditDateTime { get; set; }
        
        /// <summary> value[9] - Наименоварие поставщика </summary>
        public string ProviderName { get; set; }

        /// <summary> value[10] - Название реактива по данным производителя </summary>
        public string ProducerReactiveSn { get; set; }

        /// <summary> value[11] - срок годности (не ограничен) </summary>
        public bool IsValidForever { get; set; }

        /// <summary> value[11] - срок годности (ограничен датой) </summary>
        public DateTime? ValidUntil { get; set; }

        /// <summary> value[12] - Место хранения реактива </summary>
        public string StoredAt { get; set; }

        /// <summary> value[13] - комментарий к реактиву</summary>
        public string Comment { get; set; }

        /// <summary> value[14] - статус списания реактива</summary>
        public bool IsWrittenOff { get; set; }
    }
}