﻿using System.Threading;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Простая модель результатов импорта </summary>
    public class DbRestoreImportSingleResultModel
    {
        /// <summary> Заголовок страницы</summary>
        public string Title { get; set; }

        /// <summary> Описание успешного выполнения </summary>
        public string SuccessString { get; set; }
    }
}