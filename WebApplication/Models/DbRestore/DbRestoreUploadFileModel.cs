﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель данных для загрузки резервной копии базы данных на сайт</summary>
    public class DbRestoreUploadFileModel
    {
        /// <summary> Пароль корневого пользователя</summary>
        [Required(ErrorMessage = "Пароль технического пользователя не введён.")]
        public string RootPassword { get; set; }

        /// <summary> Подгружаемый файл резервной копии </summary>
        [Required(ErrorMessage = "Загружаемый файл не выбран")]
        public IFormFile UploadedFile { get; set; }
    }
}