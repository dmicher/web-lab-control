﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель данных для восстановления базы данных из резервной копии</summary>
    public class DbRestoreDoRestoreModel
    {
        /// <summary> Номер резервной копии</summary>
        public int Id { get; set; }

        /// <summary> Имя файла резервной копии</summary>
        public string Name { get; set; }

        /// <summary> Дата создания резервной копии</summary>
        public string CreatedAt { get; set; }

        /// <summary> Комментарий к копии </summary>
        public string Comment { get; set; }

        /// <summary> Пароль от корневого пользователя</summary>
        [Required(ErrorMessage = "Пароль от технического пользователя не введён")]
        public string RootPassword { get; set; }
    }
}