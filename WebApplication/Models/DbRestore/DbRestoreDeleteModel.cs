﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель данных при удалении резервной копии базы данных </summary>
    public class DbRestoreDeleteModel
    {
        /// <summary> Номер резервной копии </summary>
        public int Id { get; set; }

        /// <summary> Имя резервной копии </summary>
        public string Name { get; set; }

        /// <summary> Комментарий к резервной копии </summary>
        public string Comment { get; set; }

        /// <summary> Дата создания резервной коппии </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary> Пароль от корневого пользователя</summary>
        [Required(ErrorMessage = "Пароль от технического пользователя не введён")]
        public string RootPassword { get; set; }
    }
}