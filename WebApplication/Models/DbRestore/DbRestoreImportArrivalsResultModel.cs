﻿using System.Collections.Generic;
using System.Linq;

namespace WebApplication.Models.DbRestore
{
    /// <summary> Модель данных для демонстрации результатов после импорта поставок </summary>
    public class DbRestoreImportArrivalsResultModel
    {
        /// <summary> Поставки, перенесённые с ошибками</summary>
        public IEnumerable<string> WarningArrivals { get; set; }

        /// <summary> Количество поставок, перенесённых с ошибками </summary>
        public int Warnings => WarningArrivals?.Count() ?? 0;

        /// <summary> Количество добавленных единиц измерения </summary>
        public int MesUnitsCount { get; set; }

        /// <summary> Количество добавленных поставщиков </summary>
        public int ProvidersCount { get; set; }

        /// <summary> Количество добавленных поставок </summary>
        public int ArrivalsCount { get; set; }

        /// <summary> Количество добавленных шагов истории </summary>
        public int ChangesCount { get; set; }
    }
}