﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.Reactives
{
    /// <summary> Модель данных для изменения основной информации </summary>
    public class ReactivesChangeModel
    {
        /// <summary>Текущий номер серии ЛКК</summary>
        public string Lkksn { get; set; }
        
        /// <summary> Список существующих поставщиков</summary>
        public IEnumerable<SelectListItem> Providers { get; set; }

        /// <summary> Текущий поставщик </summary>
        public string Provider_Old { get; set; }

        /// <summary> Идентификатор поставщика (старый) </summary>
        public string ProviderId_Old { get; set; }

        /// <summary> Новый выбранный поставщик </summary>
        public string ProviderId_New { get; set; }

        /// <summary> Существующие производители</summary>
        public IEnumerable<SelectListItem> Producers { get; set; }

        /// <summary> Текущий производитель </summary>
        public string Producer_Old { get; set; }

        /// <summary> Номер производителя (старый) </summary>
        public string ProducerId_Old { get; set; }

        /// <summary> Новый производитель </summary>
        public string ProducerId_New { get; set; }

        /// <summary> Старое название реактива </summary>
        public string ReactiveName_Old { get; set; }

        /// <summary> Новое название реактива </summary>
        public string ReactiveName_New { get; set; }

        /// <summary> Старый серийный номер реактива</summary>
        public string ReactiveSn_Old { get; set; }

        /// <summary> Новый серийный номер реактива </summary>
        public string ReactiveSn_New { get; set; }

        /// <summary> Объём и единица измерения на момент ввода исходных данных в учётных единицах </summary>
        public string StartCountString { get; set; }

        /// <summary> Объём и единица измерения на момент ввода исходных данных в единицах производителя</summary>
        public string ProducerCountString { get; set; }

        /// <summary> Остатки по поставкам </summary>
        public string CurrentCountString { get; set; }

        /// <summary> Старая дата для срока годности реактива </summary>
        public DateTime? ValidUntil_Old { get; set; }

        /// <summary> Флаг неограниченного срока годности старый</summary>
        public bool UnlimitedLifeTime_Old { get; set; }

        /// <summary> Новая дата для срока годности реактива </summary>
        public DateTime? ValidUntil_New { get; set; }

        /// <summary> Флаг неограниченного срока годности новый</summary>
        public bool UnlimitedLifeTime_New { get; set; }

        /// <summary> Существующие места хранения </summary>
        public IEnumerable<SelectListItem> Stores { get; set; }

        /// <summary>Место хранения старое</summary>
        public string StoredAt_Old { get; set; }

        /// <summary> Место хранения новое, выбрано из списка</summary>
        public string StoredAt_NewSelected { get; set; }

        /// <summary> Место храениня новое, введено вручную</summary>
        public string StoredAt_NewInserted { get; set; }

        /// <summary> Комментарий, старый </summary>
        public string Comment_Old { get; set; }

        /// <summary> Комментарий, новый </summary>
        public string Comment_New { get; set; }

        /// <summary> Перечисление строк с указанием текущих выбранных атрибутов </summary>
        public IEnumerable<string> Attributes_Old { get; set; }

        /// <summary> Перечисление старых атрибутов в виде строки </summary>
        public string AttributesString_Old { get; set; }

        /// <summary> Словарь выбора новых атрибутов </summary>
        public Dictionary<char, bool> Attributes_New { get; set; }

        /// <summary> Словарь с названиями атрибутов </summary>
        public Dictionary<char, string> AttributesNames { get; set; }
    }
}