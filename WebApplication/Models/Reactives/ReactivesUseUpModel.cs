﻿namespace WebApplication.Models.Reactives
{
    /// <summary> Модель данных для израсходования реактива </summary>
    public class ReactivesUseUpModel
    {
        /// <summary> Серия ЛКК поставки </summary>
        public string Lkksn { get; set; }

        /// <summary> Текущее количество реактива </summary>
        public decimal CurrentCount { get; set; }

        /// <summary> Списан ли реактив к настоящему времени </summary>
        public bool IsAlready_WrittenOff { get; set; }

        /// <summary> Единицы измерения </summary>
        public string MesUnits { get; set; }

        /// <summary> Объём расходования </summary>
        public string ChangeCount { get; set; }

        /// <summary>Флаг списания, если реактив израсходован полностью</summary>
        public bool WriteOff_IfEnds { get; set; }
    }
}