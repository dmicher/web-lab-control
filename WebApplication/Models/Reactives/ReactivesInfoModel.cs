﻿using System;
using System.Collections.Generic;

namespace WebApplication.Models.Reactives
{
    /// <summary> Модель данных для отображения подробной информации о поставке</summary>
    public class ReactivesInfoModel
    {
        /// <summary>Номер серии ЛКК</summary>
        public string Lkksn { get; set; }

        /// <summary> Спецификация </summary>
        public string Specification { get; set; }

        /// <summary> Имя поставщика</summary>
        public string ProviderName { get; set; }

        /// <summary> Имя производителя</summary>
        public string ProducerName { get; set; }

        /// <summary> Имя реактива </summary>
        public string ReactiveName { get; set; }

        /// <summary> Серийный номер реактива </summary>
        public string ReactiveSn { get; set; }

        /// <summary> Объём поставки в исходных единицах</summary>
        public decimal? ProducerCount { get; set; }

        /// <summary> Исходные единицы измерения </summary>
        public string ProducerMesUnit { get; set; }

        /// <summary> Объём поставки в учётных единицах при регистрации поставки </summary>
        public decimal? StartCount { get; set; }

        /// <summary> Текущие остатки в учётных единицах </summary>
        public decimal CurCount { get; set; }

        /// <summary> Текущие учётные единицы </summary>
        public string CurMesUnit { get; set; }

        /// <summary> Сокращение текущей учётной единицы </summary>
        public string CurMesUnitShort { get; set; }

        /// <summary> Является ли реактив списанным?</summary>
        public bool CurIsWrittenOff { get; set; }

        /// <summary> Яаляется ли срок годности неограниченным?</summary>
        public bool CurIsValidForever { get; set; }

        /// <summary>Текущее значение срока годности</summary>
        public DateTime? CurValidUntil { get; set; }

        /// <summary>Место хранения</summary>
        public string CurStoredAt { get; set; }

        /// <summary> Дата и автор создания записи </summary>
        public string Created { get; set; }

        /// <summary> Дата и автор последнего изменения записи </summary>
        public string Changed { get; set; }

        /// <summary>Список текущих атрибутов</summary>
        public IEnumerable<string> CurAttributesNames{ get; set; }

        /// <summary> Перечисление изменений</summary>
        public IEnumerable<ReactivesInfoHistoryModel> Changes { get; set; }

        /// <summary> Комментарии к поставке</summary>
        public string Comments { get; set; }
    }
}