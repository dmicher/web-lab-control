﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.Reactives
{
    /// <summary> Модель данных для добавления поставки реактива</summary>
    public class ReactivesAddModel
    {
        /// <summary> Возвращает строковое представление серии ЛКК</summary>
        public string LkksnString
            => Lkksn_SpecificationId.ToString("D3") + "/" +
               Lkksn_DeliveryNum.ToString("D2") + "/" +
               Lkksn_Date.ToString("ddMMyy");

        /// <summary> (скрыт) Выбранная для работы спецификация в номере ЛКК</summary>
        public int Lkksn_SpecificationId { get; set; }

        /// <summary>Название выбранной для добавления спецификации при предварительном просмотре </summary>
        public string SpecificationName { get; set; }

        /// <summary>Порядковый номер поставки для этой спецификации в номере ЛКК</summary>
        [Required(ErrorMessage = "Номер поставки не указан")]
        public int Lkksn_DeliveryNum { get; set; }

        /// <summary>Дата поставки в номере ЛКК </summary>
        [Required(ErrorMessage = "Дата поставки не указана")]
        public DateTime Lkksn_Date { get; set; }

        /// <summary> Перечень поставщиков, учтённых в базе данных </summary>
        public IEnumerable<SelectListItem> Providers { get; set; }

        /// <summary> Номер поставщика, выбранного пользователем</summary>
        public int SelectedProvider { get; set; }

        /// <summary> Перечень производителей, учтённых в базе данных </summary>
        public IEnumerable<SelectListItem> Producers { get; set; }

        /// <summary> Номер производителя, выбранного пользователем</summary>
        public int SelectedProducer { get; set; }

        /// <summary> Серийный номер производителя для реактива </summary>
        public string ProducerReactiveSn { get; set; }

        /// <summary> Название реактива у производителя </summary>
        public string ProducerReactiveName { get; set; }

        /// <summary> Количество реактива в единицах производителя </summary>
        public string ProducerReactiveInitCount { get; set; }

        /// <summary> Единицы измерения, в которых измеряется <see cref="ProducerReactiveInitCount"/> </summary>
        public string ProducerReactiveInitMesUnit { get; set; }

        /// <summary> Количество реактива в единицах измерения ЛКК </summary>
        [Required(ErrorMessage = "Объём поставки в учётных единицах не указан")]
        public string CurrentState_Count { get; set; } = "1.00";

        /// <summary> Единицы измерения, в которых измеряется <see cref="CurrentState_Count"/></summary>
        public string CurrentState_MesUnits { get; set; }

        /// <summary> Единицы измерения, учтённые в базе данных </summary>
        public IEnumerable<SelectListItem> MesUnits { get; set; }

        /// <summary> Срок годности реактива </summary>
        public DateTime? ValidUntilDate { get; set; }

        /// <summary> Флаг: срок годности не ограничен </summary>
        public bool UnlimitedLifeTime { get; set; }

        /// <summary> Место хранения, введённое вручную </summary>
        public string CurrentState_StoredAtInserted { get; set; }

        /// <summary> Место хранения, выбранное из списка </summary>
        public string CurrentState_StoredAtSelected { get; set; }

        /// <summary> Места хранения, уже указанные в базе данных </summary>
        public IEnumerable<SelectListItem> Stores { get; set; }

        /// <summary> Комментарий </summary>
        public string CurrentState_Comment { get; set; }

        /// <summary> Словарь атрибутов, которые должны быть добавлены к поставке </summary>
        public Dictionary<char, bool> Attributes { get; set; }

        /// <summary> Словарь имён атрибутов для человекопонятного вывода</summary>
        public Dictionary<char, string> AttributesNames { get; set;  }

    }
}