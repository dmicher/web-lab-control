﻿using System;

namespace WebApplication.Models.Reactives
{
    /// <summary> Модель данных для отражения изменений в данных поставки </summary>
    public class ReactivesInfoHistoryModel
    {
        /// <summary> Дата изменения </summary>
        public DateTime? Date { get; set; }

        /// <summary> Имя пользователя, внёсшего изменение</summary>
        public string CreatedBy { get; set; }

        /// <summary> Комментарий к изменению данных</summary>
        public string RecordComment { get; set; }

        /// <summary> Изменение остатков </summary>
        public decimal? DeltaCount { get; set; }

        /// <summary> Новое значение остатков</summary>
        public decimal? NewCount { get; set; }

        /// <summary> Списан ли </summary>
        public bool? IsWrittenOff { get; set; }

        /// <summary> Срок годности неограничен ли</summary>
        public bool? IsValidForever { get; set; }

        /// <summary> Новое значение срока годности</summary>
        public DateTime? ValidUntilDate { get; set; }

        /// <summary> Новое место хранения</summary>
        public string StoredAt { get; set; }

        /// <summary> Новая строка с атрибутами</summary>
        public string Attributes { get; set; }

        /// <summary> Содержание прочих изменений </summary>
        public string OtherChanges { get; set; }

    }
}