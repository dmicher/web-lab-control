﻿using System;

namespace WebApplication.Models.Reactives
{
    /// <summary> Модель строки данных для изменения израсходованных количеств для реагента </summary>
    public class ReactivesEditCountRowModel
    {
        /// <summary> Идентификатор строки в базе данных </summary>
        public int? Id { get; set; }

        /// <summary> Номер серии ЛКК </summary>
        public string Lkksn { get; set; }

        /// <summary> Существующий комментарий к записи истории </summary>
        public string RecordComment { get; set; }

        /// <summary> Учетные единицы </summary>
        public string MesUnits { get; set; }

        /// <summary> Дата расходования старая </summary>
        public DateTime? RecordDate_Old { get; set; }

        /// <summary> Дата расходования новая </summary>
        public DateTime? RecordDate_New { get; set; }

        /// <summary> Изменение расходования старое </summary>
        public decimal? Delta_Old { get; set; }

        /// <summary> Изменение расходования новое </summary>
        public string Delta_New { get; set; }

        /// <summary> Итоговое значение старое </summary>
        public decimal? Count_Old { get; set; }

        /// <summary> Итоговое значение новое </summary>
        public string Count_New { get; set; }
        
        /// <summary> Комментарий к изменению записи в истории </summary>
        public string EditingComment { get; set; }

    }
}