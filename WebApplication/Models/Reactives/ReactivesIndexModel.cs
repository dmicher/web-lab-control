﻿using System;

namespace WebApplication.Models.Reactives
{
    /// <summary> Модель строки данных в результатах поиска поставки </summary>
    public class ReactivesIndexModel
    {
        /// <summary> (первичный ключ) Серийный номер ЛКК </summary>
        public string Lkksn { get; set; }

        /// <summary> Название спецификации </summary>
        public string Specification { get; set; }

        /// <summary> Оставшееся количество </summary>
        public decimal? Count { get; set; }

        /// <summary> Единица измерения </summary>
        public string MesUnitShort { get; set; }

        /// <summary> Является ли срок годности реактива неограниченным? </summary>
        public bool IsValidForever { get; set; }

        /// <summary> Срок годности </summary>
        public DateTime? ValidUntil { get; set; }

        /// <summary> Списана ли текущая поставка </summary>
        public bool IsWrittenOff { get; set; }

    }
}