﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.Reactives
{
    /// <summary> Модель данных для изменения истории списания реагентов </summary>
    public class ReactivesEditCountsModel
    {
        /// <summary> Серия ЛКК</summary>
        public string Lkksn { get; set; }

        /// <summary> Существующие единицы измерения для выбора единиц производителя </summary>
        public IEnumerable<SelectListItem> MesUnitsProducer { get; set; }

        /// <summary> Существующие единицы измерения для выбора учётных единиц </summary>
        public IEnumerable<SelectListItem> MesUnitsCounted { get; set; }

        /// <summary> Объём поставки в единицах измерения производителя - старый</summary>
        public decimal? ProducerCount_Old { get; set; }

        /// <summary> Объём поставки в единицах измерения производителя - новый </summary>
        public string ProducerCount_New { get; set; }

        /// <summary> Единица измерения производителя - старая</summary>
        public string ProducerMesUnits_Old { get; set; }

        /// <summary> Единица измерения производителя - новая </summary>
        public string ProducerMesUnits_New { get; set; }

        /// <summary> Начальное количество в учётных единицах измерения - старое</summary>
        public decimal StartCount_Old { get; set; }

        /// <summary> Начальное количество в учётных единицах измерения - новое </summary>
        public string StartCount_New { get; set; }

        /// <summary> Текущее количество в учётных единицах - старое </summary>
        public decimal CurrentCount_Old { get; set; }

        /// <summary> Текущее количество в учётных единицах - новое</summary>
        public string CurrentCount_New { get; set; }

        /// <summary> Учётная единица измерения - старая </summary>
        public string StartMesUnits_Old { get; set; }

        /// <summary> Учётная единица измерения - новая </summary>
        public string StartMesUnits_New { get; set; }

        /// <summary> Комментарий к изменению записи в истории </summary>
        public string EditingComment { get; set; }

        /// <summary> Строки истории списания реактива </summary>
        public IEnumerable<ReactivesEditCountRowModel> Rows { get; set; }
    }
}