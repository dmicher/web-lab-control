﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.Reactives
{
    /// <summary> Модель данных для изменения серии ЛКК в существующей поставке</summary>
    public class ReactivesEditSpecificationModel
    {
        /// <summary> Номер серии ЛКК выбранной поставки </summary>
        public string Lkksn { get; set; }

        /// <summary> Все существующие номера спецификаций </summary>
        public IEnumerable<SelectListItem> Specifications { get; set; }

        /// <summary> Выбранная спецификация </summary>
        public string SelectedSpecification { get; set; }

        /// <summary> Введённый номер спецификации</summary>
        public int? InsertedSpecification { get; set; }

        /// <summary> Номер доставки в выбранном году</summary>
        public int DeliveryNumber { get; set; }

        /// <summary> Дата поставки</summary>
        public DateTime? Date { get; set; }

        /// <summary> Новый номер, сформированный по введённым данным</summary>
        public string NewLkksn
            => (InsertedSpecification == null
                   ? int.TryParse(SelectedSpecification, out var i)
                       ? i.ToString("D3")
                       : InsertedSpecification?.ToString("D3")
                   : InsertedSpecification?.ToString("D3")) + "/" +
               DeliveryNumber.ToString("D2") + "/" +
               Date?.ToString("ddMMyy");
    }
}