﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.Reactives
{
    /// <summary> Фильтр и результаты поиска реактивов по сериям ЛКК </summary>
    public class ReactivesIndexFilterModel
    {
        /// <summary> Результаты поиска реактивов </summary>
        public IEnumerable<ReactivesIndexModel> Reactives { get; set; }

        /// <summary> Спецификации для выпадающего списка </summary>
        public IEnumerable<SelectListItem> Specifications { get; set; }

        /// <summary> Производители для выпадающего списка </summary>
        public IEnumerable<SelectListItem> Producers { get; set; }

        /// <summary> Поставщики для выпадающего списка </summary>
        public IEnumerable<SelectListItem> Providers { get; set; }

        /// <summary> Существующие атрибуты для выбора </summary>
        public IEnumerable<SelectListItem> SortByOptions { get; set; } = new List<SelectListItem>
        {
            new SelectListItem("по дате поступления - новые вначале (по умолчанию)", "date_new", true),
            new SelectListItem("по дате поступления - старые вначале", "date_old"),
            new SelectListItem("по серии ЛКК", "lkksn"),
            new SelectListItem("по дате создания записи - новые вначале", "created_at_new"),
            new SelectListItem("по дате создания записи - старые вначале", "created_at_old"),
            new SelectListItem("по дате последнего изменения - новые вначале", "changed_at_new"),
            new SelectListItem("по дате последнего изменения - старые вначале", "changed_at_old")
        };

        /// <summary> Статусы списания </summary>
        public IEnumerable<SelectListItem> WrittenOff => new List<SelectListItem>
        {
            new SelectListItem("не списанные", "no", true),
            new SelectListItem("списанные", "yes"),
            new SelectListItem("все", "any")
        };

        /// <summary>Поле поиска спецификации по её номеру</summary>
        public string Filter_Lkksn_SpecificationLike { get; set; }

        /// <summary> Поле поиска по имени спецификации</summary>
        public string Filter_SpecificationNameLike { get; set; }

        /// <summary> Выбранная спецификация из выпадающего списка </summary>
        public int Filter_Lkksn_SpecificationSelected { get; set; }

        /// <summary> Поле поиска по номеру ЛКК </summary>
        public string Filter_Lkksn_NumberLike { get; set; }

        /// <summary> Дата поиска по номеру ЛКК</summary>
        public DateTime? Filter_Lkksn_Date { get; set; }

        /// <summary> Выбранный фильтр по списанию </summary>
        public string Filter_WrittenOff { get; set; } = "no";

        /// <summary> Выбранный производитель</summary>
        public int Filter_ProducerSelected { get; set; }

        /// <summary> Выбранный поставщик </summary>
        public int Filter_ProviderSelected { get; set; }

        /// <summary> Комментарий содержит </summary>
        public string Filter_CommentsLike { get; set; }

        /// <summary> Выбранный год для фильтрации </summary>
        public int? Filter_YearInserted { get; set; }

        /// <summary> Выбранный месяц для фильтрации </summary>
        public int Filter_MonthSelected { get; set; }

        /// <summary> Выбранное значение для варианта сортировки </summary>
        public string SortBySelected { get; set; }

        /// <summary> Ограничение по количеству строк в результатах поиска</summary>
        public int ResultRowsCountLimit { get; set; } = 25;

        /// <summary> Словарь включенных фильтров по атрибутам на основе их кодов </summary>
        public Dictionary<char, bool> Filter_Attributes { get; set; }

        /// <summary> Словарь названий атрибутов для человекопонятного вывода</summary>
        public Dictionary<char, string> AttributesNames { get; set; } 

    }

}