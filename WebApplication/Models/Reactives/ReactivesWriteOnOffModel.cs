﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.Reactives
{
    /// <summary> Модель списания / восстановления поставки </summary>
    public class ReactivesWriteOnOffModel
    {
        /// <summary>Номер серии ЛКК</summary>
        public string Lkksn { get; set; }

        /// <summary> Текущие остатки в серии</summary>
        public decimal CurrentCount { get; set; }

        /// <summary> Единицы измерения серии</summary>
        public string MesUnits { get; set; }

        /// <summary> Статус списания к настоящему моменту </summary>
        public bool IsAlready_WrittenOff { get; set; }

        /// <summary>Допустимые статусы списания</summary>
        public IEnumerable<SelectListItem> WrittenOffStates => new List<SelectListItem>
        {
            new SelectListItem("", "-1", true),
            new SelectListItem("Списан", "1", false),
            new SelectListItem("Не списан", "0", false)
        };

        /// <summary> Новый статус списания</summary>
        public int SetWrittenOff { get; set; }

        /// <summary> Игнорировать ненулевое значение остатков</summary>
        public bool IgnoreNotNullCount { get; set; }
    }
}