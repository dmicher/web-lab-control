﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.Login
{
    /// <summary> Модель данных для формы входа </summary>
    public class LoginModel
    {
        /// <summary> Имя пользователя </summary>
        [Required(ErrorMessage = "Не указан пользователь")]
        public string Name { get; set; }

        /// <summary> Пароль пользователя </summary>
        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}