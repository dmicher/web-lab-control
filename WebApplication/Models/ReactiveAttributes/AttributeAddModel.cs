﻿namespace WebApplication.Models.ReactiveAttributes
{
    /// <summary> Модель добавления атрибута </summary>
    public class AttributeAddModel
    {
        /// <summary> Атрибут для добавления </summary>
        public AttributeModel ToAdd { get; set; }

        /// <summary> Существующие атрибуты </summary>
        public string Existing { get; set; }
    }
}