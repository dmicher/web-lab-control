﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.ReactiveAttributes
{
    /// <summary> Модель изменения для атрибута </summary>
    public class AttributeEditModel
    {
        /// <summary> Изменяемый атрибут </summary>
        [Required(ErrorMessage = "Изменяемый атрибут не указан в запросе")]
        public AttributeModel EditFor { get; set; }

        /// <summary> Новое имя для изменяемого атрибута </summary>
        [Required(ErrorMessage = "Новое описание для атрибута не введено")]
        public string NewName { get; set; }
    }
}