﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.ReactiveAttributes
{
    /// <summary> Модель атрибута </summary>
    public class AttributeModel
    {
        /// <summary> Код </summary>
        [Required(ErrorMessage = "Код атрибута не введён")]
        public char Code { get; set; }

        /// <summary> Имя </summary>
        [Required(ErrorMessage = "Описание атрибута не введено")]
        public string Name { get; set; }
    }
}