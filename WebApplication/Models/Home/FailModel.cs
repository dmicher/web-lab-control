﻿namespace WebApplication.Models.Home
{
    /// <summary> Провал, случившийся где-либо </summary>
    public class FailModel
    {
        /// <summary> Кратко название </summary>
        public string Name { get; set; }

        /// <summary> Описание</summary>
        public string Description { get; set; }

        /// <summary> Требуется ли после демонстрации ошибки снять с пользователя аутентификацию </summary>
        public bool Logout { get; set; } = false;
    }
}