﻿using System.ComponentModel.DataAnnotations;
using WebApplication.Models.Shared;

namespace WebApplication.Models.Producers
{
    /// <summary> Модель данных для производителя реактивов </summary>
    public class ProducerInfoModel
    {
        /// <summary> Номер поставщика </summary>
        public int Id { get; set; }

        /// <summary> Название поставщика </summary>
        [Required(ErrorMessage = "Название поставщика не заполнено")]
        public string Name { get; set; }

        /// <summary> Произвольный комментарий </summary>
        public string Comment { get; set; }

        /// <summary> Идентификатор строки </summary>
        public DbRecordIdentity RecordIdentity { get; set; }
    }
}