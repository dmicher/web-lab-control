﻿using System.ComponentModel.DataAnnotations;
using WebApplication.Models.Shared;

namespace WebApplication.Models.Producers
{
    /// <summary> Модель данных для изменения информации о производителе </summary>
    public class ProducerEditModel
    {
        /// <summary> Номер </summary>
        public int Id { get; set; }

        /// <summary> Старое имя </summary>
        public string OldName { get; set; }

        /// <summary> Старый комментарий  </summary>
        public string OldComment { get; set; }

        /// <summary> Новое имя </summary>
        [Required(ErrorMessage = "Не введено новое имя поставщика")]
        public string NewName { get; set; }

        /// <summary> Новый комментарий </summary>
        public string NewComment { get; set; }

        /// <summary> Идентификатор строки в БД </summary>
        public DbRecordIdentity RecordIdentity { get; set; }
    }
}