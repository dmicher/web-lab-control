﻿using System.Collections.Generic;

namespace WebApplication.Models.Producers
{
    /// <summary> Модель данных для просмотра и фильтрации производителей реактивов </summary>
    public class ProducerViewFilterModel
    {
        /// <summary> Выбранные производители реактивов </summary>
        public IEnumerable<ProducerIndexModel> Producers { get; set; }

        /// <summary> Фильтр производителя по названию </summary>
        public string FilterName { get; set; }

        /// <summary> Фильтр производителя по содержанию в комментарии </summary>
        public string FilterComment { get; set; }
    }
}