﻿namespace WebApplication.Models.Producers
{
    /// <summary> Краткая информация о производителе для просмотра </summary>
    public class ProducerIndexModel
    {
        /// <summary> Номер </summary>
        public int Id { get; set; }
        
        /// <summary> Имя организации </summary>
        public string Name { get; set; }

        /// <summary> Комментарий </summary>
        public string Comment { get; set; }
    }
}