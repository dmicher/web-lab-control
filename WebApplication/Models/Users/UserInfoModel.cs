﻿namespace WebApplication.Models.Users
{
    /// <summary> Описание пользователя </summary>
    public class UserInfoModel
    {
        /// <summary> Номер пользователя </summary>
        public int Id { get; set; }

        /// <summary> Логин пользователя, с которым он входит на сайт </summary>
        public string Login { get; set; }

        /// <summary> Полное имя пользователя (Ф.И.О.) </summary>
        public string Name { get; set; }

        /// <summary> Группа пользователя </summary>
        public string Group { get; set; }
    }
}