﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.Users
{
    /// <summary> Модель данных для добавления новой информации о пользователе </summary>
    public class UserAddModel
    {
        /// <summary> Имя для входа на сайт </summary>
        [Required(ErrorMessage = "Поле с именем для входа пользователя не заполнено")]
        public string Login { get; set; }

        /// <summary> Имя пользователя</summary>
        [Required(ErrorMessage = "Поле с именем пользователя не заполнено")]
        public string Name { get; set; }

        /// <summary> Пароль пользователя </summary>
        [Required(ErrorMessage = "Поле с паролем пользователя не заполнено")]
        [MinLength(4, ErrorMessage = "Пароль не может быть меньше 4 символов")]
        public string Password { get; set; }

        [Required]
        public int SelectedGroup { get; set; }

        /// <summary> Группы пользователя </summary>
        public IEnumerable<SelectListItem> Groups { get; set; }
    }
}