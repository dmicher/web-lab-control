﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.Users
{
    /// <summary> Изменяемая модель информации о пользователе </summary>
    public class UserEditModel
    {
        /// <summary> (неизменно, скрыто) Номер пользователя </summary>
        public int Id { get; set; }

        /// <summary> Старое имя (Ф.И.О.) пользователя </summary>
        public string OldName { get; set; }

        /// <summary> Старое имя для входа </summary>
        public string OldLogin { get; set; }

        /// <summary> Старая группа пользователя (строка) </summary>
        public string OldGroup { get; set; }

        /// <summary> Номер старой группы пользователя (скрытый)</summary>
        public int OldGroupNumber { get; set; }

        /// <summary> Новое имя пользователя </summary>
        [Required(ErrorMessage = "Новое имя не заполнено.")]
        public string NewName { get; set; }

        /// <summary> Новое имя для входа </summary>
        [Required(ErrorMessage = "Новое имя для входа не заполнено")]
        public string NewLogin { get; set; }

        /// <summary> Новый пароль пользователя </summary>
        [MinLength(4, ErrorMessage = "Минимальный размер пароля - 4 символа")]
        public string NewPassword { get; set; }

        /// <summary> Новая группа пользователя </summary>
        [Required(ErrorMessage = "Новая группа не выбрана")]
        public int NewGroupNumber { get; set; }

        /// <summary> Список групп пользователей </summary>
        public IEnumerable<SelectListItem> Groups { get; set; }

    }
}