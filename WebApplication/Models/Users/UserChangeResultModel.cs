﻿namespace WebApplication.Models.Users
{
    /// <summary> Модель для результатов изменения данных о пользователе </summary>
    public class UserChangeResultModel
    {
        /// <summary> Титул для HTML страницы</summary>
        public string Title { get; set; }

        /// <summary> Заголовок для страницы </summary>
        public string Header { get; set; }

        /// <summary> Описание на странице </summary>
        public string Description { get; set; }
    }
}