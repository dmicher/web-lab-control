﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Models.Users
{
    /// <summary> Модель удаления пользователя </summary>
    public class UserDeleteModel
    {
        /// <summary> Текущий пароль корневого пользователя </summary>
        [Required(ErrorMessage = "Пароль корневого пользователя не введён")]
        public string RootPassword { get; set; }

        /// <summary> Номер удаляемого пользователя </summary>
        public int UserId { get; set; }

        /// <summary> Имя удаляемого пользователя </summary>
        public string UserName { get; set; }

        /// <summary> Группа удаляемого пользователя </summary>
        public string UserGroup { get; set; }
    }
}
