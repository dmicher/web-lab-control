﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models.Users
{
    public class ChangeMyPasswordModel
    {
        /// <summary> Действующий пароль пользователя </summary>
        [Required(ErrorMessage = "Текущий пароль не введён")]
        public string CurrentPassword { get; set; }

        /// <summary> Новый пароль пользователя </summary>
        [Required(ErrorMessage = "Новый пароль не задан")]
        [MinLength(4, ErrorMessage = "Новый пароль не может быть меньше 4 символов")]
        public string NewPassword { get; set; }

        /// <summary> Повтор нового пароля пользователя</summary>
        [Required(ErrorMessage = "Повтор нового пароля не заполнен")]
        [Compare(nameof(NewPassword), ErrorMessage = "Пароль и его повтор не совпадают.")]
        public string RepeatNewPassword { get; set; }
    }
}