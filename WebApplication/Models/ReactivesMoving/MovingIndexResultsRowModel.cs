﻿using System;

namespace WebApplication.Models.ReactivesMoving
{
    /// <summary> Структура данных для строк в результатах поиска прихода и расхода реактивов </summary>
    public class MovingIndexResultsRowModel
    {
        /// <summary> Дата и время изменения</summary>
        public DateTime? MovingDateTime { get; set; }

        /// <summary> Номер спецификации </summary>
        public int? SpecificationId { get; set; }

        /// <summary> Название спецификации </summary>
        public string SpecificationName { get; set; }

        /// <summary>Серия ЛКК поставки реактива</summary>
        public string Lkksn { get; set; }
        
        /// <summary>На какую величину изменилась поставка</summary>
        public decimal Difference { get; set; }

        /// <summary>Единица измерения, на которую поменялись данные</summary>
        public string MesUnits { get; set; }

        /// <summary> Комментарий к изменению данных</summary>
        public string RecordComment { get; set; }

        /// <summary> Пользователь, изменивший данные</summary>
        public string UserName { get; set; }
    }
}