﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Models.ReactivesMoving
{
    /// <summary> Модель данных для фильтрации и представления результатов поиска прихода и расхода реактивов</summary>
    public class MovingIndexFilterModel
    {
        /// <summary> Дата начала выборки </summary>
        public DateTime FilterStartDate { get; set; } 
            = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);

        /// <summary> Дата завершения выборки </summary>
        public DateTime FilterEndDate { get; set; } 
            = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);

        /// <summary> Имеющиеся в базе спецификации</summary>
        public IEnumerable<SelectListItem> Specifications { get; set; }

        /// <summary>Выбранная спецификация для фильтра</summary>
        public int? FilterSelectedSpecification { get; set; }

        /// <summary> Спецификация, введённая в ручную</summary>
        public string FilterInsertedSpecification { get; set; }

        /// <summary>Имеющиеся в базе атрибуты</summary>
        public Dictionary<char, string> Attributes { get; set; }

        /// <summary> Выбранные атрибуты для фильтра "Присутствует"</summary>
        public Dictionary<char, bool> FilterAttributesOn { get; set; }

        /// <summary> Выбранные атрибуты для фильтра "Отсутствует"</summary>
        public Dictionary<char, bool> FilterAttributesOff { get; set; }

        /// <summary> Учтённые в базе данных пользователи</summary>
        public IEnumerable<SelectListItem> Users { get; set; }

        /// <summary>Выбранный для фильтрации пользователь</summary>
        public int? FilterSelectedUser { get; set; }

        /// <summary> Результаты поиска </summary>
        public IEnumerable<MovingIndexResultsRowModel> Rows { get; set; }
    }
}