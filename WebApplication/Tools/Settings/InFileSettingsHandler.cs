﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WebApplication.Tools.Settings
{
    /// <summary> Обработчик настроек, хранящий свои данные в файловой системе </summary>
    public class InFileSettingsHandler : ISettingsHandler
    {
        /// <summary> Путь к файлу с настройками </summary>
        private readonly string _filePath = Path.Combine
        (
            AppDomain.CurrentDomain.BaseDirectory ?? "",
            "App_Data",
            "settings.ini"
        );

        /// <summary> Получить значение настройки по его имени </summary>
        /// <param name="settingName">Имя настройки</param>
        /// <returns>Строковое представление настройки</returns>
        public string GetValue(SettingsNames settingName)
        {
            var dict = ReadFile();
            return dict.ContainsKey(settingName) ? dict[settingName] : null;
        }

        /// <summary> Задать новое значение нстройки по его имени</summary>
        /// <param name="settingName">Имя настройки</param>
        /// <param name="settingValue">Новое значение настройки</param>
        public void SetValue(SettingsNames settingName, string settingValue)
        {
            var file = new FileInfo(_filePath);
            var lines = new List<string>();

            if (file.Exists)
                lines.AddRange(File.ReadAllLines(_filePath));
            else if (!Directory.Exists(file.DirectoryName))
                Directory.CreateDirectory(file.DirectoryName);

            var index = lines.FindIndex(x => x.StartsWith(settingName.ToString()));
            if (index < 0) lines.Add(settingName + "=" + settingValue);
            else lines[index] = settingName + "=" + settingValue;

            using var writer = file.CreateText();
            foreach (var line in lines)
                writer.WriteLine(line);
            writer.Dispose();
        }

        /// <summary> Получить все имена и значения настроек</summary>
        /// <returns> Словарь, где названия настроек лежат в ключах, а значение - в значениях</returns>
        public Dictionary<SettingsNames, string> GetAll() => ReadFile();

        /// <summary> Провесть все настройки из файла </summary>
        /// <returns> словарь настроек </returns>
        private Dictionary<SettingsNames, string> ReadFile()
        {
            if (!File.Exists(_filePath)) return null;

            var lines = File.ReadAllLines(_filePath);
            if (lines.Length < 1) return null;

            var result = new Dictionary<SettingsNames, string>();
            foreach (var line in lines)
            {
                var str = line.Trim();
                if (string.IsNullOrWhiteSpace(str) || str.StartsWith("#")) continue;
                var index = str.IndexOf('=');
                if (index < 0) continue;

                if (!Enum.TryParse(str.Substring(0, index), out SettingsNames key))
                    continue;
                var value = str.Substring(index + 1, str.Length - index - 1);
                if (value.StartsWith('\"') && value.EndsWith('\"'))
                    value = value.Substring(1, value.Length - 2);
                result.Add(key, value);
            }

            return result;
        }

    }
}