﻿using System.Collections.Generic;

namespace WebApplication.Tools.Settings
{
    /// <summary> Декларация взаимодействия с настройками </summary>
    public interface ISettingsHandler
    {
        /// <summary> Получить значение настройки по его имени </summary>
        /// <param name="settingName">Имя настройки</param>
        /// <returns>Строковое представление настройки</returns>
        public string GetValue(SettingsNames settingName);

        /// <summary> Задать новое значение нстройки по его имени</summary>
        /// <param name="settingName">Имя настройки</param>
        /// <param name="settingValue">Новое значение настройки</param>
        public void SetValue(SettingsNames settingName, string settingValue);

        /// <summary> Получить все имена и значения настроек</summary>
        /// <returns> Словарь, где названия настроек лежат в ключах, а значение - в значениях</returns>
        public Dictionary<SettingsNames, string> GetAll();
    }
}