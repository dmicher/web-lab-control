﻿namespace WebApplication.Tools.Settings
{
    /// <summary> Названия настроек </summary>
    public enum SettingsNames
    {
        /// <summary> Общая строка подключения к базе данных </summary>
        DbConnectionString,

        /// <summary> Часть строки подключения, подлежащая замене при формирования подключения </summary>
        DbConnectionStringReplacePart,

        /// <summary> Путь к файлу (относительно базовой папки приложения) к основной базе приложения </summary>
        DbMainBaseFile,

        /// <summary> Путь к папке с резервными копиями базы даннных </summary>
        DbBackUpBasePath,

        /// <summary> Путь к хранению временных файлов </summary>
        TempFilesPath
    }
}