﻿using System.Data;

namespace WebApplication.Tools.DbConnector
{
    /// <summary> Декларация взаимодейтсвия с базой данных </summary>
    public interface IDbConnector
    {
        /// <summary> Читает данные по SQL запросу. Ответ, равный <see langword="null"/>, не считается возможным </summary>
        /// <param name="sqlRequest"> SQL запрос к базе данных </param>
        /// <param name="error">Сообщение об ошибке, если она произошла</param>
        /// <returns>Данные, получаемые в ответ. <see langword="null"/> считается ошибкой</returns>
        DataTable ReadData(string sqlRequest, out string error);
        
        /// <summary> Читает данные по SQP запросу. Ответ, равный <see langword="null"/>, считается возможным </summary>
        /// <param name="sqlRequest">SQL запрос к базе данных </param>
        /// <param name="result">Результирующие данные, для которых <see langword="null"/> считается допустимым занчением</param>
        /// <param name="error">Сообщение об ошибке, если она произошла </param>
        /// <returns>Флаг успешного выполнения</returns>
        bool TryReadData(string sqlRequest, out DataTable result, out string error);

        /// <summary> Читает единственное значение (ячейка [0][0]) в базе данных.</summary>
        /// <param name="sqlRequest">SQL-запрос, предусматривающий строго одну ячейку ответа</param>
        /// <param name="error">Сообщение об ошибке</param>
        /// <returns>Строковое представление ответа</returns>
        string GetValue(string sqlRequest, out string error);

        /// <summary>
        /// Производит запись данных в базу. Считается недопустимым, если команда не была применена
        /// ни к одной из строк базы данных.
        /// </summary>
        /// <param name="sqlRequest">SQL запрос к базе данных</param>
        /// <param name="error">Сообщение об ошибке, если та произошла</param>
        /// <returns>Количество строк, на которые повлияло выполенине команды</returns>
        int WriteData(string sqlRequest, out string error);

        /// <summary>
        /// Производит запись данных в базу. Считается допустимым, если команда
        /// не была применена ни на одной строке базы данных.
        /// </summary>
        /// <param name="sqlRequest">SQL запрос к базе данных, не подразумевающий возвращение данных</param>
        /// <param name="rowsAffected">Количество строк, на которые повлияло выполнение команды</param>
        /// <param name="error">Сообщение об ошибке, если она произошла</param>
        /// <returns>Флаг успешного выполнения команды</returns>
        bool TryWriteData(string sqlRequest, out int rowsAffected, out string error);
    }
}