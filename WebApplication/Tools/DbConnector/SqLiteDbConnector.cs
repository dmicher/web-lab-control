﻿using System;
using System.Data;
using System.Data.SQLite;
using System.IO;
using WebApplication.Tools.Settings;

namespace WebApplication.Tools.DbConnector
{
    /// <summary> Соединитель с базой данных SqLite</summary>
    public class SqLiteDbConnector : IDbConnector
    {
        /// <summary> Текущий сервис настроек </summary>
        private readonly ISettingsHandler _settings;

        /// <summary> Конструктор, требующий сервис настроек </summary>
        /// <param name="settings"> Сервис настроек </param>
        public SqLiteDbConnector(ISettingsHandler settings)
            => _settings = settings;

        /// <summary> Читает данные по SQL запросу. Ответ, равный <see langword="null"/>, не считается возможным </summary>
        /// <param name="sqlRequest"> SQL запрос к базе данных </param>
        /// <param name="error">Сообщение об ошибке, если она произошла</param>
        /// <returns>Данные, получаемые в ответ. <see langword="null"/> считается ошибкой</returns>
        public DataTable ReadData(string sqlRequest, out string error)
        {
            var basePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? "",
                _settings.GetValue(SettingsNames.DbMainBaseFile));
            if (!File.Exists(basePath))
            {
                error = "Файл базы данных не обнаружен в месте, заданном настройками программы. ";
                return null;
            }

            try
            {
                using var connection = new SQLiteConnection
                (
                    _settings
                        .GetValue(SettingsNames.DbConnectionString)
                        .Replace(_settings.GetValue(SettingsNames.DbConnectionStringReplacePart),
                            basePath)
                );
                var command = new SQLiteCommand(sqlRequest, connection);
                connection.Open();
                var data = new DataTable("result");
                data.Load(command.ExecuteReader());
                connection.Close();
                connection.Dispose();

                if (data.Columns.Count < 1 || data.Rows.Count < 1)
                {
                    error = "Провал чтения данных. Получена пустая таблица.";
                    return null;
                }

                error = null;
                return data;
            }
            catch (Exception e)
            {
                error = "Провал чтения данных. " + e.Message;
                return null;
            }
        }

        /// <summary> Читает единственное значение (ячейка [0][0]) в базе данных.</summary>
        /// <param name="sqlRequest">SQL-запрос, предусматривающий строго одну ячейку ответа</param>
        /// <param name="error">Сообщение об ошибке</param>
        /// <returns>Строковое представление ответа</returns>
        public string GetValue(string sqlRequest, out string error) =>
            ReadData(sqlRequest, out error)?.Rows[0][0]?.ToString()?.Trim();

        /// <summary> Читает данные по SQP запросу. Ответ, равный <see langword="null"/>, считается возможным </summary>
        /// <param name="sqlRequest">SQL запрос к базе данных </param>
        /// <param name="result">Результирующие данные, для которых <see langword="null"/> считается допустимым занчением</param>
        /// <param name="error">Сообщение об ошибке, если она произошла </param>
        /// <returns>Флаг успешного выполнения</returns>
        public bool TryReadData(string sqlRequest, out DataTable result, out string error)
        {
            var basePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? "",
                _settings.GetValue(SettingsNames.DbMainBaseFile));
            if (!File.Exists(basePath))
            {
                error = "Файл базы данных не обнаружен в месте, заданном настройками программы. ";
                result = null;
                return false;
            }

            try
            {
                using var connection = new SQLiteConnection
                (
                    _settings
                        .GetValue(SettingsNames.DbConnectionString)
                        .Replace(_settings.GetValue(SettingsNames.DbConnectionStringReplacePart),
                            basePath)
                );
                var command = new SQLiteCommand(sqlRequest, connection);
                connection.Open();
                var data = new DataTable("result");
                data.Load(command.ExecuteReader());
                connection.Close();
                connection.Dispose();

                result = data.Columns.Count < 1 || data.Rows.Count < 1
                    ? null
                    : data;

                error = null;
                return true;
            }
            catch (Exception e)
            {
                error = "Провал чтения данных. " + e.Message;
                result = null;
                return false;
            }
        }

        /// <summary>
        /// Производит запись данных в базу. Считается недопустимым, если команда не была применена
        /// ни к одной из строк базы данных.
        /// </summary>
        /// <param name="sqlRequest">SQL запрос к базе данных</param>
        /// <param name="error">Сообщение об ошибке, если та произошла</param>
        /// <returns>Количество строк, на которые повлияло выполенине команды</returns>
        public int WriteData(string sqlRequest, out string error)
        {
            var basePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? "",
                _settings.GetValue(SettingsNames.DbMainBaseFile));
            if (!File.Exists(basePath))
            {
                error = "Файл базы данных не обнаружен в месте, заданном настройками программы. ";
                return -1;
            }

            try
            {
                using var connection = new SQLiteConnection
                (
                    _settings
                        .GetValue(SettingsNames.DbConnectionString)
                        .Replace(_settings.GetValue(SettingsNames.DbConnectionStringReplacePart),
                            basePath)
                );
                var command = new SQLiteCommand(sqlRequest, connection);
                connection.Open();
                var rowsAffected = command.ExecuteNonQuery();
                connection.Close();
                connection.Dispose();

                error = rowsAffected < 1
                    ? "Провал записи данных. Запрос не был применён ни к одной из строк базы данных."
                    : null;
                return rowsAffected;
            }
            catch (Exception e)
            {
                error = "Провал записи в базу данных. " + e.Message;
                return 0;
            }
        }

        /// <summary> Производит запись данных в базу без возвращаемых данных из базы</summary>
        /// <param name="sqlRequest">SQL запрос к базе данных, не подразумевающий возвращение данных</param>
        /// <param name="rowsAffected">Количество строк, на которые повлияло выполнение команды</param>
        /// <param name="error">Сообщение об ошибке, если она произошла</param>
        /// <returns>Флаг успешного выполнения</returns>
        public bool TryWriteData(string sqlRequest, out int rowsAffected, out string error)
        {
            var basePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? "",
                _settings.GetValue(SettingsNames.DbMainBaseFile));
            if (!File.Exists(basePath))
            {
                error = "Файл базы данных не обнаружен в месте, заданном настройками программы. ";
                rowsAffected = -1;
                return false;
            }

            try
            {
                using var connection = new SQLiteConnection
                (
                    _settings
                        .GetValue(SettingsNames.DbConnectionString)
                        .Replace(_settings.GetValue(SettingsNames.DbConnectionStringReplacePart),
                            basePath)
                );
                var command = new SQLiteCommand(sqlRequest, connection);
                connection.Open();
                rowsAffected = command.ExecuteNonQuery();
                connection.Close();
                connection.Dispose();
                error = null;
                return true;
            }
            catch (Exception e)
            {
                error = "Провал записи в базу данных. " + e.Message;
                rowsAffected = 0;
                return false;
            }
        }
    }
}