﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Home;

namespace WebApplication.Tools.Project
{
    /// <summary> Методы расширения для проекта </summary>
    public static class Extensions
    {
        /// <summary> (dak-расширение) Получает идентификатор пользователя в БД из данных аутентификации</summary>
        /// <param name="ctr">Контроллер ASP MVC</param>
        public static int GetUserId(this Controller ctr)
            => int.TryParse(ctr.User.Claims.FirstOrDefault(x => x.Type == "user_id")?.Value, out var i)
                ? i
                : -1;

        /// <summary> (dak-расширение) Возвращает дату в виде строки для базы данных SQLite. </summary>
        /// <param name="date">Преобразуемая дата</param>
        /// <returns>Строка в формате: yyyy-MM-dd HH:mm:ss.fff</returns>
        public static string ToSqLiteString(this DateTime date)
            => date.ToString("yyyy-MM-dd HH:mm:ss.fff");

        /// <summary> (dak-расширение) Возвращает числов в виде строки для SQLite </summary>
        /// <param name="dec">Преобразуемое число</param>
        /// <returns>Строка в формате: 00.00</returns>
        public static string ToSqLiteString(this decimal dec)
            => dec.ToString(CultureInfo.InvariantCulture).Replace(",", ",");

        /// <summary> (dak-расширение) Возвращает дату в виде строкового представления с датой </summary>
        /// <param name="date">Преобразуемая дата</param>
        /// <returns>Строка в формате: dd.MM.yyyy</returns>
        public static string ToDateString(this DateTime date)
            => date.ToString("dd.MM.yyyy");

        /// <summary> (dak-расширение) Возвращает дату в виде строкового представления с датой и временем </summary>
        /// <param name="date">Преобразуемая дата</param>
        /// <returns>Строка в формате: dd.MM.yyyy HH:mm:ss</returns>
        public static string ToDateTimeString(this DateTime date)
            => date.ToString("dd.MM.yyyy HH:mm:ss");

        /// <summary> (dak-расширение) Получает из этой строки указанное значение </summary>
        /// <param name="row">Строка, из которой получается значение</param>
        /// <param name="name">Название столбца, значение которого получается</param>
        /// <returns>Очищенное от пустых символов в начале и конце значение указанного столбца</returns>
        public static string GetValue(this DataRow row, string name)
        {
            var result = row?[name]?.ToString()?.Trim();
            return string.IsNullOrWhiteSpace(result)
                ? null // конкретно null - важно
                : result;
        }

        /// <summary> (dak-расширение) Получает из строки указанное значение в структуре <see cref="decimal"/></summary>
        /// <param name="row">Строка таблицы</param>
        /// <param name="name">Столбец, в котором хранится дробное число</param>
        /// <returns>Нуль-допустимое числовое значение повышенной точночти</returns>
        public static decimal? GetDecimal(this DataRow row, string name)
            => decimal.TryParse(row?[name]?.ToString()?.Trim().Replace(".", ","), out var dec)
                ? (decimal?) dec
                : null;

        /// <summary> (dak-расширение) Получает из строки указанное значение в структуре <see cref="decimal"/></summary>
        /// <param name="row">Строка таблицы</param>
        /// <param name="name">Столбец, в котором хранится дробное число</param>
        /// <returns>Нуль-допустимое целое значение</returns>
        public static int? GetInt(this DataRow row, string name)
            => int.TryParse(row?[name]?.ToString()?.Trim().Replace(".", ","), out var i)
                ? (int?) i
                : null;

        /// <summary> (dak-расширение) Получает из указанной ячейки строки дату </summary>
        /// <param name="row">Строка, из которой извлекается дата</param>
        /// <param name="name">Столбец строки, где должна находиться дата</param>
        /// <returns>Дата или <see langword="null"/></returns>
        public static DateTime? GetDate(this DataRow row, string name)
            => DateTime.TryParse(row?[name]?.ToString()?.Trim(), out var date)
                ? (DateTime?) date
                : null;

        /// <summary> (dak-расширение) Приводит <see cref="string"/> к нуль-допустимому <see cref="decimal"/>. </summary>
        /// <param name="str">Преобразуемая строка</param>
        /// <returns>положительное число при удаче или <see langword="null"/> при провале</returns>
        public static decimal? ToDecimal(this string str)
            => string.IsNullOrWhiteSpace(str)
                ? null
                : decimal.TryParse(str.Trim().Replace(".", ","), out var dec)
                    ? (decimal?) dec
                    : null;

        /// <summary> (dak-расширение) Возвращает представление с выводом ошибки</summary>
        /// <param name="c">Контроллер, для которого определяется расширение</param>
        /// <param name="name">Название ошибки</param>
        /// <param name="description">Описание ошибки</param>
        /// <param name="logout">Выкинуть ли пользователя из состояния "вошёл в систему"</param>
        public static IActionResult Error(this Controller c, string name, string description, bool logout = false)
            => c.RedirectToAction("Fail", "Home",
                new FailModel {Name = name, Description = description, Logout = logout});
    }
}