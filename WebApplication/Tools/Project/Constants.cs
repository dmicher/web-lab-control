﻿namespace WebApplication.Tools.Project
{
    /// <summary> Базовые константы </summary>
    public static class Constants
    {
        /// <summary> Характерная часть строки в ошибке базы данных, сообщающей о нарушении уникального ключа </summary>
        public const string DataBaseUniqueConstraintError = "UNIQUE constraint failed";
    }
}