using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Settings;

namespace WebApplication
{
    public class Startup
    {
        /// <summary> ����������� ����������� ������ ������������ </summary>
        /// <param name="configuration">������������</param>
        public Startup(IConfiguration configuration) => Configuration = configuration;

        /// <summary> ������������ ���������� </summary>
        public IConfiguration Configuration { get; }

        /// <summary> ����� ���������� ������ ����������. ����� ������� ��������� ���� </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            
            // �����������
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = new PathString("/Login/Index");
                });

            // ��������� � ADO ����������� � ����
            var settingsHandler = new InFileSettingsHandler();
            services.AddSingleton<ISettingsHandler>(settingsHandler);
            services.AddSingleton<IDbConnector>(new SqLiteDbConnector(settingsHandler));
        }

        /// <summary> ����� ���������� ������ ����������. ������������ ��� ��������� ���������. </summary>
        /// <param name="app">����������</param>
        /// <param name="env">���������</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
