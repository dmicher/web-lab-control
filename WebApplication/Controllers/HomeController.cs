﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Home;

namespace WebApplication.Controllers
{
    /// <summary> Входной контроллер </summary>
    public class HomeController : Controller
    {
        /// <summary> Страница приветствия </summary>
        [Authorize]
        public IActionResult Index() => View();

        /// <summary> Вход в панель администрирования </summary>
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult AdminPanel() => View();

        /// <summary> Вход в панель управления реактивами</summary>
        [Authorize]
        public IActionResult ReactivesPanel() => View();

        /// <summary> Вход в панель отчётов и статистики </summary>
        [Authorize]
        public IActionResult StatisticsPanel() => View();

        /// <summary> Сюда попадают провалы и ошибки каких-либо действий</summary>
        /// <param name="model">Модель провала: название и описание</param>
        public async Task<IActionResult> Fail(FailModel model)
        {
            if (model.Logout) 
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return View(model);
        }

        /// <summary> Страница ошибки </summary>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() 
            => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
