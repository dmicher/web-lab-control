﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Home;
using WebApplication.Models.Providers;
using WebApplication.Models.Shared;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;

namespace WebApplication.Controllers
{
    /// <summary> Управление поставщиками </summary>
    [Authorize]
    public class ProvidersController : Controller
    {
        /// <summary> Сервис базы данных </summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструктор запрашивает нужные сервисы </summary>
        /// <param name="connector">Сервис поюключения к базе данных</param>
        public ProvidersController(IDbConnector connector)
            => _connector = connector;

        /// <summary> Начало всех запросов select в контроллере </summary>
        private const string SelectStart =
            "select tbl.\"Id\", tbl.\"name\", tbl.\"comments\", " +
            "tbl.\"created_at_date\", u1.\"name\" as \"created_by\", tbl.\"changed_at_date\", u2.\"name\" as \"changed_by\" " +
            "from \"Reactives_Providers\" tbl " +
            "left join \"Users\" u1 on tbl.\"created_by\" = u1.\"Id\" " +
            "left join \"Users\" u2 on tbl.\"changed_by\" = u2.\"Id\" ";

        /// <summary> Просмотр и фильтрация поставщиков</summary>
        public IActionResult Index(ProviderViewFilterModel model)
        {
            var builder = new StringBuilder("select \"Id\", \"name\", \"comments\" from \"Reactives_Providers\"");
            
            var isFirstCondition = true;

            string AndWhere()
            {
                if (!isFirstCondition) return " and ";
                isFirstCondition = false;
                return " where ";
            }

            if (!string.IsNullOrWhiteSpace(model.FilterName))
                builder.Append(AndWhere() + $"\"name\" like \'%{model.FilterName}%\' ");
            if (!string.IsNullOrWhiteSpace(model.FilterComment))
                builder.Append(AndWhere() + $"\"comments\" like \'%{model.FilterComment}%\' ");

            builder.Append(" order by \"Id\" asc;");

            // обращается к базе
            if (!_connector.TryReadData(builder.ToString(), out var data, out var error))
            {
                RedirectToAction("Error", "Home",
                    new FailModel {Name = "Провал базы данных", Description = error});
            }

            model.Providers = data?.Rows.Cast<DataRow>()
                .Select(x => new ProviderIndexModel
                {
                    Id = int.TryParse(x.GetValue("Id"), out var i) ? i : 0,
                    Name = x.GetValue("name"),
                    Comment = x.GetValue("comments")
                });

            // выводит представление
            return View(model);
        }

        /// <summary> Подробная информация по поставщику </summary>
        /// <param name="id">Номер поставщика</param>
        [HttpGet]
        public IActionResult Info(int? id)
        {
            if (id == null)
                return this.Error("Провал чтения данных",
                    "Невозможно загрузить данные о поставщике - в запросе отсутствует его номер. " +
                    "Обратитесь к разработчику.");

            // получает данные из базы
            var data = _connector.ReadData(SelectStart + $" where tbl.\"Id\" = {id};", out var error);

            if (data == null || data.Rows.Count != 1)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Неприемлемый ответ базы данных",
                        Description = "Невозможно загрузить данные о поставщике - от базы получено неприемлемое количество значений. " +
                                      error + " Обратитесь к разработчику."
                    });

            return View(new ProviderInfoModel
            {
                Id = (int)id,
                Name = data.Rows[0].GetValue("name"),
                Comment = data.Rows[0].GetValue("comments"),
                RecordIdentity = new DbRecordIdentity(data.Rows[0])
            });
        }

        /// <summary> Вход в инструмент добавления поставщика </summary>
        [HttpGet]
        public IActionResult Add() => View();

        /// <summary> Подтверждение добавления поставщика</summary>
        /// <param name="model">Модель поставщика</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Add(ProviderInfoModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var isComment = !string.IsNullOrWhiteSpace(model.Comment);

            var now = DateTime.Now.ToSqLiteString();
            var user = this.GetUserId();
            var done = _connector.WriteData
            (
                "insert into \"Reactives_Providers\" (\"name\", " + (isComment ? "\"comments\", " : null) +
                "\"created_at_date\", \"created_by\", \"changed_at_date\", \"changed_by\") " +
                $"values (\'{model.Name.Trim()}\', " + (isComment ? $"\'{model.Comment.Trim()}\', " : null) +
                $"\'{now}\', {user}, \'{now}\', {user});",
                out var error
            );

            if (done > 0)
                return RedirectToAction("Index", "Providers",
                    new ProviderViewFilterModel {FilterName = model.Name});

            if (error.Contains(Constants.DataBaseUniqueConstraintError))
                error = $"В базе данных уже существует поставщик с именем {model.Name}. " +
                        "Повторные номера не допустимы.";

            return RedirectToAction("Fail", "Home",
                new FailModel
                {
                    Name = "Ошибка базы данных",
                    Description = "При загрузке нового поставщика в базу данных произошла ошибка. " + error +
                                  " Данные не были загружены."
                });
        }

        /// <summary> Вход в инструмент изменения поставщика</summary>
        /// <param name="id">Номер поставщика, подлежащего изменению</param>
        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (id == null)
                return this.Error("Провал изменения поставщика - нет номера",
                    "При попытке загрузить инструмент для изменения поставщика произошла " +
                    "ошибка из-за отсутствия номера поставщика, подлежащего изменению. " +
                    "Обратитесь к разработчику.");

            // получает данные из базы
            var data = _connector.ReadData(SelectStart + $" where tbl.\"Id\" = {id};", out var error);

            if (data == null || data.Rows.Count != 1)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Провал изменения поставщика - неприемлемый ответ базы данных",
                        Description = "При попытке загрузить инструмент для изменения поставщика произошла " +
                                      "ошибка из-за неприемлемого ответа базы данных. " + error +
                                      "Обратитесь к разработчику."
                    });

            var name = data.Rows[0].GetValue("name");
            var comment = data.Rows[0].GetValue("comments");

            return View(new ProviderEditModel
            {
                Id = (int)id,
                OldName = name,
                NewName = name,
                OldComment = comment,
                NewComment = comment,
                RecordIdentity = new DbRecordIdentity(data.Rows[0])
            });

        }

        /// <summary> Подтверждение изменения поставщика </summary>
        /// <param name="model">Модель для изменения поставщика</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Edit(ProviderEditModel model)
        {
            // валидация входных данных
            if (string.IsNullOrWhiteSpace(model.OldName))
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Ошибка формы изменения данных",
                        Description = "Во время обновления данных обнаружена ошибка: служебные поля HTTP запроса " +
                                      "оказались незаполненными. Обратитесь к разработчику."
                    });

            var isNewComment = !string.IsNullOrWhiteSpace(model.NewComment + model.OldComment) &&
                               model.NewComment?.Trim() != model.OldComment;

            if (model.NewName?.Trim() == model.OldName && !isNewComment)
                ModelState.AddModelError("", "Новые введённые данные полностью совпадают с существующими. ");

            if (!ModelState.IsValid) return View(model);

            // готовит запрос и изменяет базу
            var builder = new StringBuilder("update \"Reactives_Providers\" set ");

            var isFirst = true;
            if (model.OldName != model.NewName?.Trim())
            {
                isFirst = false;
                builder.Append($"\"name\" = '{model.NewName?.Trim()}'");
            }

            if (isNewComment)
                builder.Append((isFirst ? null : ", ") + $"\"comments\" = \'{model.NewComment?.Trim()}\' ");

            builder.Append($", \"changed_at_date\" = \'{DateTime.Now.ToSqLiteString()}\'" +
                           $", \"changed_by\" = {this.GetUserId()} ");

            builder.Append($"where \"name\" = \'{model.OldName}\';");

            var done = _connector.WriteData(builder.ToString(), out var error);

            if (done < 1)
            {
                if (error.Contains(Constants.DataBaseUniqueConstraintError))
                    error = $"В базе данных уже существует поставщик с именем \"{model.NewName}\". " +
                            "Названия поставщиков должны быть уникальными.";
                return this.Error("Ошибка базы данных",
                    "При обновлении данных по поставщику в базе данных произошла ошибка. " + error +
                    " Данные не обновлены. ");
            }

            return RedirectToAction("Index", "Providers",
                new ProviderViewFilterModel {FilterName = model.NewName});
        }

        /// <summary> Вход в инструмент удаления поставщика </summary>
        /// <param name="id">Номер поставщика, подлежащего удалению</param>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Delete(int? id)
        {
            // валидация данных
            if (id == null)
                return this.Error("Провал удаления поставщика - нет номера",
                    "При попытке загрузить инструмент для удаления поставщика произошла " +
                    "ошибка из-за отсутствия номера поставщика, подлежащего удалению. " +
                    "Обратитесь к разработчику.");

            // Получает данные из базы
            var data = _connector.ReadData(SelectStart + $" where tbl.\"Id\" = '{id}';", out var error);
            
            if (data == null || data.Rows.Count != 1)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Провал удаления поставщика - неприемлемый ответ базы данных",
                        Description = "При попытке загрузить инструмент для удаления поставщика произошла " +
                                      "ошибка из-за неприемлемого ответа базы данных. " + error +
                                      ((data?.Rows.Count ?? 0) < 1
                                          ? " В базе данных отсутствует указанный поставщик. "
                                          : " В базе данных присутствует больше одного поставщика, с указанным именем. "
                                      )
                                      + "Обратитесь к разработчику."
                    });

            return View(new ProviderInfoModel
            {
                Name = data.Rows[0].GetValue("name"),
                Comment = data.Rows[0].GetValue("comments"),
                RecordIdentity = new DbRecordIdentity(data.Rows[0])
            });

        }

        /// <summary> Подтверждение удаления поставщика </summary>
        /// <param name="model">Модель для удаления поставщика</param>
        [HttpPost]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Delete(ProviderInfoModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.Name))
                return this.Error("Провал удаления поставщика - недостаточно данных",
                    "При удалении поставщика произошла ошибка из-за недостаточно полной информации," +
                    " поступившей в запросе. Обратитесь к разработчику.");

            var done = _connector.WriteData
            (
                $"delete from \"Reactives_Providers\" where \"name\" = \'{model.Name}\';",
                out var error
            );

            if (done < 1)
                return this.Error("Ошибка базы данных",
                    "При удалении поставщика в базе данных произошла ошибка. " + error +
                    " Данные не обновлены. ");

            return RedirectToAction("Index", "Providers");

        }
    }
}
