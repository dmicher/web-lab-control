﻿using System.Data;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication.Models.ReactivesRemainings;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;

namespace WebApplication.Controllers
{
    /// <summary> Отчёты по остаткам реактивов </summary>
    [Authorize]
    public class ReactivesRemainingsController : Controller
    {
        /// <summary>Сервис связи с базой данных</summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструктор запрашивает необхоидмые сервисы</summary>
        /// <param name="connector">Связь с базой данных</param>
        public ReactivesRemainingsController(IDbConnector connector)
            => _connector = connector;
        
        /// <summary> Просмотр и фильтрация остатков </summary>
        /// <param name="model">Модель фильтрации и представления данных </param>
        public IActionResult Index(RemainingsIndexFilterModel model)
        {
            model ??= new RemainingsIndexFilterModel();
            if (model.Limit < 1 || model.Limit > 999) model.Limit = 25;

            if (model.Attributes == null || !model.Attributes.Any())
            {
                if (!_connector.TryReadData("select \"code\", \"name\" from \"Reactives_Attributes\";",
                    out var atrData, out var error))
                    return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);

                model.Attributes = atrData == null || atrData.Rows.Count < 1
                    ? null
                    : atrData.Rows.Cast<DataRow>()
                        .ToDictionary(x => x.GetValue("code")[0], y => y.GetValue("name"));
            }

            if (model.SelectedAttributesOn == null || !model.SelectedAttributesOn.Any())
                model.SelectedAttributesOn = model.Attributes?.ToDictionary(x => x.Key, y => false);

            if (model.SelectedAttributesOff == null || !model.SelectedAttributesOff.Any())
                model.SelectedAttributesOff = model.Attributes?.ToDictionary(x => x.Key, y => false);

            if (model.Specifications == null || !model.Specifications.Any())
            {
                if (!_connector.TryReadData("select \"Id\", \"name\" from \"Reactives_Specifications\";",
                    out var specData, out var error)) 
                    return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);

                model.Specifications = specData == null || specData.Rows.Count < 1
                    ? null
                    : specData.Rows.Cast<DataRow>()
                        .Select(x => new SelectListItem(x.GetValue("name"), x.GetValue("Id"), false));
            }

            var builder = new StringBuilder
            (
                "select " +
                    "s.\"Id\" as \"spec_id\", " +
                    "s.\"name\" as \"spec_name\", " +
                    "sum(a.\"current_state_count\") as \"count_remain\", " +
                    "a.\"start_state_mes_unit_name\" as \"mes_unit\" " +
                "from \"Reactives_Arrivals\" a " +
                    "inner join \"Reactives_Specifications\" s on a.\"LKKSN_specification_id\" = s.\"Id\" " +
                "where a.\"current_state_count\" <> 0 "
            );

            if (model.SelectedAttributesOn != null)
                foreach (var on in model.SelectedAttributesOn.Where(x => x.Value))
                    builder.Append($" and a.\"current_state_attribute_string\" like \'%{on.Key}%\' ");

            if (model.SelectedAttributesOff != null)
                foreach (var off in model.SelectedAttributesOff.Where(x => x.Value))
                    builder.Append(" and (a.\"current_state_attribute_string\" is null " +
                                   $"or a.\"current_state_attribute_string\" not like \'%{off.Key}%\') ");

            if (!string.IsNullOrWhiteSpace(model.InsertedSpecificationFilter))
                builder.Append($" and s.\"upper_name\" like \'%{model.InsertedSpecificationFilter.ToUpper()}%\' ");
            else if (model.SelectedSpecificationFilter != null)
                builder.Append($"and a.\"LKKSN_specification_id\" = {model.SelectedSpecificationFilter}");

            switch (model.IsWrittenOffSelectedFilter)
            {
                case "no":
                    builder.Append(" and a.\"current_state_is_written_off\" = 0 ");
                    break;
                case "yes":
                    builder.Append(" and a.\"current_state_is_written_off\" = 1 ");
                    break;
            }

            builder.Append(" group by a.\"LKKSN_specification_id\", \"start_state_mes_unit_name\" " +
                           $" order by s.\"Id\" limit {model.Limit}; ");

            if (!_connector.TryReadData(builder.ToString(), out var data, out var gError))
                return this.Error("Ошибка базы данных", "При загрузке данных по выбранным фильтрам произошла ошибка. " + gError);

            if (data == null || data.Rows.Count < 1)
                return View(model);

            model.Rows = data.Rows.Cast<DataRow>()
                .Select(x => new RemainingsIndexResultRowModel
                {
                    SpecificationNumber = x.GetInt("spec_id"),
                    SpecificationName = x.GetValue("spec_name"),
                    Counts = data.Rows.Cast<DataRow>()
                        .Where(y => y.GetInt("spec_id") == x.GetInt("spec_id"))
                        .Select(y => new RemainingsIndexResultRowModel.RemainingCount
                        {
                            Value = y.GetDecimal("count_remain"),
                            MesUnit = y.GetValue("mes_unit")
                        })
                }).Distinct();

            return View(model);
        }

        /// <summary> Детали по остаткам выбранной спецификации </summary>
        /// <param name="id">Номер спецификации</param>
        public IActionResult Details(int? id)
        {
            if (id == null)
                return this.Error("Системная ошибка", 
                    "Остатки, детали. Запрос не содержит номера спецификации. Обратитесь к разработчику.");

            var specName = _connector.GetValue($"select \"name\" from \"Reactives_Specifications\" where \"Id\" = {id}", out var error);
            if (string.IsNullOrWhiteSpace(specName))
                return this.Error("Ошибка базы данных",
                    "Остатки, детали. Пои поиске спецификации в базе данных произошла ошибка. " + error);

            if (!_connector.TryReadData
            (
                "select " +
                    "\"LKKSN\" as \"lkksn\", " +
                    "\"current_state_count\" as \"count\", " +
                    "\"start_state_mes_unit_name\" as \"mes_units\", " +
                    "\"current_state_stored_at\" as \"stored_at\", " +
                    "\"current_state_is_written_off\" as \"is_written_off\" " +
                "from \"Reactives_Arrivals\" " +
                $"where \"LKKSN_specification_id\" = {id} and \"current_state_count\" <> 0",
                out var data,
                out error
            ))
                return this.Error("Ошибка базы данных",
                    "Остатки, детали. При поиске информации в базе данных произошла ошибка. " + error);

            var model = new RemainingsDetailsModel
            {
                SpecificationId = (int) id,
                SpecificationName = specName
            };

            if (data == null || data.Rows.Count < 1)
                return View(model);

            model.Arrivals = data.Rows.Cast<DataRow>()
                .Select(x => new RemainingsDetailsModel.ArrivalInfo
                {
                    Lkksn = x.GetValue("lkksn"),
                    Count = x.GetDecimal("count"),
                    MesUnits = x.GetValue("mes_units"),
                    StoredAt = x.GetValue("stored_at"),
                    IsWrittenOff = x.GetValue("is_written_off") == "1"
                });

            return View(model);
        }

    }
}
