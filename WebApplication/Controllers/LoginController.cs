﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using WebApplication.Models.Login;
using WebApplication.Tools.DbConnector;


namespace WebApplication.Controllers
{
    /// <summary> Контроллер входа в веб-приложение </summary>
    public class LoginController : Controller
    {
        /// <summary> Подключатель к базе данных </summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструктор запрашивает сервисы</summary>
        /// <param name="connector">Сервис подключения к базе данных</param>
        public LoginController(IDbConnector connector) => _connector = connector;

        /// <summary> Первый перевод на страницу</summary>
        [HttpGet]
        public IActionResult Index() => View();

        /// <summary> Передача данных для аутентификации</summary>
        /// <param name="model">Модель данных</param>
        [HttpPost] [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Index(LoginModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var done = _connector.TryReadData
            (
                "select u.\"Id\", u.\"name\", g.\"name\" as \"group\" " +
                "from \"Users\" u " +
                "inner join \"Users_Groups\" g on u.\"group_id\" = g.\"Id\" " +
                $"where u.\"login\" = \'{model.Name}\' and u.\"password\" = \'{model.Password}\' " +
                "and g.\"name\" <> \"Заблокированный\"",
                out var data,
                out var message
            );

            if (!done)
            {
                ModelState.AddModelError("", "Ошибка базы данных. " + message);
                return View(model);
            }

            if (data == null || !data.Columns.Contains("name") || data.Rows.Count < 1)
            {
                ModelState.AddModelError("", "Некорректные логин и/или пароль. ");
                return View(model);
            }

            var id = data.Rows[0]["Id"]?.ToString()?.Trim();
            var name = data.Rows[0]["name"]?.ToString()?.Trim();
            var role = data.Rows[0]["group"]?.ToString()?.Trim();

            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(role))
            {
                ModelState.AddModelError("", "Ошибка структуры базы данных: обнаружены недопустимо пустые поля.");
                return View(model);
            }

            await Authenticate(id, name, role);
            return RedirectToAction("Index", "Home");

        }

        /// <summary> Аутентифицирует пользователя по имени и роли </summary>
        /// <param name="id">Номер пользователя в базе данных</param>
        /// <param name="userName">имя пользователя</param>
        /// <param name="userRole">роль пользователя</param>
        private async Task Authenticate(string id, string userName, string userRole)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, userRole),
                new Claim("user_id", id)
            };
            var identity = new ClaimsIdentity
            (
                claims,
                "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType
            );
            await HttpContext.SignInAsync
            (
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(identity)
            );
        }

        /// <summary> Отменяет аутентификацию пользователя </summary>
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Login");
        }
    }
}
