﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.MesUnits;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;

namespace WebApplication.Controllers
{
    /// <summary> Управление единицами измерения </summary>
    [Authorize(Roles = "Администратор, Создатель")]
    public class MesUnitsController : Controller
    {
        /// <summary> Сервис подключения к базе данных </summary>
        private readonly IDbConnector _connector;

        /// <summary>Конструктор запрашивает сервисы</summary>
        /// <param name="connector">Сервис подключения к базе данных</param>
        public MesUnitsController(IDbConnector connector)
            => _connector = connector;

        /// <summary> Просмотр единиц измерения </summary>
        public IActionResult Index()
        {
            if (!_connector.TryReadData("select \"name\", \"short\", \"position\" from \"Reactives_Mes_Units\" " +
                                       "order by \"position\", \"name\";",
                out var data, out var error))
                return this.Error("Ошибка базы данных",
                    "Данные о зарегистрированных единицах измерения не загружены. " + error +
                                  " Обратитесь к разработчику.");

            if (data == null || data.Rows.Count < 1)
                return View();

            Dictionary<string, int> usedUnits;

            try
            {
                usedUnits = UsedUnits();
            }
            catch (Exception e)
            {
                return this.Error("Ошибка базы данных",
                    "Данные об использованных единицах измерения не загружены. " + e.Message +
                                  " Обратитесь к разработчику.");
            }

            var model = data.Rows.Cast<DataRow>()
                .Select(x => new MesUnitModel
                {
                    Name = x.GetValue("name"),
                    Short = x.GetValue("short"),
                    Position = int.TryParse(x.GetValue("position"), out var i) ? i : 0,
                    UseCount = usedUnits == null 
                        ? 0
                        : usedUnits.ContainsKey(x.GetValue("name"))
                            ? usedUnits[x.GetValue("name")]
                            : 0
                });

            return View(model);
        }

        /// <summary> Вход в инструмент добавления </summary>
        [HttpGet]
        public IActionResult Add() => View();

        /// <summary> Подтверждение добавления </summary>
        /// <param name="model">Модель данных по единице измерения</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Add(MesUnitModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var done = _connector.WriteData
            (
                "insert into \"Reactives_Mes_Units\" (\"name\", \"short\", \"position\") " +
                $"values ('{model.Name}', '{model.Short}', {model.Position});",
                out var error
            );

            if (done > 0) return RedirectToAction("Index", "MesUnits");

            if (error.Contains(Constants.DataBaseUniqueConstraintError))
                error = $"В базе данных уже существует единица измерения с именем \"{model.Name}\". " +
                        "Названия должны быть уникальными.";

            return this.Error("Ошибка базы данных",
                "При добавлении единицы измерения в базу данных произошла ошибка. " + error +
                              " Данные не обновлены. ");

        }

        /// <summary> Вход в инструмент правки </summary>
        /// <param name="name">Имя единицы измерения</param>
        [HttpGet]
        public IActionResult Edit(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return this.Error("Имя не указано",
                    "При попытке загрузить инструмент изменения единицы измерения произошла " +
                                  "ошибка из-за отсутствия имени единицы измерения в запросе. Обратитесь к " +
                                  "разработчику");

            var data = _connector.ReadData(
                $"select \"short\", \"position\" from \"Reactives_Mes_Units\" where \"name\" = '{name}';",
                out var error);

            if (data == null || data.Rows.Count < 1)
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить инструмент изменения единицы измерения произошла " +
                                  "ошибка - получено неприемлемое количество данных в ответе базы данных. " +
                                  error + "Обратитесь к разработчику.");

            var shortName = data.Rows[0].GetValue("short");
            var position = int.TryParse(data.Rows[0].GetValue("position"), out var i) ? i : 0;

            return View(new MesUnitEditModel
            {
                OldName = name,
                NewName = name,
                OldShort = shortName,
                NewShort = shortName,
                OldPosition = position,
                NewPosition = position
            });
        }

        /// <summary>Подтверждение правки</summary>
        /// <param name="model">Модель исправления</param>
        [HttpPost] 
        [AutoValidateAntiforgeryToken]
        public IActionResult Edit(MesUnitEditModel model)
        {
            if (string.IsNullOrWhiteSpace(model.OldName))
                return this.Error("Ошибка запроса",
                    "В запросе отсутствует необходимый компонент \'Старое имя\'. Обратитесь к разработчику");

            if (model.OldName == model.NewName.Trim()
                && model.OldShort == model.NewShort.Trim()
                && model.OldPosition == model.NewPosition)
                ModelState.AddModelError("", "Введённые новые данные полностью совпадают с уже установленными");

            if (!ModelState.IsValid) return View(model);

            var builder = new StringBuilder("update \"Reactives_Mes_Units\" set ");
            var isFirst = true;
            string AndWhere()
            {
                if (!isFirst) return ", ";
                isFirst = false;
                return null;
            }

            if (model.OldName != model.NewName)
                builder.Append(AndWhere() + $"\"name\" = \'{model.NewName.Trim()}\'" );
            if (model.OldShort != model.NewShort)
                builder.Append(AndWhere() + $"\"short\" = \'{model.NewShort.Trim()}\' ");
            if (model.OldPosition != model.NewPosition)
                builder.Append(AndWhere() + $"\"position\" = {model.NewPosition} ");

            builder.Append($" where \"name\" = \'{model.OldName}\';");

            var done = _connector.WriteData(builder.ToString(), out var error);

            if (done > 0) return RedirectToAction("Index", "MesUnits");

            if (error.Contains(Constants.DataBaseUniqueConstraintError))
                error = $"В базе данных уже существует единица измерения с именем \"{model.NewName}\". " +
                        "Названия должны быть уникальными.";

            return this.Error("Ошибка базы данных",
                "При изменении единицы измерения в базу данных произошла ошибка. " + error +
                              " Данные не обновлены. ");
        }

        /// <summary>Вход в инструмент удаления</summary>
        /// <param name="name">Название единицы измерения</param>
        [HttpGet]
        public IActionResult Delete(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return this.Error("Имя не указано",
                    "При попытке загрузить инструмент удаления единицы измерения произошла " +
                                  "ошибка из-за отсутствия имени единицы измерения в запросе. Обратитесь к " +
                                  "разработчику");

            var data = _connector.ReadData(
                $"select \"short\", \"position\" from \"Reactives_Mes_Units\" where \"name\" = '{name}';",
                out var error);

            if (data == null || data.Rows.Count < 1)
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить инструмент удаления единицы измерения произошла " +
                                  "ошибка: получено неприемлемое количество данных в ответе базы данных. " +
                                  error + "Обратитесь к разработчику.");

            var usedUnits = UsedUnits();

            if (usedUnits?.Any(x => x.Key == name && x.Value != 0) ?? false)
                return this.Error("Системная ошибка",
                    "При попытке загрузить инструмент удаления единицы измерения произошла " +
                                  "ошибка: удаляемая единица измерения уже использована в базе данных. " +
                                  "Удаление используемых единиц измерения неприемлемо.");

            return View(new MesUnitModel
            {
                Name = name,
                Short = data.Rows[0].GetValue("short"),
                Position = int.TryParse(data.Rows[0].GetValue("position"), out var i) ? i : 0,
                UseCount =  usedUnits?.Where(x => x.Key == name)
                    .Select(x => x.Value)
                    .FirstOrDefault() ?? 0
            });
        }

        /// <summary>Подтверждение удаления</summary>
        /// <param name="model">Модель, подлежащая удалению</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Delete(MesUnitModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Name))
                return this.Error("Ошибка запроса",
                    "В запросе отсутствует необходимый компонент \'Имя\'. Обратитесь к разработчику");

            var done = _connector.WriteData(
                $"delete from \"Reactives_Mes_Units\" where \"name\" = '{model.Name}';",
                out var error);

            if (done > 0) return RedirectToAction("Index", "MesUnits");

            return this.Error("Ошибка базы данных",
                "При удалении единицы измерения в базу данных произошла ошибка. " + error +
                              " Обратитесь к разработчику");
        }

        /// <summary>Получает из базы список всех использованных единиц измерений</summary>
        /// <returns>Набор строк с названиями использованных хоть раз единиц измерений</returns>
        /// <exception cref="Exception">При ошибке базы данных</exception>
        private Dictionary<string, int> UsedUnits()
        {
            if (!_connector.TryReadData
            (
                "select \"unit\", sum(\"count\") as \"count\" from " +
                    "(select \"producer_reactive_mes_unit_name\" as \"unit\", count(\"producer_reactive_mes_unit_name\") as \"count\" " +
                    "from \"Reactives_Arrivals\" where \"unit\" is not null group by \"producer_reactive_mes_unit_name\" " +
                    "union " +
                    "select \"current_state_mes_unit_name\" as \"unit\", count(\"current_state_mes_unit_name\") as \"count\" " +
                    "from \"Reactives_Arrivals\" group by \"current_state_mes_unit_name\") " +
                "group by \"unit\";",
                out var data,
                out var error
            ))
            {
                throw new Exception(error);
            }

            return data?.Rows.Cast<DataRow>().ToDictionary
            (
                x => x.GetValue("unit"),
                y => int.TryParse(y.GetValue("count"), out var i) ? i : 1
            );
        }

    }
}
