﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using System.IO.Compression;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.DbRestore;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;
using WebApplication.Tools.Settings;

namespace WebApplication.Controllers
{
    /// <summary> Управление резервными копиями базы данных </summary>
    [Authorize(Roles = "Создатель")]
    public class DbRestoreController : Controller
    {
        /// <summary> Сервис настроек </summary>
        private readonly ISettingsHandler _settings;

        /// <summary> Сервис подключения к базе данных </summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструктор запрашивает сервисы </summary>
        /// <param name="settings">Сервис настроек</param>
        /// <param name="connector">Сервис базы данных</param>
        public DbRestoreController(ISettingsHandler settings, IDbConnector connector)
        {
            _settings = settings;
            _connector = connector;
        }

        /// <summary> Просмотр резервных копий и функционала </summary>
        public IActionResult Index()
        {
            // загружает данные из базы
            var isOk = _connector.TryReadData
            (
                "select \"Id\", \"name\", \"size\", \"created_at\", \"comment\" from \"DbRestore\"",
                out var data,
                out var error
            );

            if (!isOk)
                return this.Error("Ошибка базы данных",
                    "Загрузка данных о существующих резервных копиях провалилась. " + error);

            var backUps = data == null 
                ? new List<DbRestoreFileInfoModel>()
                : data.Rows.Cast<DataRow>()
                .Select(x => new DbRestoreFileInfoModel
                {
                    Id = int.TryParse(x["Id"]?.ToString()?.Trim(), out var i) ? i : -1,
                    Name = x["name"]?.ToString()?.Trim(),
                    Size = uint.TryParse(x["size"]?.ToString()?.Trim(), out var ui) ? ui : 0,
                    CreationDateTime = DateTime.TryParse(x["created_at"]?.ToString()?.Trim(), out var date)
                        ? date
                        : DateTime.MinValue,
                    Comment = x["comment"]?.ToString()?.Trim()
                }).ToList();

            // сверяет (и коррестирует, если нужно) данные из базы с данными файловой системы
            var files = new DirectoryInfo(BackUpDirectory).GetFiles("*.dbak", SearchOption.TopDirectoryOnly);
            var isAnyChanges = false;
            foreach (var file in files) // случай: есть файл, но нет данных в базе
            {
                if (backUps.Any(x => file.Name == x.Name)) continue;
                isAnyChanges = true;
                var result = _connector.WriteData
                (
                    "insert into \"DbRestore\" (\"name\", \"size\", \"created_at\", \"comment\") " +
                    $"values (\'{file.Name}\', {file.Length}, \'{file.CreationTime:yyyy-MM-dd HH:mm:ss.fff}\', " +
                    "\'[авто] Из файловой системы\')",
                    out error
                );
                if (result < 1) this.Error("Ошибка связанности данных",
                    "При проверке существующих файлов резервных копий обнаружена неучтённая копия. " +
                                  "При попытке внедрения её в систему учёта произошла ошибка базы данных. " + error +
                                  " Обратитесь к разработчику.");
            }

            foreach (var record in backUps.Where(record => files.All(file => record.Name != file.Name))) // случай: есть в базе, нет файла
            {
                isAnyChanges = true;
                var result = _connector.WriteData
                (
                    $"delete from \"DbRestore\" where Id = {record.Id};",
                    out error
                );
                if (result < 0) this.Error("Ошибка связанности данных",
                    "При проверке существующих файлов резервных копий не обнаружена копия для существующей учётной записи. " +
                                  "При попытке удаления записи из системы учёта произошла ошибка базы данных. " + error +
                                  " Обратитесь к разработчику.");
            }

            // рекурсивно вызывает себя, если что-то менялось (по идее, глубина рекурсии не более 1)
            if (isAnyChanges) return RedirectToAction("Index"); 
            
            return View(backUps);
        }

        /// <summary> Вход в создание резервной копии </summary>
        [HttpGet]
        public IActionResult Create() => View();

        /// <summary> Подтверждение создания резервной копии </summary>
        /// <param name="model">Модель данных для создания резервной копии</param>
        [HttpPost]
        public IActionResult Create(DbRestoreCreateModel model)
        {
            if (!ModelState.IsValid) return View(model);
            // проверяет пароль пользователя
            if (!CheckPassword(model.RootPassword, out var error))
            {
                if (!string.IsNullOrWhiteSpace(error))
                    this.Error("Ошибка базы данных",
                        "При попытке проверки пароля технического пользователя произошла ошибка. " +
                                      error + " Обратитесь к разработчику.");

                ModelState.AddModelError("", "Введён неверный пароль технического пользователя.");
                return View(model);
            }

            // записывает в себя новую резервную копию и её создаёт файл
            var dir = BackUpDirectory;
            string backUpFullPath;
            string backUpName;
            do
            {
                backUpName = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "=" + 
                             model.Comment.Replace(" ", "_") + "=" + 
                             Path.GetRandomFileName().Remove(4) +
                             ".dbak";
                backUpFullPath = Path.Combine(dir,  backUpName);
            } while (System.IO.File.Exists(backUpFullPath));

            var curBase = new FileInfo(CurrentBaseFile);

            var record = _connector.WriteData
            (
                "insert into \"DbRestore\" (\"name\", \"size\", \"created_at\", \"comment\") " +
                $"values (\'{backUpName}\', {curBase.Length}, \'{DateTime.Now.ToSqLiteString()}\', \'{model.Comment}\');",
                out error
            );

            if (record != 1)
                return this.Error("Ошибка базы данных",
                    "При регистрации резервной копии в базе данных произошла ошибка. " + error +
                    " Резервная копия не создана.");

            curBase.CopyTo(backUpFullPath, true);

            if (System.IO.File.Exists(backUpFullPath)) return RedirectToAction("Index", "DbRestore");

            if(_connector.TryWriteData($"delete from \"DbRestore\" where \"name\" = \'{backUpName}\';", out _, out error))
                return this.Error("Комплексная ошибка",
                    "1. При создании резервной копии базы данных произошла непредвиденная ошибка, из-за " +
                                  "которой файл не был создан в ожидаемом месте. 2. При попытке изъять созданную " +
                                  "запись о не созданной резервной копии произошла ошибка. " + error +
                                  " Обратитесь к разработчику.");

            return this.Error("Ошибка файловой системы",
                "При создании резервной копии базы данных произошла непредвиденная ошибка, из-за " +
                              "которой файл не был создан в ожидаемом месте. Обратитесь к разработчику.");

        }
        
        /// <summary> Скачать указанную резервную копию </summary>
        /// <param name="id">Номер резервной копии</param>
        public IActionResult Download(int? id)
        {
            if (id == null || id <= 0)
                return this.Error("Номер копии не задан",
                    "При попытке загрузить резервную копию произошла ошибка из-за того," +
                                  " что номер резервной копии не задан или задан не верно. Обратитесь " +
                                  "к разработчику.");

            var name = _connector.GetValue($"select \"name\" from \"DbRestore\" where \"Id\" = {id};", out var error);
            if (!string.IsNullOrWhiteSpace(error) || string.IsNullOrWhiteSpace(name))
                return this.Error("Резервная копия не зарегистрирована",
                    "Резервная копия с номером " + id + " не зарегистрирована в базе данных, либо " +
                                  " произошла ошибка базы данных. " + error + "Обратитесь к разработчику.");

            var path = Path.Combine
            (
                AppDomain.CurrentDomain.BaseDirectory ?? "",
                _settings.GetValue(SettingsNames.DbBackUpBasePath),
                name
            );

            if (System.IO.File.Exists(path))
                return File(System.IO.File.ReadAllBytes(path), "application/dbak",
                    $"BackUp_{id}_{DateTime.Now:dd-MM-yyyy}.dbak");

            var count = _connector.WriteData($"delete from \"DbRestore\" where Id = {id};", out error);
            if (count == 1)
                return this.Error("Резервная копия не существует",
                    "Зарегистрированная резервная копия не была обнаружена в файловой системе. Регистрационная запись удалена.");

            return this.Error("Ошибка базы данных",
                "Зарегистрированная копия не была обнаружена в файловой системе. При попытке удаления регистрационной " +
                "записи произошла ошибка. " + error + " Обратитесь к разработчику.");

        }

        /// <summary> Вход в инструмент загрузки ранее созданных резервных копий</summary>
        [HttpGet]
        public IActionResult Upload() => View();

        /// <summary> Добавление файла резервной копии </summary>
        /// <param name="model">Модель загружаемого файал</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Upload(DbRestoreUploadFileModel model)
        {
            if (!ModelState.IsValid) return View(model);
            // проверка пароля
            if (!CheckPassword(model.RootPassword, out var error))
            {
                if (!string.IsNullOrWhiteSpace(error))
                    return this.Error("Ошибка базы данных",
                        "При попытке проверки пароля технического пользователя произошла ошибка. " +
                                      error + " Обратитесь к разработчику.");

                ModelState.AddModelError("", "Введён неверный пароль технического пользователя.");
                return View(model);
            }

            // краткая проверка файла
            if (!model.UploadedFile.FileName.EndsWith(".dbak"))
            {
                ModelState.AddModelError("", "Загружаемый файл не является файлом резервной копии \"dbak\".");
                return View(model);
            }

            // сохраняет загруженный файл
            var dir = BackUpDirectory;
            string backUpFullPath;
            string backUpName;
            do
            {
                backUpName = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") +
                             "=Загруженная_копия=" +
                             Path.GetRandomFileName().Remove(4) + 
                             ".dbak";
                backUpFullPath = Path.Combine(dir, backUpName);
            } while (System.IO.File.Exists(backUpFullPath));

            using var stream = new FileStream(backUpFullPath, FileMode.Create);
            model.UploadedFile.CopyTo(stream);
            stream.Dispose();
            
            if (!System.IO.File.Exists(backUpFullPath))
                return this.Error("Ошибка файловой системы",
                    "При сохранении резервной копии базы данных произошла непредвиденная ошибка, из-за " +
                                  "которой файл не был создан в ожидаемом месте. Обратитесь к разработчику.");

            var file = new FileInfo(backUpFullPath);
            var record = _connector.WriteData
            (
                "insert into \"DbRestore\" (\"name\", \"size\", \"created_at\", \"comment\") " +
                $"values (\'{backUpName}\', {file.Length}, \'{file.CreationTime:yyyy-MM-dd HH:mm:ss.fff}\', \'[авто] Загружен из файла: {model.UploadedFile.FileName}\');",
                out error
            );

            if (record != 1) // провал
            {
                System.IO.File.Delete(backUpFullPath); // откатывает изменение в файловой системе, если база отказалась работать
                return this.Error("Ошибка базы данных",
                    "При регистрации резервной копии в базе данных произошла ошибка. " + error +
                                  " Резервная копия не была сохранена.");
            }
            return RedirectToAction("Index", "DbRestore"); // успех
        }

        /// <summary> Вход в инструмент удаления резервной копии </summary>
        /// <param name="id">Номер удаляемой резервной копии</param>
        [HttpGet]
        public IActionResult Delete(int? id)
        {
            if (id == null)
                return this.Error("Номер резервной копии не указан",
                    "При загрузке инструмента удаления резервной копии произошла " +
                                  "ошибка из-за того, что номер резервной копии не был указан. " +
                                  "Обратитесь к разработчику.");

            var data = _connector.ReadData
            (
                $"select \"name\", \"created_at\", \"comment\" from \"DbRestore\" where \"id\" = {id};",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Резервная копия на обнаружена",
                    "При попытке загрузить данные по удаляемой резервной копии " +
                                  "произошла ошибка. " + error + " Данные не обнаружены.");

            string GetValue(string name) => data.Rows[0][name]?.ToString()?.Trim();

            return View(new DbRestoreDeleteModel
            {
                Id = (int) id,
                Name = GetValue("name"),
                Comment = GetValue("comment"),
                CreatedAt = DateTime.TryParse(GetValue("created_at"), out var date) ? date : DateTime.MinValue
            });
        }

        /// <summary> Подтверждение удаления резервной копии</summary>
        /// <param name="model">Модель команды к удалению резервной копии</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Delete(DbRestoreDeleteModel model)
        {
            if (!ModelState.IsValid) return View(model);
            // проверка пароля
            if (!CheckPassword(model.RootPassword, out var error))
            {
                if (!string.IsNullOrWhiteSpace(error))
                    return this.Error("Ошибка базы данных",
                        "При попытке проверки пароля технического пользователя произошла ошибка. " +
                                      error + " Обратитесь к разработчику.");

                ModelState.AddModelError("", "Введён неверный пароль технического пользователя.");
                return View(model);
            }

            var result =
                _connector.WriteData(
                    $"delete from \"DbRestore\" where \"Id\" = {model.Id} and \"name\" = \'{model.Name}\';", 
                    out error);

            if (result < 1)
                return this.Error("Ошибка базы данных",
                    "При удалении регистрационной записи о резервной копии из базы данных произошла " +
                                  "ошибка. " + error + " Данные не удалены.");

            var path = Path.Combine(BackUpDirectory, model.Name);
            if (System.IO.File.Exists(path)) System.IO.File.Delete(path);

            return RedirectToAction("Index", "DbRestore");
        }

        /// <summary> Вход в инструмент восстановления резервной копии </summary>
        /// <param name="id">Номер резервной копии</param>
        [HttpGet]
        public IActionResult Restore(int? id)
        {
            if (id == null)
                return this.Error("Номер резервной копии не указан",
                    "При загрузке инструмента удаления резервной копии произошла " +
                                  "ошибка из-за того, что номер резервной копии не был указан. " +
                                  "Обратитесь к разработчику.");

            var data = _connector.ReadData
            (
                $"select \"name\", \"created_at\", \"comment\" from \"DbRestore\" where \"id\" = {id};",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Резервная копия на обнаружена",
                    "При попытке загрузить данные по удаляемой резервной копии " +
                                  "произошла ошибка. " + error + " Данные не обнаружены.");

            string GetValue(string name) => data.Rows[0][name]?.ToString()?.Trim();

            return View(new DbRestoreDeleteModel
            {
                Id = (int) id,
                Name = GetValue("name"),
                Comment = GetValue("comment"),
                CreatedAt = DateTime.TryParse(GetValue("created_at"), out var date) ? date : DateTime.MinValue
            });
        }

        /// <summary> Подтверждение восстановления резервыной копии </summary>
        /// <param name="model">Модель команды к восстановлению резервной копии</param>
        [HttpPost] 
        [AutoValidateAntiforgeryToken]
        public IActionResult Restore(DbRestoreDeleteModel model)
        {
            if (!ModelState.IsValid) return View(model);
            // проверка пароля
            if (!CheckPassword(model.RootPassword, out var error))
            {
                if (!string.IsNullOrWhiteSpace(error))
                    return this.Error("Ошибка базы данных",
                        "При попытке проверки пароля технического пользователя произошла ошибка. " +
                                      error + " Обратитесь к разработчику.");

                ModelState.AddModelError("", "Введён неверный пароль технического пользователя.");
                return View(model);
            }

            var restoreFile = Path.Combine(BackUpDirectory, model.Name);

            if (!System.IO.File.Exists(restoreFile))
                return this.Error("Ошибка файловой системы",
                    "Файл выбранной резервной копии не обнаружен. Обратитесь к разработчику");

            try
            {
                System.IO.File.Copy(restoreFile, CurrentBaseFile, true);
            }
            catch (Exception e)
            {
                return this.Error("Ошибка восстановления",
                    "Во время восстановления базы данных из резервной копии возникла ошибка. " +
                                  e.Message + "Данные не были изменены");
            }

            return RedirectToAction("Index");
        }

        /// <summary> Вход в инструмент по изменению комментария к резервной копии </summary>
        /// <param name="id">Номер резервной копии</param>
        [HttpGet]
        public IActionResult ReComment(int? id)
        {
            if (id == null)
                return this.Error("Номер резервной копии не указан",
                    "При загрузке инструмента удаления резервной копии произошла " +
                                  "ошибка из-за того, что номер резервной копии не был указан. " +
                                  "Обратитесь к разработчику.");

            var data = _connector.ReadData
            (
                $"select \"name\", \"created_at\", \"comment\" from \"DbRestore\" where \"id\" = {id};",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Резервная копия на обнаружена",
                    "При попытке загрузить данные по удаляемой резервной копии " +
                                  "произошла ошибка. " + error + " Данные не обнаружены.");

            string GetValue(string name) => data.Rows[0][name]?.ToString()?.Trim();

            var comment = GetValue("comment");
            return View(new DbRestoreReCommentModel
            {
                Id = (int) id,
                Name = GetValue("name"),
                OldComment = comment,
                NewComment = comment,
                CreatedAt = DateTime.TryParse(GetValue("created_at"), out var date) ? date : DateTime.MinValue
            });

        }

        /// <summary> Подтверждение изменения комментария к резервной копии</summary>
        /// <param name="model">Модель данных для изменения комментария</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult ReComment(DbRestoreReCommentModel model)
        {
            if (!ModelState.IsValid) return View(model); // тут бесполезно
            var result =
                _connector.WriteData(
                    $"update \"DbRestore\" set \"comment\" = '{model.NewComment}' where \"Id\" = {model.Id};",
                    out var error);

            if (result < 1)
                return this.Error("Ошибка базы данных",
                    "При попытке изменить комментарий к резервной копии произошла ошибка. " + error +
                                  " Данные не были изменены");

            return RedirectToAction("Index");
        }

        /// <summary> Вход в инструмент по импорту пользователей из старой программы</summary>
        [HttpGet]
        public IActionResult ImportUsers() => View();

        /// <summary> Передача данных от старой программы</summary>
        /// <param name="model">Модель данных для добавления</param>
        [HttpPost] 
        [AutoValidateAntiforgeryToken]
        public IActionResult ImportUsers(DbRestoreImportModel model)
        {
            // приём модели
            if (model == null)
                return this.Error("Системная ошибка", "Модель данных не передана с запросом");

            if (!CheckPassword(model.RootPassword, out var error))
                return this.Error("Ошибка авторизации", 
                    "Ошибка проверки пароля. " + error + 
                    " Ваш вход в систему будет прерван. ", true);

            if (model.UsersFile == null)
            {
                return View(model);
            }

            // запись файла в систему
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? "",
                _settings.GetValue(SettingsNames.TempFilesPath));

            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            string tmpFile;
            do
            {
                tmpFile = Path.Combine(path, Path.GetRandomFileName().Replace(".", null) + ".tmp");
            } while (System.IO.File.Exists(tmpFile));

            using var stream = System.IO.File.Create(tmpFile);
            model.UsersFile.CopyTo(stream);
            stream.Close();

            if (!System.IO.File.Exists(tmpFile))
                return this.Error("Системная ошибка", 
                    "Входящий файл не записан в систему. " +
                    "Если ошибка повторяется, обратитесь к разработчику");

            // чтение и валидация файла, подготовка sql запроса
            var lines = System.IO.File.ReadAllLines(tmpFile)
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToArray();
            var request = new StringBuilder("insert into \"Users\" (\"name\", \"login\", \"password\", \"group_id\") values ");
            var isFirst = true;
            var count = lines.Length;

            foreach (var line in lines)
            {
                var values = line.Split(";");
                if (values.Length != 4)
                    return this.Error("Ошибка формата файла",
                        "Одна из строк входного файла имеет неверный формат данных: количество значений в строке должно быть 4." +
                        "Операция импорта отменена.");

                var group = int.TryParse(values[3].Trim(), out var i) ? i : -1;
                if (group != 2 && group != 3)
                    return this.Error("Ошибка формата файла",
                        "Одна из строк входного файла имеет неверный формат данных: поле группы должно иметь значение 2 или 3." +
                        "Операция импорта отменена.");

                if (isFirst) isFirst = false;
                else request.Append(", ");

                request.Append($" (\'{values[0].Trim()}\', \'{values[1].Trim()}\', \'{values[2].Trim()}\', {group}) ");
            }

            request.Append(';');

            var result = _connector.WriteData(request.ToString(), out error);

            if (result < 1)
                return this.Error("Ошибка базы данных", "При добавлении данных произошла ошибка. " + error);

            foreach (var f in Directory.GetFiles(path).Where(x => DateTime.Now.Subtract(System.IO.File.GetCreationTime(path)).Days > 1))
                System.IO.File.Delete(f);

            return RedirectToAction("SingleImportResult", new DbRestoreImportSingleResultModel
            {
                Title = "Пользователи успешно добавлены",
                SuccessString = $"Импорт пользователей успешно завершён. Всего пользователей добавлено: {count}."
            });
        }

        /// <summary> Простой вывод сообщения об успехе</summary>
        /// <param name="model">модель данных для вывода информации</param>
        public IActionResult SingleImportResult(DbRestoreImportSingleResultModel model) => View(model);

        /// <summary> Вход в инструмент импортирования спецификаций</summary>
        [HttpGet]
        public IActionResult ImportSpecs() => View();

        /// <summary> Передача данных от старой программы </summary>
        /// <param name="model">Модель данных для добавления</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult ImportSpecs(DbRestoreImportModel model)
        {
            
            // приём модели
            if (model == null)
                return this.Error("Системная ошибка", "Модель данных не передана с запросом");

            if (!CheckPassword(model.RootPassword, out var error))
                return this.Error("Ошибка авторизации", 
                    "Ошибка проверки пароля. " + error + 
                    " Ваш вход в систему будет прерван. ", true);

            if (model.UsersFile == null)
            {
                return View(model);
            }

            // запись файла в систему
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? "",
                _settings.GetValue(SettingsNames.TempFilesPath));

            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            string tmpFile;
            do
            {
                tmpFile = Path.Combine(path, Path.GetRandomFileName().Replace(".", null) + ".tmp");
            } while (System.IO.File.Exists(tmpFile));

            using var stream = System.IO.File.Create(tmpFile);
            model.UsersFile.CopyTo(stream);
            stream.Close();

            if (!System.IO.File.Exists(tmpFile))
                return this.Error("Системная ошибка", 
                    "Входящий файл не записан в систему. " +
                    "Если ошибка повторяется, обратитесь к разработчику");

            // чтение и валидация файла, подготовка sql запроса
            var lines = System.IO.File.ReadAllLines(tmpFile)
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToArray();

            var request = new StringBuilder("insert into \"Reactives_Specifications\" (\"Id\", \"name\", \"upper_name\", " +
                                            "\"created_at_date\", \"created_by\", \"changed_at_date\", \"changed_by\") values ");
            var isFirst = true;
            var user = this.GetUserId();
            var now = DateTime.Now.ToSqLiteString();
            var count = lines.Length;

            foreach (var line in lines)
            {
                var values = line.Split(";");
                if (values.Length != 2)
                    return this.Error("Ошибка формата данных",
                        "Одна из строк файла имела не верный формат: количество значений должно быть 2. " +
                        "Операция импорта отменена.");

                var id = int.TryParse(values[0].Trim(), out var i) ? i : -1;
                if (id < 0)
                    return this.Error("Ошибка формата данных", 
                        "Одна из строк имела не верный форма: номер спецификации должен быть числом. " +
                        "Операция импорта отменена.");

                var name = values[1].Trim();

                if (isFirst) isFirst = false;
                else request.Append(", ");

                request.Append($" ({id}, \'{name}\', \'{name.ToUpper()}\', \'{now}\', {user}, \'{now}\', {user}) ");
            }

            request.Append(";");

            var result = _connector.WriteData(request.ToString(), out error);

            if (result <= 0)
                return this.Error("Ошибка базы данных", "При добавлении данных произошла ошибка. " + error);

            foreach (var f in Directory.GetFiles(path).Where(x => DateTime.Now.Subtract(System.IO.File.GetCreationTime(path)).Days > 1))
                System.IO.File.Delete(f);

            return RedirectToAction("SingleImportResult", new DbRestoreImportSingleResultModel
            {
                Title = "Спецификации успешно добавлены",
                SuccessString = $"Импорт спецификаций успешно завершён. Всего добавлено спецификаций: {count}."
            });
        }

        /// <summary> Вход в инструмент импортирования поставок </summary>
        [HttpGet]
        public IActionResult ImportArrivals() => View();

        /// <summary> Передава данных от старой программы</summary>
        /// <param name="model">Модель данных для импортирования</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult ImportArrivals(DbRestoreImportModel model)
        {
            #region Приём модели и интерпретация данных

            // приём модели
            if (model == null)
                return this.Error("Системная ошибка", "Модель данных не передана с запросом");

            if (!CheckPassword(model.RootPassword, out var error))
                return this.Error("Ошибка авторизации", 
                    "Ошибка проверки пароля. " + error + 
                    " Ваш вход в систему будет прерван. ", true);

            if (model.UsersFile == null)
            {
                return View(model);
            }

            // запись файла в систему
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? "",
                _settings.GetValue(SettingsNames.TempFilesPath));

            if (!Directory.Exists(path)) Directory.CreateDirectory(path);

            string zipFile;
            do
            {
                zipFile = Path.Combine(path, Path.GetRandomFileName().Replace(".", null) + ".tmp");
            } while (System.IO.File.Exists(zipFile));

            using var stream = System.IO.File.Create(zipFile);
            model.UsersFile.CopyTo(stream);
            stream.Close();

            if (!System.IO.File.Exists(zipFile))
                return this.Error("Системная ошибка", 
                    "Входящий файл не записан в систему. " +
                    "Если ошибка повторяется, обратитесь к разработчику");
            try
            {
                path = Path.Combine(path, "unzipped");
                if (Directory.Exists(path)) Directory.Delete(path);
                Directory.CreateDirectory(path);
                ZipFile.ExtractToDirectory(zipFile, path);
            }
            catch (Exception e)
            {
                return this.Error("Ошибка архива",
                    "При разархивировании полученного файла произошла ошибка. " + e.Message +
                    " Импорт данных отменён.");
            }
            finally
            {
                if (System.IO.File.Exists(zipFile)) System.IO.File.Delete(zipFile);
            }

            DbRestoreImportArrivalsModel[] models;

            try
            {
                models = Directory.GetFiles(path)
                    .Select(filePath => new DbRestoreImportArrivalsModel(filePath))
                    .ToArray();
            }
            catch (Exception e)
            {
                return this.Error("Ошибка формата входных файлов",
                    "При чтении полученных файлов произошла ошибка. " + e.Message +
                    " Импорт данных отменён.");
            }
            finally
            {
                if (Directory.Exists(path)) Directory.Delete(path, true);
            }

            if (!models.Any())
                return this.Error("Пустые данные", "На вход передан пустой архив. Импорт данных отменён.");

            #endregion

            #region Проверка связности данных и генерация предварительных запросов

            var resultModel = new DbRestoreImportArrivalsResultModel();

            // проверяет, все ли пользователи добавлены в базу данных
            var users = models.SelectMany(x => x.Rows.Select(y => y.UserName)).Distinct();
            var data = _connector.ReadData("select \"Id\", \"name\" from \"Users\";", out error);

            if (data == null || data.Rows.Count < 0)
                return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);

            var dbUsers = data.Rows.Cast<DataRow>()
                .ToDictionary(x => x.GetValue("name"), y => y.GetInt("Id"));
            if (users.Any(x => !dbUsers.ContainsKey(x)))
                return this.Error("Ошибка связанности данных", 
                    "В базе данных на текущий момент присутствуют не все пользователи, " +
                    "указанные во входных файлах как авторы изменений данных. Загрузите полный список пользователей в базу данных " +
                    "перед добавлением поставок.");

            // проверяет, все ли спецификации добавлены в базу данных
            var specs = models.SelectMany(x => x.Rows.Select(y => y.Lkksn_Specification)).Distinct();
            if (!_connector.TryReadData("select \"Id\" from \"Reactives_Specifications\"", out data, out error))
                return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);

            var dbSpecs = data.Rows.Cast<DataRow>()
                .Select(x => x.GetInt("Id") ?? -1)
                .Where(i => i > 0);
            if (specs.Any(x => !dbSpecs.Contains(x)))
                return this.Error("Ошибка связанности данных", 
                    "В базе данных на текущий момент присутствуют не все спецификации, " +
                    "указанные во входных файлах. Загрузите полный список спецификаций в базу данных " +
                    "перед добавлением поставок.");

            var user = this.GetUserId();
            var now = DateTime.Now.ToSqLiteString();

            // проверяет наличие всех поставщиков в базе данных, готовит запрос на их добавление, если нужно
            var providers = models.SelectMany(x => x.Rows.Select(y => y.ProviderName)).Where(x => !string.IsNullOrWhiteSpace(x)).Distinct().ToArray();
            string providersRequest = null;
            if (!_connector.TryReadData("select \"name\" from \"Reactives_Providers\";", out data, out error))
                return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);
            var dbProviders = data?.Rows.Cast<DataRow>().Select(x => x.GetValue("name"));
            if (providers.Any(x => !dbProviders?.Contains(x) ?? true))
            {
                var pBuilder = new StringBuilder("insert into \"Reactives_Providers\" (\"name\", \"comments\", \"created_at_date\", " +
                                                 "\"created_by\", \"changed_at_date\", \"changed_by\") values ");
                var first = true;
                foreach (var newProvider in providers.Where(x => !dbProviders?.Contains(x) ?? true).Distinct())
                {
                    resultModel.ProvidersCount++;
                    if (first) first = false;
                    else pBuilder.Append(", ");
                    pBuilder.Append($" (\'{newProvider}\', \'Импорт из Лабконтроль в.1\', \'{now}\', {user}, \'{now}\', {user}) ");
                }

                providersRequest = pBuilder + "; ";
            }

            // проверяет наличие всех единиц измерений в базе данных, готовит запрос на их добавление, если нужно
            var units = models.SelectMany(x => x.Rows.Select(y => y.MeasurementUnits)).Distinct().ToArray();
            string unitsRequest = null;
            if (!_connector.TryReadData("select \"name\" from \"Reactives_Mes_Units\";", out data, out error))
                return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);
            var dbUnits = data?.Rows.Cast<DataRow>().Select(x => x.GetValue("name"));
            if (units.Any(x => !dbUnits?.Contains(x) ?? true))
            {
                var uBuilder = new StringBuilder("insert into \"Reactives_Mes_Units\" (\"name\", \"short\", \"position\") values ");
                var first = true;
                foreach (var newUnit in units.Where(x => !dbUnits?.Contains(x) ?? true).Distinct())
                {
                    resultModel.MesUnitsCount++;
                    if (first) first = false;
                    else uBuilder.Append(", ");
                    uBuilder.Append($" (\'{newUnit}\', \'{newUnit}\', 100) ");
                }

                unitsRequest = uBuilder + "; ";
            }

            #endregion

            #region Создание общего запроса к базе данных

            var warningLkksnList = new List<string>();
            
            var arrivalsBuilder = new StringBuilder("insert into \"Reactives_Arrivals\" (\"LKKSN\", \"LKKSN_specification_id\", \"LKKSN_delivery_num\", \"LKKSN_date\", " +
                                             "\"producer_reactive_name\", \"start_state_count\", \"start_state_mes_unit_name\", " +
                                             "\"current_state_count\", \"producer_reactive_count\", \"producer_reactive_mes_unit_name\", " +
                                             "\"created_at_date\", \"created_by\", " +
                                             "\"changed_at_date\", \"changed_by\", " +
                                             "\"provider_id\", \"producer_reactive_SN\", \"current_state_valid_forever\", " +
                                             "\"current_state_valid_until_date\", \"current_state_stored_at\", " +
                                             "\"current_state_comments\", \"current_state_attribute_string\", " +
                                             "\"current_state_is_written_off\") values ");
            var changesBuilder = new StringBuilder("insert into \"Reactives_Changes\" (\"LKKSN\", \"count_difference\", \"count_new_value\", " +
                                             "\"is_written_off\", \"valid_forever\", \"valid_until_date\", " +
                                             "\"stored_at\", \"other_changes\", \"created_at_date\", " +
                                             "\"created_by\", \"record_comment\", " +
                                             "\"attribute_string\") values ");
            var firstArrival = true;
            foreach (var arrival in models)
            {
                resultModel.ArrivalsCount++;
                // основная информация о поставке
                if (!firstArrival) arrivalsBuilder.Append(", ");

                static string SqLiteNullString(string input)
                    => string.IsNullOrWhiteSpace(input) ? "null" : $"\'{input}\'";

                static string Attributes(string comment)
                    => comment?.ToUpper().Contains("ПРЕКУРС") ?? false ? "\'p\'" : "null";

                var startState = arrival.Rows.First();
                var curState = arrival.Rows.Last();
                
                var isMesUnitsChanges = startState.MeasurementUnits != curState.MeasurementUnits; 
                if (isMesUnitsChanges) warningLkksnList.Add(arrival.Lkksn);
                
                var curCount = curState.CurrentCount;
                var startCount = isMesUnitsChanges ? -1 : startState.CurrentCount;
                var startMesUnits = isMesUnitsChanges ? curState.MeasurementUnits : startState.MeasurementUnits;

                arrivalsBuilder.Append($" (\'{arrival.Lkksn}\', {arrival.Lkksn_Specification}, {arrival.Lkksn_DeliveryNum}, \'{arrival.Lkksn_Date.ToSqLiteString()}\', " +
                                $"{SqLiteNullString(curState.ProviderReactiveName)}, {startCount.ToSqLiteString()}, \'{startMesUnits}\', " +
                                $"{curCount.ToSqLiteString()}, {startCount.ToSqLiteString()}, \'{startMesUnits}\', " +
                                $"\'{startState.EditDateTime.ToSqLiteString()}\', {dbUsers[startState.UserName]}, " +
                                $"\'{curState.EditDateTime.ToSqLiteString()}\', {dbUsers[curState.UserName]}, " +
                                $"{(string.IsNullOrWhiteSpace(curState.ProviderName) ? "null" : $"(select min(\"Id\") from \"Reactives_Providers\" where \"name\" = \'{curState.ProviderName}\')")}, " +
                                $"{SqLiteNullString(curState.ProducerReactiveSn)}, " +
                                $"{(curState.IsValidForever ? "1" : "0")} , {(curState.IsValidForever ? "null" : $"\'{curState.ValidUntil?.ToSqLiteString()}\'")}, " +
                                $"{SqLiteNullString(curState.StoredAt)}, {SqLiteNullString(curState.Comment)}, {Attributes(curState.Comment)}, " +
                                $"{(curState.IsWrittenOff ? "1" : "0")}) ");

                // история изменений
                DbRestoreImportArrivalsRowModel prevRow = null;
                foreach (var row in arrival.Rows)
                {
                    resultModel.ChangesCount++;
                    if (!firstArrival || prevRow != null) changesBuilder.Append(", ");

                    if (prevRow == null) // строка новой поставки
                    {
                        var count = row.CurrentCount.ToSqLiteString();
                        changesBuilder.Append($" (\'{arrival.Lkksn}\', {count}, {count}, " +
                                              $"{(row.IsWrittenOff ? "1" : "0")}, {(row.IsValidForever ? "1" : "0")}, {(row.ValidUntil == null ? "null" : $"\'{row.ValidUntil?.ToSqLiteString()}\'")}, " +
                                              $"{SqLiteNullString(row.StoredAt)}, null, \'{row.EditDateTime.ToSqLiteString()}\', " +
                                              $"{dbUsers[row.UserName]}, \'Импорт. Новая поставка.\', " +
                                              $"{Attributes(row.Comment)}) ");
                    }
                    else // строки последующих изменений
                    {
                        // основные изменения
                        var isCountDiff = row.CurrentCount != prevRow.CurrentCount;
                        var countDiff = isCountDiff
                            ? row.CurrentCount - prevRow?.CurrentCount
                            : null;
                        var countNew = isCountDiff
                            ? (decimal?) row.CurrentCount
                            : null;

                        var isValidUntilDiff = row.IsValidForever != prevRow.IsValidForever
                                               || row.ValidUntil != prevRow.ValidUntil;
                        var validUntilDiff = isValidUntilDiff
                            ? row.ValidUntil
                            : null;
                        
                        var isStoredAtDiff = row.StoredAt != prevRow.StoredAt;
                        var storedAtDiff = isStoredAtDiff
                            ? row.StoredAt
                            : null;
                        
                        var isAttributeDiff = row.Comment?.ToUpper().Contains("ПРЕКУРС") !=
                                              prevRow?.Comment?.ToUpper().Contains("ПРЕКУРС");
                        var attributesDiff = row.Comment?.ToUpper().Contains("ПРЕКУРС") ?? false
                            ? "p"
                            : null;

                        var isWrittenOffDiff = row.IsWrittenOff != prevRow.IsWrittenOff;
                        var isWrittenOffNew = row.IsWrittenOff;

                        // прочие изменения
                        var isOtherChanges = false;
                        var otherChanges = new StringBuilder("\'");
                        void AddOtherChanges(string change)
                        {
                            if (string.IsNullOrWhiteSpace(change)) return;
                            if (!isOtherChanges) isOtherChanges = true;
                            else otherChanges.Append("\\n");
                            otherChanges.Append(change);
                        }

                        var isLkksnDiff = row.Lkksn != prevRow.Lkksn;
                        AddOtherChanges(isLkksnDiff
                            ? $"[Серия ЛКК] изменена с [{prevRow.Lkksn}] на [{row.Lkksn}]"
                            : null);

                        var isReactiveNameDiff = row.ProviderReactiveName != prevRow?.ProviderReactiveName;
                        AddOtherChanges(isReactiveNameDiff
                            ? $"[Имя реактива] изменено с [{prevRow.ProviderReactiveName}] на [{row.ProviderReactiveName}]"
                            : null);

                        var isUnitsDiff = row.MeasurementUnits != prevRow.MeasurementUnits;
                        AddOtherChanges(isUnitsDiff
                            ? $"[Единица измерения] изменена с [{prevRow.MeasurementUnits}] на [{row.MeasurementUnits}]"
                            : null);

                        var isProviderDiff = row.ProviderName != prevRow.ProviderName;
                        AddOtherChanges(isProviderDiff
                            ? $"[Поставщик] изменён с [{prevRow?.ProviderName}] на [{row.ProviderName}]"
                            : null);

                        var isReactiveSnDiff = row.ProducerReactiveSn != prevRow.ProducerReactiveSn;
                        AddOtherChanges(isReactiveSnDiff
                            ? $"[Серийный номер] изменён с [{prevRow.ProducerReactiveSn}] на [{row.ProducerReactiveSn}]"
                            : null);

                        var isCommentDiff = row.Comment != prevRow.Comment;
                        AddOtherChanges(isCommentDiff
                            ? $"[Комментарий] изменён с [{(string.IsNullOrWhiteSpace(prevRow.Comment) ? "-" : prevRow.Comment)}] " +
                              $"на [{(string.IsNullOrWhiteSpace(row.Comment) ? "-" : row.Comment)}]"
                            : null);
                        otherChanges.Append("\'");

                        // вписывает набор изменений в запрос
                        changesBuilder.Append($" (\'{arrival.Lkksn}\', {(isCountDiff ? countDiff?.ToSqLiteString() : "null")}, {(isCountDiff ? countNew?.ToSqLiteString() : "null")}, " +
                                        $"{(isWrittenOffDiff ? (isWrittenOffNew  ? "1" : "0") : "null")}, {(isValidUntilDiff ? (row.IsValidForever ? "1" : "0") : "null")}, " +
                                        $"{(isValidUntilDiff ? (validUntilDiff == null ? "null" : $"\'{validUntilDiff?.ToSqLiteString()}\'") : "null")}, " +
                                        $"{(isStoredAtDiff ? $"\'{storedAtDiff}\'": "null")}, {(isOtherChanges ? otherChanges.ToString() : "null")}, " +
                                        $"\'{row.EditDateTime.ToSqLiteString()}\', {dbUsers[row.UserName]}, " +
                                        $"\'Импорт. Изменение\', {(isAttributeDiff ? $"\'{attributesDiff}\'" : "null")}) ");
                    }
                    prevRow = row;
                }

                if (firstArrival) firstArrival = false;

            }

            arrivalsBuilder.Append("; ");
            changesBuilder.Append("; ");
            var generalRequest = new StringBuilder("begin; " + providersRequest + unitsRequest + arrivalsBuilder + changesBuilder + "commit; ");

            #endregion

            var result = _connector.WriteData(generalRequest.ToString(), out error);
            if (result < 1)
                return this.Error("Ошибка базы данных", "При импорте данных в базе произошла ошибка. " + error);

            foreach (var f in Directory.GetFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? "", _settings.GetValue(SettingsNames.TempFilesPath)))
                .Where(x => DateTime.Now.Subtract(System.IO.File.GetCreationTime(path)).Days > 1))
                System.IO.File.Delete(f);

            resultModel.WarningArrivals = warningLkksnList;
            return RedirectToAction("ImportArrivalsResult", "DbRestore", resultModel);
        }

        /// <summary> Перевход к просмотру результатов </summary>
        /// <param name="model">Перечисление проблемных поставок</param>
        public IActionResult ImportArrivalsResult(DbRestoreImportArrivalsResultModel model) => View(model);

        /// <summary> Возвращает путь к папке с резервными копиями </summary>
        /// <remarks> Обеспечивает существование папки </remarks>
        private string BackUpDirectory
        {
            get
            {
                var dir = Path.Combine
                (
                    AppDomain.CurrentDomain.BaseDirectory ?? "",
                    _settings.GetValue(SettingsNames.DbBackUpBasePath)
                );


                if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                return dir;
            }
        }

        /// <summary> Возвращает путь к текущей базе данных </summary>
        /// <remarks>Существование файла не гарантируется методом (но иначе сюда не попасть)</remarks>
        private string CurrentBaseFile => 
            Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? "", _settings.GetValue(SettingsNames.DbMainBaseFile));


        /// <summary> Проверяет пароль пользователя в базе данных</summary>
        /// <param name="error">Сообщение ошибки, если та произошла. <see langword="null"/> = не произошла</param>
        /// <param name="password">Пароль корневого пльзователя</param>
        /// <returns>Флаг того, что пароль введён верно</returns>
        private bool CheckPassword(string password, out string error)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                error = "Пароль не введён";
                return false;
            }

            var count = int.TryParse
            (
                _connector.GetValue(
                    $"select count(\"Id\") from \"Users\" where \"Id\" = 1 and \"password\" = '{password}'", out error),
                out var i
            )
                ? i
                : -1;

            return count == 1;
        }

    }
}
