﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Home;
using WebApplication.Models.Producers;
using WebApplication.Models.Shared;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;

namespace WebApplication.Controllers
{
    /// <summary> Управление производителями </summary>
    [Authorize]
    public class ProducersController : Controller
    {
        /// <summary> Сервис базы данных </summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструктор запрашивает нужные сервисы </summary>
        /// <param name="connector">Сервис подключения к базе данных</param>
        public ProducersController(IDbConnector connector)
            => _connector = connector;

        /// <summary> Начало всех запросов select в контроллере </summary>
        private const string SelectStart =
            "select tbl.\"Id\", tbl.\"name\", tbl.\"comments\", " +
            "tbl.\"created_at_date\", u1.\"name\" as \"created_by\", tbl.\"changed_at_date\", u2.\"name\" as \"changed_by\" " +
            "from \"Reactives_Producers\" tbl " +
            "left join \"Users\" u1 on tbl.\"created_by\" = u1.\"Id\" " +
            "left join \"Users\" u2 on tbl.\"changed_by\" = u2.\"Id\" ";

        /// <summary> Просмотр и фильтрация производителей</summary>
        public IActionResult Index(ProducerViewFilterModel model)
        {
            var builder = new StringBuilder("select \"Id\", \"name\", \"comments\" from \"Reactives_Producers\"");
            
            var isFirstCondition = true;

            string AndWhere()
            {
                if (!isFirstCondition) return " and ";
                isFirstCondition = false;
                return " where ";
            }

            if (!string.IsNullOrWhiteSpace(model.FilterName))
                builder.Append(AndWhere() + $"\"name\" like \'%{model.FilterName}%\' ");
            if (!string.IsNullOrWhiteSpace(model.FilterComment))
                builder.Append(AndWhere() + $"\"comments\" like \'%{model.FilterComment}%\' ");

            builder.Append(" order by \"Id\" asc;");

            // обращается к базе
            if (!_connector.TryReadData(builder.ToString(), out var data, out var error))
            {
                RedirectToAction("Error", "Home",
                    new FailModel {Name = "Провал базы данных", Description = error});
            }

            model.Producers = data?.Rows.Cast<DataRow>()
                .Select(x => new ProducerIndexModel
                {
                    Id = int.TryParse(x.GetValue("Id"), out var i) ? i : 0,
                    Name = x.GetValue("name"),
                    Comment = x.GetValue("comments")
                });

            // выводит представление
            return View(model);
        }

        /// <summary> Подробная информация по производителю </summary>
        /// <param name="id">Номер производителя</param>
        [HttpGet]
        public IActionResult Info(int? id)
        {
            if (id == null)
                return this.Error("Провал чтения данных",
                    "Невозможно загрузить данные о производителе - в запросе отсутствует его номер. " +
                    "Обратитесь к разработчику.");

            // получает данные из базы
            var data = _connector.ReadData(SelectStart + $" where tbl.\"Id\" = {id};", out var error);

            if (data == null || data.Rows.Count != 1)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Неприемлемый ответ базы данных",
                        Description = "Невозможно загрузить данные о производителе - от базы получено неприемлемое количество значений. " +
                                      error + " Обратитесь к разработчику."
                    });

            return View(new ProducerInfoModel
            {
                Id = (int)id,
                Name = data.Rows[0].GetValue("name"),
                Comment = data.Rows[0].GetValue("comments"),
                RecordIdentity = new DbRecordIdentity(data.Rows[0])
            });
        }

        /// <summary> Вход в инструмент добавления производителя </summary>
        [HttpGet]
        public IActionResult Add() => View();

        /// <summary> Подтверждение добавления производителя</summary>
        /// <param name="model">Модель производителя</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Add(ProducerInfoModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var isComment = !string.IsNullOrWhiteSpace(model.Comment);

            var now = DateTime.Now.ToSqLiteString();
            var user = this.GetUserId();
            var done = _connector.WriteData
            (
                "insert into \"Reactives_Producers\" (\"name\", " + (isComment ? "\"comments\", " : null) +
                "\"created_at_date\", \"created_by\", \"changed_at_date\", \"changed_by\") " +
                $"values (\'{model.Name.Trim()}\', " + (isComment ? $"\'{model.Comment.Trim()}\', " : null) +
                $"\'{now}\', {user}, \'{now}\', {user});",
                out var error
            );

            if (done > 0)
                return RedirectToAction("Index", "Producers",
                    new ProducerViewFilterModel { FilterName = model.Name});

            if (error.Contains(Constants.DataBaseUniqueConstraintError))
                error = $"В базе данных уже существует производитель с именем {model.Name}. " +
                        "Повторные номера не допустимы.";

            return RedirectToAction("Fail", "Home",
                new FailModel
                {
                    Name = "Ошибка базы данных",
                    Description = "При загрузке нового производителя в базу данных произошла ошибка. " + error +
                                  " Данные не были загружены."
                });
        }

        /// <summary> Вход в инструмент изменения производителя</summary>
        /// <param name="id">Номер производителя, подлежащего изменению</param>
        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (id == null)
                return this.Error("Провал изменения производителя - нет номера",
                    "При попытке загрузить инструмент для изменения производителя произошла " +
                    "ошибка из-за отсутствия номера производителя, подлежащего изменению. " +
                    "Обратитесь к разработчику.");

            // получает данные из базы
            var data = _connector.ReadData(SelectStart + $" where tbl.\"Id\" = {id};", out var error);

            if (data == null || data.Rows.Count != 1)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Провал изменения производителя - неприемлемый ответ базы данных",
                        Description = "При попытке загрузить инструмент для изменения производителя произошла " +
                                      "ошибка из-за неприемлемого ответа базы данных. " + error +
                                      "Обратитесь к разработчику."
                    });

            var name = data.Rows[0].GetValue("name");
            var comment = data.Rows[0].GetValue("comments");

            return View(new ProducerEditModel
            {
                Id = (int)id,
                OldName = name,
                NewName = name,
                OldComment = comment,
                NewComment = comment,
                RecordIdentity = new DbRecordIdentity(data.Rows[0])
            });

        }

        /// <summary> Подтверждение изменения производителя </summary>
        /// <param name="model">Модель для изменения производителя</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Edit(ProducerEditModel model)
        {
            // валидация входных данных
            if (string.IsNullOrWhiteSpace(model.OldName))
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Ошибка формы изменения данных",
                        Description = "Во время обновления данных обнаружена ошибка: служебные поля HTTP запроса " +
                                      "оказались незаполненными. Обратитесь к разработчику."
                    });

            var isNewComment = !string.IsNullOrWhiteSpace(model.NewComment + model.OldComment) &&
                               model.NewComment?.Trim() != model.OldComment;

            if (model.NewName?.Trim() == model.OldName && !isNewComment)
                ModelState.AddModelError("", "Новые введённые данные полностью совпадают с существующими. ");

            if (!ModelState.IsValid) return View(model);

            // готовит запрос и изменяет базу
            var builder = new StringBuilder("update \"Reactives_Producers\" set ");

            var isFirst = true;
            if (model.OldName != model.NewName?.Trim())
            {
                isFirst = false;
                builder.Append($"\"name\" = '{model.NewName?.Trim()}'");
            }

            if (isNewComment)
                builder.Append((isFirst ? null : ", ") + $"\"comments\" = \'{model.NewComment?.Trim()}\' ");

            builder.Append($", \"changed_at_date\" = \'{DateTime.Now.ToSqLiteString()}\'" +
                           $", \"changed_by\" = {this.GetUserId()} ");

            builder.Append($"where \"name\" = \'{model.OldName}\';");

            var done = _connector.WriteData(builder.ToString(), out var error);

            if (done < 1)
            {
                if (error.Contains(Constants.DataBaseUniqueConstraintError))
                    error = $"В базе данных уже существует производитель с именем \"{model.NewName}\". " +
                            "Названия должны быть уникальными.";
                return this.Error("Ошибка базы данных",
                    "При обновлении данных по производителю в базе данных произошла ошибка. " + error +
                    " Данные не обновлены. ");
            }

            return RedirectToAction("Index", "Producers",
                new ProducerViewFilterModel { FilterName = model.NewName});
        }

        /// <summary> Вход в инструмент удаления производителя </summary>
        /// <param name="id">Номер производителя, подлежащего удалению</param>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Delete(int? id)
        {
            // валидация данных
            if (id == null)
                return this.Error("Провал удаления производителя - нет номера",
                    "При попытке загрузить инструмент для удаления производителя произошла " +
                    "ошибка из-за отсутствия номера производителя, подлежащего удалению. " +
                    "Обратитесь к разработчику.");

            // Получает данные из базы
            var data = _connector.ReadData(SelectStart + $" where tbl.\"Id\" = '{id}';", out var error);
            
            if (data == null || data.Rows.Count != 1)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Неприемлемый ответ базы данных",
                        Description = "При попытке загрузить инструмент для удаления производителя произошла " +
                                      "ошибка из-за неприемлемого ответа базы данных. " + error +
                                      "Обратитесь к разработчику."
                    });

            return View(new ProducerInfoModel
            {
                Name = data.Rows[0].GetValue("name"),
                Comment = data.Rows[0].GetValue("comments"),
                RecordIdentity = new DbRecordIdentity(data.Rows[0])
            });

        }

        /// <summary> Подтверждение удаления производителя </summary>
        /// <param name="model">Модель для удаления производителя</param>
        [HttpPost]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Delete(ProducerInfoModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.Name))
                return this.Error("Провал удаления производителя - недостаточно данных",
                    "При удалении производителя произошла ошибка из-за недостаточно полной информации," +
                    " поступившей в запросе. Обратитесь к разработчику.");

            var done = _connector.WriteData
            (
                $"delete from \"Reactives_Producers\" where \"name\" = \'{model.Name}\';",
                out var error
            );

            if (done < 1)
                return this.Error("Ошибка базы данных",
                    "При удалении производителя в базе данных произошла ошибка. " + error +
                    " Данные не обновлены. ");

            return RedirectToAction("Index", "Producers");

        }
    }
}
