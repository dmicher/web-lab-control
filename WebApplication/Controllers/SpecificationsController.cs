﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Home;
using WebApplication.Models.Shared;
using WebApplication.Models.Specifications;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;

namespace WebApplication.Controllers
{
    /// <summary> Обработка спецификаций </summary>
    [Authorize]
    public class SpecificationsController : Controller
    {
        /// <summary> Подключатель к базе данных </summary>
        private readonly IDbConnector _connector;

        /// <summary> Начало всех select запросов для контроллера </summary>
        private const string SelectStart =
            "select s.\"Id\", s.\"name\", s.\"created_at_date\", u1.\"name\" as \"created_by\", s.\"changed_at_date\", u2.\"name\" as \"changed_by\" " +
            "from \"Reactives_Specifications\" s " +
            "left join \"Users\" u1 on s.\"created_by\" = u1.\"Id\" " +
            "left join \"Users\" u2 on s.\"changed_by\" = u2.\"Id\" ";

        /// <summary> Конструктор собирает нужные сервисы </summary>
        /// <param name="connector"> Подключатель к базе данных </param>
        public SpecificationsController(IDbConnector connector) => _connector = connector;

        /// <summary> Просмотр и фильтрация спецификаций </summary>
        public IActionResult Index(SpecificationViewFilterModel model)
        {
            // собирает SQL запрос
            var builder = new StringBuilder("select \"Id\", \"name\" from \"Reactives_Specifications\"");

            var isFirstCondition = true;

            string AndWhere()
            {
                if (!isFirstCondition) return " and ";
                isFirstCondition = false;
                return " where ";
            }

            if (!string.IsNullOrWhiteSpace(model.NumberFilter))
                builder.Append(AndWhere() + $"\"Id\" like \'{model.NumberFilter.Replace('*', '%').Replace('?', '_')}\' ");
            if (!string.IsNullOrWhiteSpace(model.NameFilter))
                builder.Append(AndWhere() +
                               $"\"name\" like \'%{model.NameFilter}%\' ");

            builder.Append(" order by \"Id\" asc;");

            // обращается к базе
            if (!_connector.TryReadData(builder.ToString(), out var data, out var error))
            {
                RedirectToAction("Error", "Home",
                    new FailModel {Name = "Провал базы данных", Description = error});
            }

            model.Specifications = data?.Rows.Cast<DataRow>()
                .Select(x => new SpecificationIndexModel
                {
                    Number = int.TryParse(x.GetValue("Id"), out var i) ? i : 0,
                    Name = x.GetValue("name")
                });

            // выводит представление
            return View(model);
        }

        /// <summary> Подробности о спецификации </summary>
        /// <param name="id">Номер спецификации</param>
        [HttpGet]
        public IActionResult Info(int? id)
        {
            if (id == null)
                return this.Error("Провал чтения данных",
                    "Невозможно загрузить данные о поставщике - в запросе отсутствует его номер. " +
                    "Обратитесь к разработчику.");

            // получает данные из базы
            var data = _connector.ReadData(SelectStart + $" where s.\"Id\" = {id};", out var error);

            if (data == null || data.Rows.Count != 1)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Неприемлемый ответ базы данных",
                        Description = "Невозможно загрузить данные о поставщике - от базы получено неприемлемое количество значений. " +
                                      error + " Обратитесь к разработчику."
                    });

            return View(new SpecificationInfoModel
            {
                Number = id.Value.ToString("D3"),
                Name = data.Rows[0].GetValue("name"),
                RecordIdentity = new DbRecordIdentity(data.Rows[0])
            });
        }

        /// <summary> Первый заход на добавление спецификации </summary>
        [HttpGet]
        public IActionResult Add() => View();

        /// <summary> Добавление спецификации </summary>
        /// <param name="model">Модель спецификации</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Add(SpecificationIndexModel model)
        {
            if (model.Number <= 0)
                ModelState.AddModelError("",
                    "Неверный формат номера спецификации: ожидается целое положительное число от 1 до 999.");

            if (!ModelState.IsValid) return View(model);

            var now = DateTime.Now.ToSqLiteString();
            var user = this.GetUserId();
            var done = _connector.WriteData
            (
                "insert into \"Reactives_Specifications\" " +
                "(\"Id\", \"name\", \"upper_name\", \"created_at_date\", \"created_by\", \"changed_at_date\", \"changed_by\") " +
                $"values ({model.Number}, \'{model.Name.Trim()}\', \'{model.Name.Trim().ToUpper()}\', \'{now}\', {user}, \'{now}\', {user});",
                out var error
            );

            if (done < 1)
            {
                if (error.Contains(Constants.DataBaseUniqueConstraintError))
                    error = $"В базе данных уже существует номер спецификации {model.Number}. " +
                            "Повторные номера спецификации не допустимы.";

                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Ошибка базы данных",
                        Description = "При загрузке новой спецификации в базу данных произошла ошибка. " + error +
                                      " Данные не были загружены."
                    });
            }

            return RedirectToAction("Index", "Specifications",
                new SpecificationViewFilterModel
                {
                    NumberFilter = "*" + model.Number,
                    NameFilter = model.Name
                });

        }

        /// <summary> Первый заход на правку данных </summary>
        [HttpGet]
        public IActionResult Edit(int? id)
        {
            // валидация данных
            if (id == null)
                return this.Error("Провал изменения спецификации - нет номера",
                    "При попытке загрузить инструмент для изменения спецификации произошла " +
                    "ошибка из-за отсутствия номера спецификации, подлежащей изменению. " +
                    "Обратитесь к разработчику.");

            if (id <= 0 || id >= 1000)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Провал изменения спецификации - неприемлемый номер",
                        Description = "При попытке загрузить инструмент для изменения спецификации произошла " +
                                      "ошибка из-за того, что указанный номер спецификации выходит за рамки " +
                                      "допустимых значений (от 1 до 999). Обратитесь к разработчику."
                    });

            // получает данные из базы
            var data = _connector.ReadData(SelectStart + $" where s.\"Id\" = {id};", out var error);

            if (data == null || data.Rows.Count != 1)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Провал изменения спецификации - неприемлемый ответ базы данных",
                        Description = "При попытке загрузить инструмент для изменения спецификации произошла " +
                                      "ошибка из-за неприемлемого ответа базы данных. " + error +
                                      "Обратитесь к разработчику."
                    });

            var name = data.Rows[0].GetValue("name");
            var number = int.TryParse(data.Rows[0].GetValue("Id"), out var i)
                ? i.ToString("D3")
                : null;

            return View(new SpecificationEditModel
            {
                OldName = name,
                NewName = name,
                OldNumber = number,
                NewNumber = number,
                RecordIdentity = new DbRecordIdentity(data.Rows[0])
            });
        }

        /// <summary> Внесение изменений в данные о выбранной спецификации </summary>
        /// <param name="model"> Модель данных </param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Edit(SpecificationEditModel model)
        {
            // валидация входных данных
            if (string.IsNullOrWhiteSpace(model.OldName) || string.IsNullOrWhiteSpace(model.OldNumber))
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Ошибка формы изменения данных",
                        Description = "Во время обновления данных обнаружена ошибка: служебные поля HTTP запроса " +
                                      "оказались незаполненными. Обратитесь к разработчику."
                    });

            if (string.IsNullOrWhiteSpace(model.NewName) || string.IsNullOrWhiteSpace(model.NewNumber))
                ModelState.AddModelError("", "Не все данные введены. ");

            if (!int.TryParse(model.NewNumber, out var newNumberInt) || newNumberInt <= 0 || newNumberInt >= 1000)
                ModelState.AddModelError("",
                    "Значение введённое для номера не допустимо: введите целое положительное " +
                    "число в диапазоне от 1 до 999.");

            if (model.NewName?.Trim() == model.OldName && model.NewNumber?.Trim() == model.OldNumber)
                ModelState.AddModelError("", "Новые введённые данные полностью совпадают с существующими. ");

            if (!ModelState.IsValid) return View(model);

            // готовит запрос и изменяет базу
            var builder = new StringBuilder("update \"Reactives_Specifications\" set ");

            var isFirst = true;
            if (model.OldName != model.NewName?.Trim())
            {
                isFirst = false;
                builder.Append($"\"name\" = '{model.NewName?.Trim()}', \"upper_name\" = '{model.NewName?.Trim().ToUpper()}' ");
            }

            if (model.OldNumber != model.NewNumber?.Trim())
                builder.Append((isFirst ? null : ", ") + $"\"Id\" = \'{model.NewNumber?.Trim()}\' ");

            builder.Append($", \"changed_at_date\" = \'{DateTime.Now.ToSqLiteString()}\'" +
                           $", \"changed_by\" = {this.GetUserId()} ");

            builder.Append($"where \"Id\" = \'{model.OldNumber}\' and \"name\" = \'{model.OldName}\';");

            var done = _connector.WriteData(builder.ToString(), out var error);

            if (done < 1)
            {
                if (error.Contains(Constants.DataBaseUniqueConstraintError))
                    error = $"В базе данных уже существует номер спецификации {model.NewNumber}. " +
                            "Повторные номера спецификации не допустимы.";
                return this.Error("Ошибка базы данных",
                    "При обновлении данных по спецификации в базе данных произошла ошибка. " + error +
                    " Данные не обновлены. ");
            }

            return RedirectToAction("Index", "Specifications",
                new SpecificationViewFilterModel
                {
                    NumberFilter = "*" + newNumberInt,
                    NameFilter = $"{model.NewName}"
                });
        }

        /// <summary> Первый заход на удаление спецификации</summary>
        /// <param name="id"></param>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Delete(int? id)
        {
            // валидация данных
            if (id == null)
                return this.Error("Провал удаления спецификации - нет номера",
                    "При попытке загрузить инструмент для удаления спецификации произошла " +
                    "ошибка из-за отсутствия номера спецификации, подлежащей удалению. " +
                    "Обратитесь к разработчику.");
            
            if (id <= 0 || id >= 1000)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Провал удаления спецификации - неприемлемый номер",
                        Description = "При попытке загрузить инструмент для удаления спецификации произошла " +
                                      "ошибка из-за того, что указанный номер спецификации выходит за рамки " +
                                      "допустимых значений (от 1 до 999). Обратитесь к разработчику."
                    });
            
            // Получает данные из базы
            var data = _connector.ReadData(SelectStart + $" where s.\"Id\" = '{id}';", out var error);
            
            if (data == null || data.Rows.Count != 1)
                return RedirectToAction("Fail", "Home",
                    new FailModel
                    {
                        Name = "Провал удаления спецификации - неприемлемый ответ базы данных",
                        Description = "При попытке загрузить инструмент для удаления спецификации произошла " +
                                      "ошибка из-за неприемлемого ответа базы данных. " + error +
                                      " Обратитесь к разработчику."
                    });

            return View(new SpecificationInfoModel
            {
                Number = int.TryParse(data.Rows[0].GetValue("Id"), out var i)
                    ? i.ToString("D3")
                    : null,
                Name = data.Rows[0].GetValue("name"),
                RecordIdentity = new DbRecordIdentity(data.Rows[0])
            });
        }

        /// <summary> Внесение изменений в данные о выбранной спецификации </summary>
        /// <param name="model"> Модель данных </param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Delete(SpecificationInfoModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.Number) || string.IsNullOrWhiteSpace(model.Name))
                return this.Error("Провал удаления спецификации - недостаточно данных",
                    "При удалении спецификации произошла ошибка из-за недостаточно полной информации," +
                    " поступившей в запросе. Обратитесь к разработчику.");

            var done = _connector.WriteData
            (
                $"delete from \"Reactives_Specifications\" where \"Id\" = \'{model.Number}\' and \"name\" = \'{model.Name}\';",
                out var error
            );

            if (done < 1)
                return this.Error("Ошибка базы данных",
                    "При удалении спецификации в базе данных произошла ошибка. " + error +
                    " Данные не обновлены. ");

            return RedirectToAction("Index", "Specifications");
        }
    }
}