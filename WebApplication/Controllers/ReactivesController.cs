﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;
using WebApplication.Models.Reactives;

namespace WebApplication.Controllers
{
    /// <summary> Управление поставками реактивов и расходных материалов </summary>
    [Authorize]
    public class ReactivesController : Controller
    {
        /// <summary> Сервис связи с базой данных </summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструктор собирает нужные сервисы</summary>
        /// <param name="connector">Сервис базы данных</param>
        public ReactivesController(IDbConnector connector)
            => _connector = connector;

        /// <summary> Просмотр и фильтрация поставок </summary>
        /// <param name="model">Модель представления: фильтры и результаты</param>
        public IActionResult Index(ReactivesIndexFilterModel model)
        {
            #region Поиск различных перечислений для списков

            // (будет использован позже) снимает с модели поднятые флаги по атрибутам (сотрёт их следующим шагом)
            var filterAttributes = model.Filter_Attributes
                ?.Where(x => x.Value)
                .Select(x => x.Key)
                .ToArray();

            try
            {
                model.Specifications = GetSpecifications();
                model.Producers = GetProducers();
                model.Providers = GetProviders();
                var attributes = GetAttributes();
                model.Filter_Attributes = attributes?.ToDictionary(x => x.Key, y => false);
                model.AttributesNames = attributes;
            }
            catch (Exception e)
            {
                return this.Error("Ошибка базы данных", e.Message);
            }

            #endregion

            #region Поиск реактивов по имеющимся данным

            var request = new StringBuilder
            (
                "select " +
                    "a.\"LKKSN\", " +
                    "s.\"name\" as \"specification\", " +
                    "a.\"current_state_count\" as \"count\", " +
                    "a.\"current_state_is_written_off\" as \"is_written_off\", " +
                    "u.\"short\" as \"unit\", " +
                    "a.\"current_state_valid_forever\" as \"valid_forever\", " +
                    "a.\"current_state_valid_until_date\" as \"valid_until\" " +
                "from \"Reactives_Arrivals\" a " +
                    "inner join \"Reactives_Specifications\" s on a.\"LKKSN_specification_id\" = s.\"Id\" " +
                    "inner join \"Reactives_Mes_Units\" as u on u.\"name\" = a.\"start_state_mes_unit_name\" "
            );

            var isFirst = true;
            string AndWhere()
            {
                if (!isFirst) return " and ";
                isFirst = false;
                return " where ";
            }

            // по выбранному периоду по году и месяцу (независимо)
            if (model.Filter_YearInserted != null)
            {
                var year = (int) model.Filter_YearInserted;
                if (year <= 0) year = DateTime.Today.AddYears(year).Year;
                else if (year <= 99) year += 2000;

                if (year < 1900 || year > DateTime.Today.Year)
                {
                    ModelState.AddModelError("", "В поле фильтра по году введено неприемлемое значение");
                    return View(model);
                }

                if (model.Filter_MonthSelected < 1)
                    request.Append(AndWhere() + $" \"LKKSN_date\" like \'{year}-%\' ");
                else
                    request.Append(AndWhere() + $" \"LKKSN_date\" like \'{year}-{model.Filter_MonthSelected:D2}-%\' ");
            }
            else if (model.Filter_MonthSelected > 0)
            {
                request.Append(AndWhere() + $" \"LKKSN_date\" like \'____-{model.Filter_MonthSelected:D2}-%\' ");
            }

            // по выбранной спецификации и части её имени
            if (model.Filter_Lkksn_SpecificationSelected > 0)
                request.Append(AndWhere() + $" \"LKKSN_specification_id\" = {model.Filter_Lkksn_SpecificationSelected} ");
            if (!string.IsNullOrWhiteSpace(model.Filter_SpecificationNameLike))
                request.Append(AndWhere() + $" s.\"upper_name\" like  \'%{model.Filter_SpecificationNameLike.ToUpper()}%\' ");

            // по введённым элементам серии ЛКК
            if (!string.IsNullOrWhiteSpace(model.Filter_Lkksn_SpecificationLike))
                request.Append(AndWhere() + $" \"LKKSN\" like \'%{model.Filter_Lkksn_SpecificationLike}/%/%\' ");
            if (!string.IsNullOrWhiteSpace(model.Filter_Lkksn_NumberLike))
                request.Append(AndWhere() + $" \"LKKSN\" like \'%/%{model.Filter_Lkksn_NumberLike}/%\' ");
            if (model.Filter_Lkksn_Date != null)
                request.Append(AndWhere() + $" \"LKKSN\" like \'%/%/{model.Filter_Lkksn_Date?.ToString("ddMMyy")}\' ");

            // по типу статуса списания
            switch (model.Filter_WrittenOff)
            {
                case "no":
                    request.Append(AndWhere() + " \"current_state_is_written_off\" = \'0\' ");
                    break;
                case "yes":
                    request.Append(AndWhere() + " \"current_state_is_written_off\" = \'1\' ");
                    break;
            }

            // по производителю и поставщику
            if (model.Filter_ProducerSelected > 0)
                request.Append(AndWhere() + $" \"producer_id\" = {model.Filter_ProducerSelected} ");
            if (model.Filter_ProviderSelected > 0)
                request.Append(AndWhere() + $" \"provider_id\" = {model.Filter_ProviderSelected} ");

            // по атрибутам
            if (filterAttributes != null && filterAttributes.Any())
                foreach (var c in filterAttributes)
                    request.Append(AndWhere() + $" \"current_state_attribute_string\" like \'%{c}%\' ");

            // по содержанию комментария
            if (!string.IsNullOrWhiteSpace(model.Filter_CommentsLike))
                request.Append(AndWhere() + $" \"current_state_comments\" like \'%{model.Filter_CommentsLike}%\' ");

            // сортировка и ограничение выборки
            switch (model?.SortBySelected)
            {
                case "date_old":
                    request.Append(" order by a.\"LKKSN_date\" asc ");
                    break;
                case "lkksn":
                    request.Append(" order by a.\"LKKSN\" asc ");
                    break;
                case "created_at_old": 
                    request.Append(" order by a.\"created_at_date\" asc ");
                    break;
                case "created_at_new": 
                    request.Append(" order by a.\"created_at_date\" desc ");
                    break;
                case "changed_at_old":
                    request.Append(" order by a.\"changed_at_date\" asc ");
                    break;
                case "changed_at_new":
                    request.Append(" order by a.\"changed_at_date\" desc ");
                    break;
                default:
                    request.Append(" order by a.\"LKKSN_date\" desc ");
                    break;
            }

            if (model.ResultRowsCountLimit <= 0)
                model.ResultRowsCountLimit = 25;
            request.Append(" limit " + model.ResultRowsCountLimit);

            if (!ModelState.IsValid) return View(model);

            if (!_connector.TryReadData(request.ToString(), out var data, out var error))
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить данные о поставках реактивов произошла ошибка. " + error +
                    "Обратитесь к разработчику");

            model.Reactives = data?.Rows.Cast<DataRow>()
                .Select(x => new ReactivesIndexModel
                {
                    Lkksn = x.GetValue("LKKSN"),
                    Specification = x.GetValue("specification"),
                    Count = x.GetDecimal("count"),
                    MesUnitShort = x.GetValue("unit"),
                    IsValidForever = (x.GetInt("valid_forever") ?? 0) > 0,
                    ValidUntil = x.GetDate("valid_until"),
                    IsWrittenOff = x.GetValue("is_written_off") == "1"
                });

            #endregion

            return View(model);
        }

        /// <summary> Вход в инструмент добавления новой поставки </summary>
        /// <param name="id">Номер спецификации, для которой регистрируется поставка </param>
        [HttpGet]
        public IActionResult Add(int? id)
        {
            // ЛКК номер и название спецификации
            if (id == null || id < 1 || id > 999)
                return this.Error("Системная ошибка",
                    "При получении запроса на новую поставку номер спецификации был не указан, " +
                    "либо указан не верно. Обратитесь к разработчику.");

            var model = new ReactivesAddModel
            {
                Lkksn_SpecificationId = (int)id,
                SpecificationName = _connector.GetValue($"select \"name\" from \"Reactives_Specifications\" where \"Id\" = {id};", out var error)
            };

            if (string.IsNullOrWhiteSpace(model.SpecificationName))
                return this.Error("Ошибка базы данных",
                    "При попытке загрузки данных о выбранной спецификации для новой поставки произошла ошибка. " +
                    error);

            // ЛКК номер новой поставки (номер сбрасывается каждый год)
            if (!_connector.TryReadData(
                "select max(\"LKKSN_delivery_num\") as \"num\" from \"Reactives_Arrivals\" " +
                $"where \"LKKSN_date\" like '{DateTime.Today.Year}-%' and \"LKKSN_specification_id\" = {model.Lkksn_SpecificationId} " +
                "group by LKKSN_specification_id;",
                out var data, out error))
                return this.Error("Системная ошибка",
                    "При получении данных о последней поставке по выбранной спецификации " +
                    "за текущий год произошла ошибка. " + error);

            model.Lkksn_DeliveryNum = data == null || data.Rows.Count < 1
                ? 1 // если данных нет, новый номер - единица
                : int.TryParse(data.Rows[0].GetValue("num"), out var i)
                    ? ++i // если данные есть - от максимального номера "плюс единица"
                    : -1;
            if (model.Lkksn_DeliveryNum < 0)
                return this.Error("Системная ошибка",
                    "При получении данных о последней поставке по выбранной спецификации " +
                    "за текущий год произошла ошибка. Полученный из базы данных номер не соответствует" +
                    " формату данных. Обратитесь к разработчику.");

            // ЛКК дата поставки
            model.Lkksn_Date = DateTime.Today;
            
            // общие подгружаемые списки
            try
            {
                model.Providers = GetProviders();
                model.Producers = GetProducers();
                model.MesUnits = GetMesUnits();
                model.Stores = GetStores();
                var attributes = GetAttributes();
                model.Attributes = attributes?.ToDictionary(x => x.Key, y => false);
                model.AttributesNames = attributes;
            }
            catch (Exception e)
            {
                return this.Error("Ошибка базы данных",
                    "При загрузке данных из базы данных произошла ошибка. " + e.Message);
            }
            
            return View(model);
        }

        /// <summary> Подтверждение добавления новой поставки </summary>
        /// <param name="model">Модель данных для добавления поставки</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Add(ReactivesAddModel model)
        {
            #region Валидация и подготовка обязательных данных, повторный ввод с представления

            if (model.Lkksn_SpecificationId <= 0 || model.Lkksn_SpecificationId > 999)
                return this.Error("Системная ошибка",
                    "При добавлении новой поставки произошла ошибка. " +
                    "Номер спецификации не передан с запросом. Обратитесь к разработчику.");

            if (model.Lkksn_DeliveryNum < 1)
                ModelState.AddModelError("", "Номер поставки не может быть меньше 1");

            var curCount = model.CurrentState_Count.ToDecimal() ?? -1;
            var producerCount = model.ProducerReactiveInitCount.ToDecimal() ?? -1;

            if (curCount <= 0)
                ModelState.AddModelError("", "Объём поставки в учтённых единицах должен быть указан и иметь значение больше нуля.");
            if (string.IsNullOrWhiteSpace(model.CurrentState_MesUnits))
                ModelState.AddModelError("", "Для объёма поставки, указанного в учётных единицах, не выбрана единица измерения.");

            if (!string.IsNullOrWhiteSpace(model.ProducerReactiveInitCount))
            {
                // если введены  единицы поставщика - проверяет их
                if (producerCount <= 0)
                    ModelState.AddModelError("","Указан объём поставки в учётных единицах. Его значение должно быть положительным числом.");
                if (string.IsNullOrWhiteSpace(model.ProducerReactiveInitMesUnit))
                    ModelState.AddModelError("","Указан объём поставки в учётных единицах, но для него не выбрана единица измерения.");
            }
            else
            {
                // если не введены единицы поставщика, то дублирует в них учётные единицы
                producerCount = curCount;
                model.ProducerReactiveInitMesUnit = model.CurrentState_MesUnits;
            }

            // Срок годности реактива
            if (!model.UnlimitedLifeTime && model.ValidUntilDate == null)
                ModelState.AddModelError("", "Срок годности реактива не указан: укажите срок годности или поставьте галку о том, что он не ограничен.");
            
            // сохраняет пользовательский выбор галочек на атрибутах перед перезагрузкой данных из базы (будет использован ещё позже)
            var selectedAttributes = model.Attributes
                ?.Where(x => x.Value)
                .Select(x => x.Key)
                .ToArray();
            try
            {
                if (!IsLkksnUnique(model.Lkksn_SpecificationId, model.Lkksn_DeliveryNum, model.Lkksn_Date.Year))
                    ModelState.AddModelError("", $"Серия ЛКК для спецификации [{model.Lkksn_SpecificationId:D3}] с номером поставки [{model.Lkksn_DeliveryNum:D2}] " +
                                                 $"уже зарегистрирована в {model.Lkksn_Date.Year} году. Выберите другой номер поставки. ");
            }
            catch (Exception e)
            {
                return this.Error("Ошибка базы данных",
                    "При проверке уникальности серии ЛКК произошла ошибка. " + e.Message);
            }

            if (!ModelState.IsValid)
            {
                // провал валидации: перезагружает данные из базы и вываливается назад в представление
                try
                {
                    model.Providers = GetProviders();
                    model.Producers = GetProducers();
                    var attributes = GetAttributes();
                    model.Attributes = attributes?.ToDictionary(x => x.Key,
                        x => selectedAttributes?.Any(y => y == x.Key) ?? false);
                    model.AttributesNames = attributes;
                    model.MesUnits = GetMesUnits();
                    model.Stores = GetStores();
                }
                catch (Exception e)
                {
                    return this.Error("Ошибка базы данных",
                        "При загрузке данных из базы данных произошла ошибка. " + e.Message);
                }

                return View(model);
            }

            #endregion

            #region Подготовка SQL запроса и запрос к базе данных
            
            var date = DateTime.Now.ToSqLiteString();
            var name = this.GetUserId();

            var builderStart1 = new StringBuilder
            (
                "begin;" +
                "insert into \"Reactives_Arrivals\" (\"LKKSN\", \"LKKSN_specification_id\", \"LKKSN_delivery_num\", " +
                "\"LKKSN_date\", \"producer_reactive_count\", \"producer_reactive_mes_unit_name\", " +
                "\"start_state_count\", \"start_state_mes_unit_name\", \"current_state_count\", \"current_state_is_written_off\", " +
                "\"created_at_date\", \"created_by\", \"changed_at_date\", \"changed_by\""
            );
            var builderEnd1 = new StringBuilder
            (
                $") values (\'{model.LkksnString}\', {model.Lkksn_SpecificationId}, {model.Lkksn_DeliveryNum}, " +
                $"\'{model.Lkksn_Date.ToSqLiteString()}\', {producerCount.ToSqLiteString()}, \'{model.ProducerReactiveInitMesUnit}\', " +
                $"{curCount.ToSqLiteString()}, \'{model.CurrentState_MesUnits}\', {curCount.ToSqLiteString()}, 0, " +
                $"\'{date}\', \'{name}\', \'{date}\', \'{name}\'"
            );
            var builderStart2 = new StringBuilder
            (
                "insert into \"Reactives_Changes\" (\"LKKSN\", \"count_difference\", \"count_new_value\", " +
                "\"created_at_date\", \"created_by\", \"record_comment\" "
            );
            var builderEnd2 = new StringBuilder
            (
                $") values (\'{model.LkksnString}\', {curCount.ToSqLiteString()}, {curCount.ToSqLiteString()}, " +
                $"\'{date}\', \'{name}\', \'Новая поставка\' "
            );

            void AddTo1(string param, string value, bool isNumeric = false)
            {
                builderStart1.Append($", \"{param}\"");
                builderEnd1.Append($", {(isNumeric ? null : "\'")}{value}{(isNumeric ? null : "\'")}");
            }

            void AddTo2(string param, string value, bool isNumeric = false)
            {
                builderStart2.Append($", \"{param}\"");
                builderEnd2.Append($", {(isNumeric ? null : "\'")}{value}{(isNumeric ? null : "\'")}");
            }

            if (model.SelectedProvider > 0)
                AddTo1("provider_id", model.SelectedProvider.ToString(), true);
            if (model.SelectedProducer > 0)
                AddTo1("producer_id", model.SelectedProducer.ToString(), true);
            if (!string.IsNullOrWhiteSpace(model.ProducerReactiveName))
                AddTo1("producer_reactive_name", model.ProducerReactiveName.Trim());
            if (!string.IsNullOrWhiteSpace(model.ProducerReactiveSn))
                AddTo1("producer_reactive_SN", model.ProducerReactiveSn.Trim());
            if (!string.IsNullOrWhiteSpace(model.CurrentState_Comment))
                AddTo1("current_state_comments", model.CurrentState_Comment);

            if (model.UnlimitedLifeTime)
            {
                AddTo1("current_state_valid_forever", "1", true);
                AddTo2("valid_forever", "1", true);
            }
            else
            {
                var validDate = model.ValidUntilDate?.ToSqLiteString();
                AddTo1("current_state_valid_forever", "0", true);
                AddTo1("current_state_valid_until_date", validDate);
                AddTo2("valid_forever", "0", true);
                AddTo2("valid_until_date", validDate);
            }

            if (!string.IsNullOrWhiteSpace(model.CurrentState_StoredAtInserted))
            {
                AddTo1("current_state_stored_at", model.CurrentState_StoredAtInserted.Trim());
                AddTo2("stored_at", model.CurrentState_StoredAtInserted.Trim());
            }
            else if (!string.IsNullOrWhiteSpace(model.CurrentState_StoredAtSelected))
            {
                AddTo1("current_state_stored_at", model.CurrentState_StoredAtSelected.Trim());
                AddTo2("stored_at", model.CurrentState_StoredAtSelected.Trim());
            }

            if (selectedAttributes != null &&selectedAttributes.Any())
            {
                var str = new string(selectedAttributes);
                AddTo1("current_state_attribute_string", str);
                AddTo2("attribute_string", str);
            }

            var request = builderStart1.ToString() + builderEnd1 + "); " +
                          builderStart2 + builderEnd2 + "); commit;";

            var result = _connector.WriteData(request, out var error);

            if (result < 1)
                return this.Error("Ошибка базы данных", "В ходе записи данных в базу произошла ошибка. " + error);

            return RedirectToAction("Info", new { lkksn = model.LkksnString});

            #endregion

        }

        /// <summary> Просмотр подробной информации по поставке </summary>
        /// <param name="lkksn">Номер поставки</param>
        public IActionResult Info(string lkksn)
        {
            if (string.IsNullOrWhiteSpace(lkksn))
                return this.Error("Системная ошибка",
                    "При попытке просмотреть дополнительную информацию о поставке произошла" +
                    " ошибка из-за того, что в запросе не передана строка с серией ЛКК. " +
                    "Обратитесь к разработчику.");

            var arrivalRow = _connector.ReadData
            (
                "select " +
                    "s.\"name\" as \"specification\", " +
                    "pv.\"name\" as \"provider\", " +
                    "pd.\"name\" as \"producer\", " +
                    "a.\"producer_reactive_name\" as \"reactive_name\", " +
                    "a.\"producer_reactive_SN\" as \"reactive_sn\", " +
                    "a.\"producer_reactive_count\" as \"producer_count\", " +
                    "up.\"name\" as \"producer_mes_unit\", " +
                    "a.\"start_state_count\" as \"init_count\", " +
                    "a.\"current_state_count\" as \"cur_count\", " +
                    "uc.\"name\" as \"cur_mes_unit\", " +
                    "uc.\"short\" as \"cur_mes_unit_short\", " +
                    "a.\"current_state_is_written_off\" as \"cur_is_written_off\", " +
                    "a.\"current_state_valid_forever\" as \"cur_valid_forever\", " +
                    "a.\"current_state_valid_until_date\" as \"cur_valid_until\", " +
                    "a.\"current_state_stored_at\" as \"cur_stored_at\", " +
                    "a.\"current_state_attribute_string\" as \"attributes\", " +
                    "a.\"current_state_comments\" as \"comments\", " +
                    "a.\"created_at_date\", " +
                    "u1.\"name\" as \"created_by\", " +
                    "a.\"changed_at_date\", " +
                    "u2.\"name\" as \"changed_by\" " +
                "from \"Reactives_Arrivals\" a " +
                    "left join \"Reactives_Specifications\" s on a.\"LKKSN_specification_id\" = s.\"Id\" " +
                    "left join \"Reactives_Mes_Units\" uc on a.\"start_state_mes_unit_name\" = uc.\"name\" " +
                    "left join \"Reactives_Mes_Units\" up on a.\"producer_reactive_mes_unit_name\" = up.\"name\" " +
                    "left join \"Users\" u1 on a.\"created_by\" = u1.\"Id\" " +
                    "left join \"Users\" u2 on a.\"changed_by\" = u2.\"Id\" " +
                    "left join \"Reactives_Providers\" pv on a.\"provider_id\" = pv.\"Id\" " +
                    "left join \"Reactives_Producers\" pd on a.\"producer_id\" = pd.\"Id\" " +
                $"where a.\"LKKSN\" = \'{lkksn.Trim()}\';",
                out var error
            )?.Rows[0];

            if (arrivalRow == null)
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить данные по серии ЛКК произошла ошибка. " + error +
                    " Данные не были загружены.");

            var changesData = _connector.ReadData
            (
                "select " +
                    "c.\"created_at_date\" as \"date\", " +
                    "u.\"name\" as \"created_by\", " +
                    "c.\"record_comment\", " +
                    "c.\"count_difference\" as \"delta_count\", " +
                    "c.\"count_new_value\" as \"new_count\", " +
                    "c.\"is_written_off\", " +
                    "c.\"valid_forever\", " +
                    "c.\"valid_until_date\", " +
                    "c.\"stored_at\", " +
                    "c.\"attribute_string\" as \"attributes\", " +
                    "c.\"other_changes\" " +
                "from \"Reactives_Changes\" c " +
                    "inner join \"Reactives_Arrivals\" a on a.\"LKKSN\" = c.\"LKKSN\" " +
                    "left join \"Users\" u on u.\"Id\" = c.\"created_by\" " +
                $"where c.\"LKKSN\" = \'{lkksn.Trim()}\' " +
                "order by c.\"Id\";",
                out error
            );

            if (changesData == null || changesData.Rows.Count < 1)
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить историю по серии ЛКК произошла ошибка. " + error +
                    " Данные не были загружены.");

            if (!_connector.TryReadData("select \"code\", \"name\" from \"Reactives_Attributes\";",
                out var attributesData, out error))
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить атрибуты поставок произошла ошибка. " + error +
                    " Данные не были загружены.");

            return View(new ReactivesInfoModel
            {
                Lkksn = lkksn,
                Specification = arrivalRow.GetValue("specification"),
                ProviderName = arrivalRow.GetValue("provider"),
                ProducerName = arrivalRow.GetValue("producer"),
                ReactiveName = arrivalRow.GetValue("reactive_name"),
                ReactiveSn = arrivalRow.GetValue("reactive_sn"),
                ProducerCount = arrivalRow.GetDecimal("producer_count"),
                ProducerMesUnit = arrivalRow.GetValue("producer_mes_unit"),
                CurCount = arrivalRow.GetDecimal("cur_count") ?? 0,
                CurMesUnit = arrivalRow.GetValue("cur_mes_unit"),
                CurIsWrittenOff = arrivalRow.GetInt("cur_is_written_off") > 0,
                CurIsValidForever = arrivalRow.GetInt("cur_valid_forever") > 0,
                CurValidUntil = arrivalRow.GetDate("cur_valid_until"),
                CurStoredAt = arrivalRow.GetValue("cur_stored_at"),
                Created = arrivalRow.GetValue("created_by") + " " +
                          arrivalRow.GetDate("created_at_date")?.ToString("dd.MM.yyyy HH:mm:ss"),
                Changed = arrivalRow.GetValue("changed_by") + " " +
                          arrivalRow.GetDate("changed_at_date")?.ToString("dd.MM.yyyy HH:mm:ss"),
                StartCount = arrivalRow.GetDecimal("init_count"),
                CurMesUnitShort = arrivalRow.GetValue("cur_mes_unit_short"),
                Comments = arrivalRow.GetValue("comments"),
                CurAttributesNames = attributesData == null || attributesData.Rows.Count < 1
                    ? null
                    : attributesData.Rows.Cast<DataRow>()
                        .Where(x =>
                            {
                                var str = arrivalRow.GetValue("attributes");
                                return !string.IsNullOrWhiteSpace(str) && str.Contains(x.GetValue("code"));
                            })
                        .Select(x => "[" + x.GetValue("code") + "] " + x.GetValue("name")),
                Changes = changesData.Rows.Cast<DataRow>()
                    .Select(x => new ReactivesInfoHistoryModel
                    {
                        Date = x.GetDate("date"),
                        CreatedBy = x.GetValue("created_by"),
                        RecordComment = x.GetValue("record_comment"),
                        DeltaCount = x.GetDecimal("delta_count"),
                        NewCount = x.GetDecimal("new_count"),
                        IsWrittenOff = string.IsNullOrWhiteSpace(x.GetValue("is_written_off"))
                            ? null
                            : (bool?) (x.GetValue("is_written_off") == "1"),
                        IsValidForever = x.GetValue("valid_forever") == "1",
                        ValidUntilDate = x.GetDate("valid_until_date"),
                        StoredAt = x.GetValue("stored_at"),
                        Attributes = x.GetValue("attributes"),
                        OtherChanges = x.GetValue("other_changes")
                    })
            });
        }

        /// <summary> Вход в инструмент "израсходовать реагент"</summary>
        /// <param name="lkksn">Серия ЛКК для поставки</param>
        [HttpGet]
        public IActionResult UseUp(string lkksn)
        {
            if (string.IsNullOrWhiteSpace(lkksn))
                return this.Error("Системная ошибка",
                    "Пои попытке загрузить инструмент расходования" +
                    "реагента произошла ошибка. Инструменту не передан номер серии ЛКК. " +
                    "Обратитесь к разработчику");

            var data = _connector.ReadData
            (
                "select \"current_state_count\" as \"current_count\", " +
                "\"start_state_mes_unit_name\" as \"mes_units\"," +
                "\"current_state_is_written_off\" as \"is_already_written_off\" " +
                "from \"Reactives_Arrivals\" " +
                $"where \"LKKSN\" = \'{lkksn}\';",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить инструмент расходования реагента произошла ошибка. " + error +
                    " Данные о текущем количестве реагента и единицах измерения не загружены.");

            var count = data.Rows[0].GetDecimal("current_count") ?? 0;

            return View(new ReactivesUseUpModel
            {
                Lkksn = lkksn,
                CurrentCount = count,
                IsAlready_WrittenOff = data.Rows[0].GetValue("is_already_written_off") == "1",
                MesUnits = data.Rows[0].GetValue("mes_units"),
                ChangeCount = count.ToString(CultureInfo.InvariantCulture),
                WriteOff_IfEnds = true
            });
        }

        /// <summary> Подтверждение расходования реактива</summary>
        /// <param name="model">Модель расходования реактива</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult UseUp(ReactivesUseUpModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Lkksn))
                return this.Error("Системная ошибка",
                    "При попытке израсходовать реагент произошла ошибка. В запросе отсутствует указание " +
                    "на серию ЛКК. Обратитесь к разработчику.");

            var delta = model.ChangeCount.ToDecimal() ?? -1;
            if (delta <= 0)
                ModelState.AddModelError("", 
                    "Ошибка формата в поле количества списываемого реагента. " +
                    "Размер списания должен быть целым или дробным положительным числом. ");

            if (!ModelState.IsValid) return View(model);

            if (delta > model.CurrentCount) delta = model.CurrentCount;
            var newCount = model.CurrentCount - delta;
            var writeOff = !model.IsAlready_WrittenOff && delta == model.CurrentCount && model.WriteOff_IfEnds;
            var user = this.GetUserId();
            var now = DateTime.Now.ToSqLiteString();

            var request =
                "begin; update \"Reactives_Arrivals\" " + 
                $"set \"current_state_count\" = {newCount.ToSqLiteString()}, \"changed_by\" = {user}, \"changed_at_date\" = \'{now}\'" +
                (writeOff ? ", \"current_state_is_written_off\" = 1 " : " ") +
                $"where \"LKKSN\" = \'{model.Lkksn}\'; " +
                "insert into \"Reactives_Changes\" (\"LKKSN\", \"count_difference\", \"count_new_value\", " +
                "\"created_at_date\", \"created_by\", \"record_comment\"" + (writeOff ? ", \"is_written_off\")" : ")") +
                $" values (\'{model.Lkksn}\', {(-delta).ToSqLiteString()}, {newCount.ToSqLiteString()}, \'{now}\', {user}, \'Расходование\'" +
                (writeOff ? ", 1);" : ");") + " commit;";

            var result = _connector.WriteData(request, out var error);
            return result < 2
                ? this.Error("Ошибка базы данных", "При внесении изменений произошла ошибка. " + error)
                : RedirectToAction("Info", "Reactives", new { lkksn = model.Lkksn });
        }

        /// <summary> Вход в инструмент списания/восстановления </summary>
        /// <param name="lkksn">Номер серии ЛКК</param>
        [HttpGet]
        public IActionResult WriteOnOff(string lkksn)
        {
            if (string.IsNullOrWhiteSpace(lkksn))
                return this.Error("Системная ошибка",
                    "При попытке загрузить инструмент изменения статуса списания для поставки произошла ошибка." +
                    " Серия ЛКК не была передана. Обратитесь к разработчику.");

            var data = _connector.ReadData
            (
                "select \"current_state_count\" as \"current_count\", \"start_state_mes_unit_name\" as \"mes_units\", " +
                "\"current_state_is_written_off\" as \"is_already_written_off\" " +
                $"from \"Reactives_Arrivals\" where \"LKKSN\" = \'{lkksn}\';",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);

            return View(new ReactivesWriteOnOffModel
            {
                Lkksn = lkksn,
                CurrentCount = data.Rows[0].GetDecimal("current_count") ?? 0,
                MesUnits = data.Rows[0].GetValue("mes_units"),
                IsAlready_WrittenOff = data.Rows[0].GetValue("is_already_written_off") == "1",
                SetWrittenOff = -1
            });
        }

        /// <summary> Установить новый статус списания</summary>
        /// <param name="model">Модель списания</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult WriteOnOff(ReactivesWriteOnOffModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Lkksn))
                return this.Error("Системная ошибка",
                    "При попытке изменения данных произошла ошибка. В запросе не передано значение серии ЛКК. " +
                    "Обратитесь к разработчику.");

            if (model.SetWrittenOff < 0)
                ModelState.AddModelError("", "Не выбран новый статус списания.");
            if (!model.IgnoreNotNullCount && model.CurrentCount > 0 && model.SetWrittenOff == 1)
                ModelState.AddModelError("",
                    "Текущие остатки больше нуля. Чтобы принудительно списать реагент с ненулевыми остатками," +
                    "отметьте галочкой флажок \'Игнорировать ненулевые остатки\'. Чтобы одновременно израсходовать и " +
                    " списать поставку, воспользуйтесь инструментов расходования.");
            if (model.IsAlready_WrittenOff && model.SetWrittenOff == 1
               || !model.IsAlready_WrittenOff && model.SetWrittenOff == 0)
                ModelState.AddModelError("","Поставка уже имеет выбранный статус списания. Изменение не требуется.");

            if (!ModelState.IsValid) return View(model);

            var user = this.GetUserId();
            var now = DateTime.Now.ToSqLiteString();

            var request = "begin; " +
                          "update \"Reactives_Arrivals\" " +
                          $"set \"current_state_is_written_off\" = {model.SetWrittenOff}, " +
                          $"\"changed_at_date\" = \'{now}\', \"changed_by\" = {user} " +
                          $"where \"LKKSN\" = \'{model.Lkksn}\'; " +
                          "insert into \"Reactives_Changes\" (\"LKKSN\", \"is_written_off\", \"created_at_date\", \"created_by\", \"record_comment\") " +
                          $"values (\'{model.Lkksn}\', {model.SetWrittenOff}, \'{now}\', {user}, \'Статус списания\');" +
                          "commit;";

            var result = _connector.WriteData(request, out var error);
            return result < 2
                ? this.Error("Ошибка базы данных", "При обновлении данных произошла ошибка. " + error)
                : RedirectToAction("Info", "Reactives", new {lkksn = model.Lkksn});
        }

        /// <summary> Вход в инструмент "общие изменения"</summary>
        /// <param name="lkksn">Номер серии ЛКК, подлежащей изменению</param>
        [HttpGet]
        public IActionResult Change(string lkksn)
        {
            if (string.IsNullOrWhiteSpace(lkksn))
                return this.Error("Системная ошибка",
                    "При выходе в инструмент редактирования произошла ошибка. В запросе отсутствует" +
                    " номер серии ЛКК для идентификации изменений. Обратитесь к разработчику.");

            var data = _connector.ReadData
            (
                "select " +
                    "pv.\"name\" as \"provider\", a.\"provider_id\", pd.\"name\" as \"producer\", a.\"producer_id\", " +
                    "a.\"producer_reactive_name\" as \"reactive_name\", a.\"current_state_count\" as \"cur_count\", " +
                    "a.\"producer_reactive_SN\" as \"reactive_sn\", a.\"start_state_count\" as \"start_count\", " +
                    "u2.\"short\" as \"start_mes_units\", \"current_state_valid_forever\" as \"valid_forever\", " +
                    "a.\"producer_reactive_count\" as \"prod_count\", u1.\"short\" as \"prod_mes_units\", " +
                    "a.\"current_state_valid_until_date\" as \"valid_until\", a.\"current_state_stored_at\" as \"stored_at\", " +
                    "a.\"current_state_comments\" as \"comments\", a.\"current_state_attribute_string\" as \"attributes\" " +
                "from \"Reactives_Arrivals\" a " +
                    "left join \"Reactives_Providers\" pv on a.\"provider_id\" = pv.\"Id\" " +
                    "left join \"Reactives_Producers\" pd on a.\"producer_id\" = pd.\"Id\" " +
                    "left join \"Reactives_Mes_Units\" u1 on a.\"producer_reactive_mes_unit_name\" = u1.\"name\" " +
                    "left join \"Reactives_Mes_Units\" u2 on a.\"start_state_mes_unit_name\" = u2.\"name\" " +
                $"where a.\"LKKSN\" = \'{lkksn}\';",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Ошибка базы данных", "При попытке загрузить данные из базы произошла ошибка. " + error);

            var row = data.Rows[0];
            var isValidForever = row.GetInt("valid_forever") > 0;
            var validUntil = row.GetDate("valid_until");
            var model = new ReactivesChangeModel
            {
                Lkksn = lkksn,
                Provider_Old = row.GetValue("provider") ?? "-",
                ProviderId_Old = row.GetValue("provider_id"),
                Producer_Old = row.GetValue("producer") ?? "-",
                ProducerId_Old = row.GetValue("producer_id"),
                ReactiveName_Old = row.GetValue("reactive_name") ?? "-",
                ReactiveName_New = row.GetValue("reactive_name"),
                ReactiveSn_Old = row.GetValue("reactive_sn") ?? "-",
                ReactiveSn_New = row.GetValue("reactive_sn"),
                StartCountString = row.GetDecimal("start_count") + " " + row.GetValue("start_mes_units"),
                ProducerCountString = row.GetDecimal("prod_count") + " " + row.GetValue("prod_mes_units"),
                CurrentCountString = row.GetDecimal("cur_count") + " " + row.GetValue("start_mes_units"),
                ValidUntil_Old = validUntil,
                UnlimitedLifeTime_Old = isValidForever,
                ValidUntil_New = validUntil,
                UnlimitedLifeTime_New = isValidForever,
                StoredAt_Old = row.GetValue("stored_at") ?? "-",
                Comment_Old = row.GetValue("comments") ?? "-",
                Comment_New = row.GetValue("comments")
            };

            try
            {
                model.Providers = GetProviders()?.Select(x => new SelectListItem(x.Text, x.Value, model.Provider_Old == x.Text));
                model.Producers = GetProducers()?.Select(x => new SelectListItem(x.Text, x.Value, model.Producer_Old == x.Text));
                model.Stores = GetStores()?.Select(x => new SelectListItem(x.Text, x.Value, model.StoredAt_Old == x.Text));

                model.AttributesNames = GetAttributes();
                model.AttributesString_Old = row.GetValue("attributes");
                model.Attributes_Old = string.IsNullOrWhiteSpace(model.AttributesString_Old)
                    ? null
                    : model.AttributesString_Old
                        .Where(x => model.AttributesNames?.ContainsKey(x) ?? false)
                        .Select(x => "[" + x + "] " + model.AttributesNames[x]);
                model.Attributes_New = model.AttributesNames
                    ?.ToDictionary(x => x.Key, y => model.AttributesString_Old?.Contains(y.Key) ?? false);
            }
            catch (Exception e)
            {
                this.Error("Ошибка базы данных","При загрузке данных из базы произошла ошибка. " + e.Message);
            }

            return View(model);
        }

        /// <summary> Регистрация общих изменений</summary>
        /// <param name="model">Модель изменения</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Change(ReactivesChangeModel model)
        {
            // валидация
            if (model == null)
                return this.Error("Системная ошибка", 
                    "При загрузке данных из запроса возникла ошибка. Модель данных" +
                    " не получена с запросом.");
            
            var providerChanges = model.ProviderId_New != model.ProviderId_Old;
            var producerChanges = model.ProducerId_New != model.ProducerId_Old;
            var reactiveNameChanges = model.ReactiveName_New != (model.ReactiveName_Old == "-" ? null : model.ReactiveName_Old);
            var reactiveSnChanges = model.ReactiveSn_New != (model.ReactiveSn_Old == "-" ? null : model.ReactiveSn_Old);
            var validUntilChanges = model.ValidUntil_New != model.ValidUntil_Old
                                    || model.UnlimitedLifeTime_New != model.UnlimitedLifeTime_Old;
            if (validUntilChanges && !model.UnlimitedLifeTime_New && model.ValidUntil_New == null)
                ModelState.AddModelError("",
                    "Новый срок годности реактива не выбран. Выберите дату или поставьте галочку \"бессрочно\"");

            var storedAtChanges = string.IsNullOrWhiteSpace(model.StoredAt_NewInserted)
                ? model.StoredAt_NewSelected != (model.StoredAt_Old == "-" ? null : model.StoredAt_Old)
                : model.StoredAt_NewInserted != (model.StoredAt_Old == "-" ? null : model.StoredAt_Old);

            var commentChanges = model.Comment_New != (model.Comment_Old == "-" ? null : model.Comment_Old);

            bool attributesChanges;
            if (string.IsNullOrWhiteSpace(model.AttributesString_Old))
            {
                attributesChanges = model.Attributes_New.Any(x => x.Value);
            }
            else
            {
                attributesChanges = model.Attributes_New // выбран какой-либо новый атрибут
                    .Any(x => x.Value && !model.AttributesString_Old.Contains(x.Key));
                attributesChanges |= model.AttributesString_Old // снят какой-то из атрибутов
                    .Any(x => !model.Attributes_New.ContainsKey(x) || !model.Attributes_New[x]);
            }

            if (!providerChanges
                && !producerChanges
                && !reactiveNameChanges
                && !reactiveSnChanges
                && !validUntilChanges
                && !storedAtChanges
                && !commentChanges
                && !attributesChanges) 
                ModelState.AddModelError("", "Не внесено никаких изменений");
            if (!ModelState.IsValid)
            {
                try // если что-то не то, вновь подгружает недостающие элементы этого вида и вываливает его
                {
                    model.Providers = GetProviders()?.Select(x => new SelectListItem(x.Text, x.Value, model.ProviderId_New == x.Text));
                    model.Producers = GetProducers()?.Select(x => new SelectListItem(x.Text, x.Value, model.ProducerId_New == x.Text));
                    model.Stores = GetStores()?.Select(x => new SelectListItem(x.Text, x.Value, model.StoredAt_NewSelected == x.Text));

                    model.AttributesNames = GetAttributes();
                    model.Attributes_Old = string.IsNullOrWhiteSpace(model.AttributesString_Old)
                        ? null
                        : model.AttributesString_Old
                            .Where(x => model.AttributesNames?.ContainsKey(x) ?? false)
                            .Select(x => "[" + x + "] " + model.AttributesNames[x]);
                }
                catch (Exception e)
                {
                    this.Error("Ошибка базы данных", "При загрузке данных из базы произошла ошибка. " + e.Message);
                }

                return View(model);
            }

            // изменение данных
            var now = DateTime.Now.ToSqLiteString();
            var user = this.GetUserId();
            var builder1 = new StringBuilder("begin; update \"Reactives_Arrivals\" set " +
                                             $"\"changed_at_date\" = \'{now}\', \"changed_by\" = {user}");
            var builder2OtherChanges = new StringBuilder();
            var builder2Start = new StringBuilder($" where \"LKKSN\" = \'{model.Lkksn}\'; " +
                                                  "insert into \"Reactives_Changes\" (\"LKKSN\", \"created_at_date\", " +
                                                  "\"created_by\", \"record_comment\" ");
            var builder2End = new StringBuilder($") values (\'{model.Lkksn}\', \'{now}\', {user}, \'Простая правка\'");

            void Add1(string name, string value, bool isNumeric = false) 
                => builder1.Append($", \"{name}\" = " +
                (string.IsNullOrWhiteSpace(value)
                    ? "null"
                    : $"{(isNumeric ? value : $"\'{value}\'")}"));

            void Add2(string name, string value, bool isNumeric = false)
            {
                builder2Start.Append($", \"{name}\"");
                builder2End.Append(", " + (isNumeric ? value : $"\'{value}\'"));
            }

            void AddOther(string name, string oldValue, string newValue)
                => builder2OtherChanges.Append($"\\n- поле [{name}]: значение [{oldValue}] заменено на [{newValue ?? "-"}]");
            
            if (providerChanges)
            {
                var newProviderName = GetProviders()
                    ?.Where(x => x.Value == model.ProviderId_New)
                    .Select(x => x.Text)
                    .FirstOrDefault();
                Add1("provider_id", model.ProviderId_New, true);
                AddOther("Поставщик", model.Provider_Old, newProviderName);
            }

            if(producerChanges)
            {
                var newProducerName = GetProducers()
                    ?.Where(x => x.Value == model.ProducerId_New)
                    .Select(x => x.Text)
                    .FirstOrDefault();
                Add1("producer_id", model.ProducerId_New, true);
                AddOther("Производитель", model.Producer_Old, newProducerName);
            }

            if (reactiveNameChanges)
            {
                Add1("producer_reactive_name", model.ReactiveName_New);
                AddOther("Название реактива", model.ReactiveName_Old, model.ReactiveName_New);
            }

            if (reactiveSnChanges)
            {
                Add1("producer_reactive_SN", model.ReactiveSn_New);
                AddOther("Серийный номер реактива", model.ReactiveSn_Old, model.ReactiveSn_New);
            }

            if (validUntilChanges)
            {
                Add1("current_state_valid_forever", model.UnlimitedLifeTime_New ? "1" : "0", true);
                Add2("valid_forever", model.UnlimitedLifeTime_New ? "1" : "0");
                var validUntilNew = model.ValidUntil_New?.ToSqLiteString();
                if (string.IsNullOrWhiteSpace(validUntilNew))
                {
                    Add1("current_state_valid_until_date", "null", true);
                    Add2("valid_until_date", "null", true);
                }
                else
                {
                    Add1("current_state_valid_until_date", validUntilNew);
                    Add2("valid_until_date", validUntilNew);
                }
            }

            if (storedAtChanges)
            {
                var newStoredAt = !string.IsNullOrWhiteSpace(model.StoredAt_NewInserted)
                    ? model.StoredAt_NewInserted
                    : model.StoredAt_NewSelected;
                Add1("current_state_stored_at", newStoredAt);
                Add2("stored_at", newStoredAt);
            }

            if (commentChanges)
            {
                Add1("current_state_comments", model.Comment_New);
                AddOther("Комментарии", model.Comment_Old, model.Comment_New);
            }

            if (attributesChanges)
            {
                var newAttributeString = new string
                (
                    model.Attributes_New
                        .Where(x => x.Value)
                        .Select(x => x.Key)
                        .OrderBy(x => x)
                        .ToArray()
                );

                Add1("current_state_attribute_string", newAttributeString);
                Add2("attribute_string", newAttributeString);
            }

            if (builder2OtherChanges.Length > 0) Add2("other_changes", builder2OtherChanges.ToString());

            var request = builder1.ToString() + builder2Start + builder2End + "); commit;";

            return _connector.WriteData(request, out var error) < 2 
                ? this.Error("Ошибка базы данных", "При записи новых данных произошла ошибка. " + error) 
                : RedirectToAction("Info", "Reactives", new {lkksn = model.Lkksn});
        }

        /// <summary> Вход в инструмент редактирования </summary>
        /// <param name="lkksn">Номер серии ЛКК</param>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult EditReactiveSpecification(string lkksn)
        {
            if (string.IsNullOrWhiteSpace(lkksn))
                return this.Error("Системная ошибка",
                    "При выходе в инструмент редактирования произошла ошибка. В запросе отсутствует" +
                    " номер серии ЛКК для идентификации изменений. Обратитесь к разработчику.");

            var data = _connector.ReadData
            (
                "select \"LKKSN_specification_id\", \"LKKSN_delivery_num\", \"LKKSN_date\" " +
                $"from \"Reactives_Arrivals\" where \"LKKSN\" = \'{lkksn}\';",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Ошибка базы данных", "При попытке загрузить информацию из базы произошла ошибка. " + error);

            var specificationId = data.Rows[0].GetValue("LKKSN_specification_id");
            IEnumerable<SelectListItem> specifications;
            try
            {
                specifications = GetSpecifications()
                    ?.Select(x => new SelectListItem(x.Value + " - " + x.Text, x.Value, x.Value == specificationId));
            }
            catch (Exception e)
            {
                return this.Error("Ошибка базы данных", "При попытке загрузить информацию из базы данных произошла ошибка" + e.Message);
            }

            return View(new ReactivesEditSpecificationModel
            {
                Lkksn = lkksn,
                Specifications = specifications,
                InsertedSpecification = null,
                DeliveryNumber = data.Rows[0].GetInt("LKKSN_delivery_num") ?? 0,
                Date = data.Rows[0].GetDate("LKKSN_date")
            });
        }

        /// <summary> Подтверждение изменений</summary>
        /// <param name="model">Модель данных для изменений</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult EditReactiveSpecification(ReactivesEditSpecificationModel model)
        {
            // валидация
            if (model == null)
                return this.Error("Системная ошибка", "В запросе отсутствует модель данных. Обратитесь к разработчику");

            var selectedSpecification = int.TryParse(model.SelectedSpecification, out var i)
                ? i
                : -1;

            if ((model.InsertedSpecification ?? 0) <= 0 && selectedSpecification <= 0)
                ModelState.AddModelError("", "Новая спецификация не выбрана.");

            var newSpec = model.InsertedSpecification ?? selectedSpecification;
            try
            {
                model.Specifications = GetSpecifications()
                    ?.Select(x => new SelectListItem(x.Value + " - " + x.Text, x.Value, x.Value == newSpec.ToString()));
            }
            catch (Exception e)
            {
                return this.Error("Ошибка базы данных", "При попытке загрузить информацию из базы данных произошла ошибка" + e.Message);
            }


            if (model.DeliveryNumber <= 0)
                ModelState.AddModelError("", "Номер поставки должен быть больше нуля.");
            if (model.Date == null)
                ModelState.AddModelError("", "Дата поставки не выбрана");
            
            var dropNextSteps = false;

            if (model.NewLkksn == model.Lkksn)
            {
                ModelState.AddModelError("", "Вы не ввели никакой новой информации. Изменения не требуются.");
                dropNextSteps = true;
            }

            if (model.Specifications?.All(x => x.Value != newSpec.ToString()) ?? true)
            {
                ModelState.AddModelError("",
                    "Введённый номер спецификации не обнаружен в базе данных. Введите существующий номер спецификации, " +
                    "либо добавьте новую спецификацию.");
                dropNextSteps = true;
            }

            if (!dropNextSteps && model.DeliveryNumber > 0)
            {
                try
                {
                    if (!IsLkksnUnique(newSpec, model.DeliveryNumber, model.Date?.Year ?? 0))
                        ModelState.AddModelError("", $"Серия ЛКК для спецификации [{newSpec:D3}] с номером поставки [{model.DeliveryNumber:D2}] " +
                                                     $"уже зарегистрирована в {model.Date?.Year} году. Выберите другой номер поставки. ");
                }
                catch (Exception e)
                {
                    return this.Error("Ошибка базы данных",
                        "При проверке уникальности серии ЛКК произошла ошибка. " + e.Message);
                }
            }

            if (!ModelState.IsValid) return View(model);

            // изменения в базе данных
            var user = this.GetUserId();
            var now = DateTime.Now.ToSqLiteString();
            var request = "begin; " +
                $"update \"Reactives_Arrivals\" set \"LKKSN\" = \'{model.NewLkksn}\', \"LKKSN_specification_id\" = {newSpec}, " +
                    $"\"LKKSN_delivery_num\" = {model.DeliveryNumber}, \"LKKSN_date\" = \'{model.Date?.ToSqLiteString()}\', " +
                    $"\"changed_at_date\" = \'{now}\', \"changed_by\" = {user} " +
                    $"where \"LKKSN\" = \'{model.Lkksn}\'; " +
                "insert into \"Reactives_Changes\" (\"LKKSN\", \"created_at_date\", \"created_by\", \"record_comment\", \"other_changes\") " +
                    $"values (\'{model.NewLkksn}\', \'{now}\', {user}, \'Адм.: Смена серии ЛКК\', \'Серия ЛКК изменена с [{model.Lkksn}] на [{model.NewLkksn}]\'); " +
                $"update \"Reactives_Changes\" set \"LKKSN\" = \'{model.NewLkksn}\' where \"LKKSN\" = \'{model.Lkksn}\'; " +
                "commit; ";

            return _connector.WriteData(request, out var error2) > 0
                ? RedirectToAction("Change", "Reactives", new {lkksn = model.NewLkksn})
                : this.Error("Ошибка базы данных", "При изменении данных произошла ошибка. " + error2);
        }

        /// <summary> Вход в инструмент для редактирования сумм реагентов </summary>
        /// <param name="lkksn">Номер серии ЛКК</param>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult EditCounts(string lkksn)
        {
            if (string.IsNullOrWhiteSpace(lkksn))
                return this.Error("Системная ошибка",
                    "При передаче запроса отсутствовал номер серии ЛКК. " +
                    "Обратитесь к разработчику.");

            var data = _connector.ReadData
            (
                "select " +
                "\"producer_reactive_count\" as \"producer_count\", " +
                "\"producer_reactive_mes_unit_name\" as \"producer_mes_units\", " +
                "\"start_state_count\" as \"start_count\", " +
                "\"start_state_mes_unit_name\" as \"start_mes_units\", " +
                "\"current_state_count\" as \"cur_count\"" +
                "from \"Reactives_Arrivals\" " +
                $"where \"LKKSN\" = \'{lkksn}\';",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);

            var rowsData = _connector.ReadData
            (
                "select \"Id\", \"record_comment\", \"count_difference\", \"count_new_value\", \"created_at_date\" " +
                "from \"Reactives_Changes\" " +
                $"where \"LKKSN\" = \'{lkksn}\';",
                out error
            );
            
            if (rowsData == null || rowsData.Rows.Count < 1)
                return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);

            var pCount = data.Rows[0].GetDecimal("producer_count");
            var pUnit = data.Rows[0].GetValue("producer_mes_units");
            var sCount = data.Rows[0].GetDecimal("start_count");
            var cCount = data.Rows[0].GetDecimal("cur_count");
            var sUnit = data.Rows[0].GetValue("start_mes_units");
            var units = GetMesUnits().ToArray();

            return View(new ReactivesEditCountsModel
            {
                Lkksn = lkksn,
                ProducerCount_Old = pCount,
                ProducerCount_New = pCount.ToString(),
                ProducerMesUnits_Old = pUnit,
                ProducerMesUnits_New = pUnit,
                StartCount_Old = sCount ?? 0,
                StartCount_New = sCount.ToString(),
                StartMesUnits_Old = sUnit,
                StartMesUnits_New = sUnit,
                CurrentCount_Old = cCount ?? 0,
                CurrentCount_New = cCount.ToString(),
                MesUnitsProducer = units.Select(x => new SelectListItem(x.Text, x.Value, x.Text == pUnit)),
                MesUnitsCounted = units.Select(x => new SelectListItem(x.Text, x.Value, x.Text == sUnit)),
                Rows = rowsData.Rows.Cast<DataRow>()
                    .Select(x => new ReactivesEditCountRowModel
                    {
                        Id = x.GetInt("Id"),
                        Lkksn = lkksn,
                        RecordComment = x.GetValue("record_comment"),
                        RecordDate_Old = x.GetDate("created_at_date"),
                        Delta_Old = x.GetDecimal("count_difference"),
                        Count_Old = x.GetDecimal("count_new_value")
                    })
            });
        }

        /// <summary> Подтверждение изменений </summary>
        /// <param name="model">Модель изменений</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult EditCounts(ReactivesEditCountsModel model)
        {
            // приемлемость данных формы
            if (model == null)
                return this.Error("Системная ошибка", "Модель данных отсутствует в запросе. Обратитесь к разработчику");

            if (string.IsNullOrWhiteSpace(model.Lkksn))
                return this.Error("Системная ошибка", "Модель данных не содержит серии ЛКК. Обратитесь к разработчику");

            var newPCount = model.ProducerCount_New.ToDecimal();
            var isNewPCount = model.ProducerCount_Old != newPCount;

            if (isNewPCount && newPCount < 0)
                ModelState.AddModelError("",
                    "Введено некорректное значение для нового значения \"Объём поставки в единицах производителя\". " +
                    "Ожидается целое или дробное неотрицательное число, либо пустая строка.");

            var isNewPUnits = model.ProducerMesUnits_Old != model.ProducerMesUnits_New;
            if (isNewPUnits || isNewPCount)
            {
                if (newPCount != null && string.IsNullOrWhiteSpace(model.ProducerMesUnits_New))
                    ModelState.AddModelError("",
                        "Для введённых непустого объёма поставки в единицах производителя " +
                        "не указана единица измерения. Выберите единицу измерения, либо сотрите объём поставки.");
                else if (newPCount == null) model.ProducerMesUnits_New = null;
            }

            var newSCount = model.StartCount_New.ToDecimal() ?? -1;
            var isNewSCount = model.StartCount_Old != newSCount;

            if (isNewSCount && newSCount < 0)
                ModelState.AddModelError("",
                    "Введено некорректное значение для нового значения \"Объём поставки в учётных единицах\". " +
                    "Ожидается целое или дробное неотрицательное число.");

            var isNewSUnits = model.StartMesUnits_New != model.StartMesUnits_Old;
            if (isNewSUnits && string.IsNullOrWhiteSpace(model.StartMesUnits_New))
                ModelState.AddModelError("",
                    "Не выбрана единица измерения для нового значения \"Объём поставки в учётных единицах\".");

            var newCCount = model.CurrentCount_New.ToDecimal() ?? -1;
            var isNewCCount = model.CurrentCount_Old != newCCount;
            
            if (isNewCCount && newCCount < 0)
                ModelState.AddModelError("",
                    "Введено некорректное значение для нового значения \"Текущие остатки в учётных единицах\". " +
                    "Ожидается целое или дробное неотрицательное число.");

            if (!isNewPCount && !isNewSCount && !isNewPUnits && !isNewSUnits && !isNewCCount)
                ModelState.AddModelError("", "Вы не ввели никаких новых данных. Изменение не требуется.");

            if (!ModelState.IsValid)
            {
                
                var rowsData = _connector.ReadData
                (
                    "select \"Id\", \"record_comment\", \"count_difference\", \"count_new_value\", \"created_at_date\" " +
                    "from \"Reactives_Changes\" " +
                    $"where \"LKKSN\" = \'{model.Lkksn}\';",
                    out var error
                );
            
                if (rowsData == null || rowsData.Rows.Count < 1)
                    return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);

                var units = GetMesUnits().ToArray();

                model.Rows = rowsData.Rows.Cast<DataRow>()
                    .Select(x => new ReactivesEditCountRowModel
                    {
                        Id = x.GetInt("Id"),
                        Lkksn = model.Lkksn,
                        RecordComment = x.GetValue("record_comment"),
                        RecordDate_Old = x.GetDate("created_at_date"),
                        Delta_Old = x.GetDecimal("count_difference"),
                        Count_Old = x.GetDecimal("count_new_value")
                    });
                model.MesUnitsProducer = units.Select(x => new SelectListItem(x.Text, x.Value, x.Text == model.ProducerMesUnits_New));
                model.MesUnitsCounted = units.Select(x => new SelectListItem(x.Text, x.Value, x.Text == model.StartMesUnits_New));
                return View(model);
            }

            // изменение данных
            var user = this.GetUserId();
            var now = DateTime.Now.ToSqLiteString();

            var request = "begin; " +
                          "update \"Reactives_Arrivals\" set " +
                          (isNewPCount
                              ? $"\"producer_reactive_count\" = {newPCount?.ToSqLiteString() ?? "null"}, "
                              : null) +
                          (isNewPUnits
                              ? $"\"producer_reactive_mes_unit_name\" = {(string.IsNullOrWhiteSpace(model.ProducerMesUnits_New) ? "null" : $"\'{model.ProducerMesUnits_New}\'")}, "
                              : null) +
                          (isNewSCount 
                              ? $"\"start_state_count\" = \'{newSCount.ToSqLiteString()}\', " 
                              : null) +
                          (isNewSUnits 
                              ? $"\"start_state_mes_unit_name\" = \'{model.StartMesUnits_New}\',  " 
                              : null) +
                          (isNewCCount
                              ? $"\"current_state_count\" = \'{newCCount.ToSqLiteString()}\', " 
                              : null) +
                          $"\"changed_at_date\" = \'{now}\', \"changed_by\" = {user} " +
                          $"where \"LKKSN\" = \'{model.Lkksn}\'; " +
                          "insert into \"Reactives_Changes\" (\"LKKSN\", \"created_at_date\", \"created_by\", \"record_comment\", \"other_changes\") " +
                          $"values (\'{model.Lkksn}\', \'{now}\', {user}, \'Адм.: изменение объёмов поставки\', \'" +
                          (isNewPCount 
                              ? "[Объём поставки в единицах производителя] " +
                                $"изменён с [{model.ProducerCount_Old}] на [{model.ProducerCount_New}]" 
                              : null) +
                          (isNewPUnits 
                              ? "\\n[Единица измерения поставщика] " +
                                $"изменена с [{model.ProducerMesUnits_Old}] на [{model.ProducerMesUnits_New}]" 
                              : null) +
                          (isNewSCount 
                              ? "\\n[Объём поставки в учётных единицах] " +
                                $"изменён с [{model.StartCount_Old}] на [{model.StartCount_New}]" 
                              : null) +
                          (isNewSUnits 
                              ? "\\n[Учётная единица] " +
                                $"изменена с [{model.StartMesUnits_Old}] на [{model.StartMesUnits_New}]" 
                              : null) +
                          (isNewCCount 
                              ? "\\n[Текущие остатки] " +
                                $"изменены с [{model.CurrentCount_Old}] на [{model.CurrentCount_New}]" 
                              : null) +
                          (string.IsNullOrWhiteSpace(model.EditingComment) 
                              ? null
                              : $"\\n\\nЗаметка изменяющего: {model.EditingComment}") + 
                          "\'); commit;";

            var result = _connector.WriteData(request, out var error1);

            return (result < 2)
                ? this.Error("Ошибка базы данных", "При записи данных в базу произошла ошибка. " + error1)
                : RedirectToAction("EditCounts", new {lkksn = model.Lkksn});
        }

        /// <summary> Переход к исправлению конкретной строки в истории реактива </summary>
        /// <param name="id">номер строки в истории реактива</param>
        /// <param name="lkksn">номер серии ЛКК</param>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult EditCountRow(int? id, string lkksn)
        {
            if (id == null || string.IsNullOrWhiteSpace(lkksn))
                return this.Error("Системная ошибка", 
                    "В запросе отсутствует либо идентификатор строки, либо серия ЛКК. " +
                    "Обратитесь к разработчику.");

            var data = _connector.ReadData
            (
                "select c.\"record_comment\" as \"comment\", c.\"created_at_date\" as \"date\", " +
                "c.\"count_difference\" as \"diff\", c.\"count_new_value\" as \"new_value\", " +
                "a.\"start_state_mes_unit_name\" as \"mes_units\" " +
                "from \"Reactives_Changes\" c " +
                "inner join \"Reactives_Arrivals\" a on a.\"LKKSN\" = c.\"LKKSN\" " +
                $"where c.\"Id\" = {id} and c.\"LKKSN\" = \'{lkksn}\';",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Ошибка базы данных", "При загрузке денных произошла ошибка. " + error);

            var date = data.Rows[0].GetDate("date");
            var delta = data.Rows[0].GetDecimal("diff");
            var value = data.Rows[0].GetDecimal("new_value");

            if (delta == null || value == null)
                return this.Error("", 
                    "Вы пытаетесь изменить расходование для строки истории, в которой " +
                    "изначально не проводилось расходование реактива. Выберите другую строку для редактирования истории.");

            return View(new ReactivesEditCountRowModel
            {
                Id = id,
                Lkksn = lkksn,
                RecordComment = data.Rows[0].GetValue("comment"),
                MesUnits = data.Rows[0].GetValue("mes_units"),
                RecordDate_Old = date,
                RecordDate_New = date,
                Delta_Old = delta,
                Delta_New = delta.ToString(),
                Count_Old = value,
                Count_New = value.ToString(),
                EditingComment = null
            });
        }

        /// <summary> Подтверждение изменения строки в истории реактива </summary>
        /// <param name="model">Модель изменения строки в истории реактива</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult EditCountRow(ReactivesEditCountRowModel model)
        {
            // валидация
            if (model == null)
                return this.Error("Системная ошибка", 
                    "В запросе отсутствует необходимая модель данных. Обратитесь к разработчику");

            if (model.RecordDate_New == null)
                ModelState.AddModelError("", "Новая дата записи не может быть пустой");

            var isNewDate = model.RecordDate_Old?.ToString("yyyy-MM-dd-HH-mm-ss") !=
                            model.RecordDate_New?.ToString("yyyy-MM-dd-HH-mm-ss");

            var newDelta = model.Delta_New.ToDecimal();
            var isNewDelta = newDelta != model.Delta_Old;
            if (newDelta == null)
                ModelState.AddModelError("", 
                    "Новое значение поля \"изменение\" имеет не верный формат. " +
                    "Ожидается целое или дробное число, положительное для поставок и отрицательное для расходования.");

            var newValue = model.Count_New.ToDecimal() ?? -1;
            var isNewValue = newValue != model.Count_Old;
            if (newValue < 0)
                ModelState.AddModelError("", 
                    "Новое значение поля \"остаток\" имеет не верный формат. " +
                    "Ожидается целое или дробное неотрицательное число.");

            if (!isNewDate && !isNewDelta && !isNewValue)
                ModelState.AddModelError("","Вы не ввели новые значения. Изменения не требуются.");

            if (!ModelState.IsValid) return View(model);

            // изменение данных
            var isFirst = true;
            string Coma()
            {
                if (isFirst) isFirst = false;
                else return ", ";
                return null;
            }

            var request = "begin; " +
                          "update \"Reactives_Changes\" set " +
                          (isNewDate ? $"\"created_at_date\" = \'{model.RecordDate_New?.ToSqLiteString()}\'" : null) +
                          (isNewDelta ? Coma() + $"\"count_difference\" = \'{newDelta?.ToSqLiteString()}\' " : null) +
                          (isNewValue ? Coma() + $"\"count_new_value\" = \'{newValue.ToSqLiteString()}\' " : null) +
                          $"where \"Id\" = {model.Id} and \"LKKSN\" = \'{model.Lkksn}\'; " +
                          "insert into \"Reactives_Changes\" (\"LKKSN\", \"created_at_date\",  " +
                          "\"created_by\", \"record_comment\", \"other_changes\") " +
                          $"values  (\'{model.Lkksn}\', \'{DateTime.Now.ToSqLiteString()}\', {this.GetUserId()}, " +
                          "\'Адм.: изменение истории\', \'" +
                          $"Изменение строки истории № {model.Id} для {model.Lkksn}:" +
                          (isNewDate ? $"\\n[Дата строки] изменена с [{model.RecordDate_Old?.ToSqLiteString()}] на [{model.RecordDate_New?.ToSqLiteString()}]" : null) +
                          (isNewDelta ? $"\\n[Объём расходования] изменён с [{model.Delta_Old}] на [{model.Delta_New}] {model.MesUnits}" : null) +
                          (isNewValue ? $"\\n[Остаток] изменён с [{model.Count_Old}] на [{model.Count_New}] {model.MesUnits}" : null) +
                          (string.IsNullOrWhiteSpace(model.EditingComment) ? null : $"\\n\\nЗаметка изменяющего: {model.EditingComment}") +
                          "\'); commit;";

            var result = _connector.WriteData(request, out var error);
            return result < 2
                ? this.Error("Ошибка базы данных", "При записи данных произошла ошибка. " + error)
                : RedirectToAction("EditCounts", new {lkksn = model.Lkksn, id = model.Id});
        }
        
        /// <summary> Получает из базы данных список спецификаций </summary>
        /// <returns>Перечень спецификаций из базы данных</returns>
        /// <exception cref="Exception"></exception>
        private IEnumerable<SelectListItem> GetSpecifications()
        {
            if (!_connector.TryReadData($"select \"Id\", \"name\" from \"Reactives_Specifications\" order by \"Id\"",
                out var data, out var error))
                throw new Exception("При попытке загрузить данные о спецификациях произошла ошибка. " + error);
            return data?.Rows.Cast<DataRow>()
                .Select(x => new SelectListItem(x.GetValue("name"), x.GetValue("Id"), false));
        }

        /// <summary> Получает из базы данных список производителей </summary>
        /// <returns>Перечень производителей из базы данных</returns>
        /// <exception cref="Exception"></exception>
        private IEnumerable<SelectListItem> GetProducers()
        {
            if (!_connector.TryReadData("select \"Id\", \"name\" from \"Reactives_Producers\" order by \"name\"",
                out var data, out var error))
                throw new Exception("При попытке загрузить данные о производителях произошла ошибка. " + error);
            return data?.Rows.Cast<DataRow>()
                .Select(x => new SelectListItem(x.GetValue("name"), x.GetValue("Id"), false));
        }

        /// <summary> Получает из базы данных список поставщиков </summary>
        /// <returns>Перечень поставщиков из базы данных</returns>
        /// <exception cref="Exception"></exception>
        private IEnumerable<SelectListItem> GetProviders()
        {
            if (!_connector.TryReadData("select \"Id\", \"name\" from \"Reactives_Providers\" order by \"name\"",
                out var data, out var error))
                throw new Exception("При попытке загрузить данные о поставщиках произошла ошибка. " + error);
            return data?.Rows.Cast<DataRow>()
                .Select(x => new SelectListItem(x.GetValue("name"), x.GetValue("Id"), false));
        }

        /// <summary> Получает из базы данных список атрибутов поставки </summary>
        /// <returns>Словарь атрибутов базы данных: код=название</returns>
        /// <exception cref="Exception"></exception>
        private Dictionary<char, string> GetAttributes()
        {
            if (!_connector.TryReadData("select \"code\", \"name\" from \"Reactives_Attributes\" order by \"Id\"",
                out var data, out var error))
                throw new Exception("При попытке загрузить список атрибутов поставки произошла ошибка. " + error);

            return data?.Rows.Cast<DataRow>()
                .ToDictionary(x => x.GetValue("code")[0], y => y.GetValue("name"));
        }

        /// <summary> Получает из базы данных список ранее введённых мест хранения </summary>
        /// <returns>Перечень атрибутов из базы данных</returns>
        /// <exception cref="Exception"></exception>
        private IEnumerable<SelectListItem> GetStores()
        {
            if (!_connector.TryReadData(
                "select distinct \"current_state_stored_at\" as \"val\" from \"Reactives_Arrivals\" " +
                "where \"current_state_stored_at\" is not null and \"current_state_stored_at\" <> '';",
                out var data, out var error))
                throw new Exception("При загрузке данных о местах хранения из базы данных произошла ошибка. " + error);
            return data?.Rows.Cast<DataRow>()
                .Select(x => new SelectListItem(x.GetValue("val"), x.GetValue("val"), false));
        }

        /// <summary> Получает из базы данных список единиц измерения </summary>
        /// <returns>Перечень атрибутов из базы данных</returns>
        /// <exception cref="Exception"></exception>
        private IEnumerable<SelectListItem> GetMesUnits()
        {
            if (!_connector.TryReadData(
                "select \"name\" from \"Reactives_Mes_Units\" order by \"position\", \"name\"",
                out var data, out var error))
                throw new Exception("При загрузке данных о единицах измерения из базы данных произошла ошибка. " + error);
            return data?.Rows.Cast<DataRow>()
                .Select(x => new SelectListItem(x.GetValue("name"), x.GetValue("name"), false));
        }

        /// <summary> Проверяет, являются ли набор элементов в номере серии ЛКК уникальными </summary>
        /// <param name="specification">номер спецификации</param>
        /// <param name="delivery">номер поставки в году</param>
        /// <param name="year">год</param>
        /// <returns>true = уникальный, false = не уникальный</returns>
        private bool IsLkksnUnique(int specification, int delivery, int year)
        {
            var result = _connector.GetValue("select count(\"LKKSN\") from \"Reactives_Arrivals\" " +
                                             $"where \"LKKSN_specification_id\" = {specification} and \"LKKSN_delivery_num\" = {delivery} and \"LKKSN_date\" like '{year}-%'", 
                out var error);
            if (result == null) throw new Exception(error);

            return result == "0";
        }
    }
}
