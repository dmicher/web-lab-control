﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.ReactiveAttributes;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;

namespace WebApplication.Controllers
{
    /// <summary> Управление атрибутами к реактивам </summary>
    [Authorize]
    public class ReactivesAttributesController : Controller
    {
        /// <summary> Сервис свяхи с базой данных </summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструкто собирает нужные сервисы </summary>
        /// <param name="connector">Сервис связи с базой данных</param>
        public ReactivesAttributesController(IDbConnector connector)
            => _connector = connector;

        /// <summary>Просмотр атрибутов</summary>
        public IActionResult Index()
        {
            if (!_connector.TryReadData("select \"code\", \"name\" from \"Reactives_Attributes\" order by \"code\"",
                out var data, out var error))
                return this.Error("Ошибка базы данных",
                    "При загрузке атрибутов произошла ошибка. " + error);

            return View(data?.Rows.Cast<DataRow>().Select(x => new AttributeModel
                {Code = x.GetValue("code")[0], Name = x.GetValue("name")}));
        }

        /// <summary> Вход в инструмент для добавления атрибута </summary>
        [HttpGet]
        public IActionResult Add()
        {
            if (!_connector.TryReadData("select \"code\" from \"Reactives_Attributes\";", out var data,
                out var error))
                return this.Error("Ошибка базы данных",
                    "Атрибуты, добавить. При загрузке данных из базы произошла ошибка. " + error);

            if (data == null || data.Rows.Count < 1) return View();

            var existingList = data.Rows.Cast<DataRow>().Select(x => x.GetValue("code")[0]).ToList();
            existingList.Sort();

            var model = new AttributeAddModel {Existing = new string(existingList.ToArray())};

            return View(model);
        }

        /// <summary> Подтверждение добавления модели </summary>
        /// <param name="model">Модель атрибута</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Add(AttributeAddModel model)
        {
            if (model.Existing?.Any(x => x == model.ToAdd.Code) ?? false)
                ModelState.AddModelError("", "Такой код уже существует в базе данных, выберите другой");
            if (!new Regex(@"[A-Za-z\d]{1}").IsMatch(model.ToAdd.Code.ToString()))
                ModelState.AddModelError("", "Кодом может быть только заглавная или строчка буквы латиницы, либо цифры.");
            if (!ModelState.IsValid) return View(model);

            var result =
                _connector.WriteData("insert into \"Reactives_Attributes\" (\"code\", \"name\") " +
                                     $"values ('{model.ToAdd.Code}', '{model.ToAdd.Name.Trim()}')",
                    out var error2);

            if (result > 0) return RedirectToAction("Index", "ReactivesAttributes");

            if (error2.Contains(Constants.DataBaseUniqueConstraintError))
                error2 = "В базе данных уже существует единица измерения с таким названием и/или сокращением." +
                        " Эти значения не могут повторяться.";

            return this.Error("Ошибка базы данных", "При добавлении атрибута произошла ошибка. " + error2);

        }

        /// <summary> Вход в инструмент изменения атрибута (только администратор)</summary>
        /// <param name="code">Код атрибута</param>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Edit(char? code)
        {
            if (code == null)
                return this.Error("Системная ошибка",
                    "При загрузке инструмента изменения атрибута произошла ошибка: " +
                    "код атрибута не получен. Обратитесь к разработчику.");

            var name = _connector.GetValue(
                "select \"name\" from \"Reactives_Attributes\" " +
                $"where \"code\" = \'{code}\'", out var error);

            if (string.IsNullOrWhiteSpace(name))
                return this.Error("Ошибка базы данных",
                    "При загрузке инструмента изменения атрибута произошла ошибка. " + error +
                    "Возможно, атрибут с указанным кодом отсутствует в базе данных. Обратитесь к разработчику.");

            return View(new AttributeEditModel
            {
                EditFor = new AttributeModel
                {
                    Code = (char) code,
                    Name = name
                },
                NewName = name
            });
        }

        /// <summary>Подтверждение изменения атрибута (только администратор)</summary>
        /// <param name="model">Модель для изменения атрибута</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Edit(AttributeEditModel model)
        {
            if (model.NewName == model.EditFor.Name)
                ModelState.AddModelError("","Новое описание атрибута совпадает со старым описанием");
            if (!ModelState.IsValid) return View(model);

            var result = _connector.WriteData
            (
                $"update \"Reactives_Attributes\" set \"name\" = '{model.NewName.Trim()}' " +
                $"where \"code\" = \'{model.EditFor.Code}\'",
                out var error
            );

            if (result > 0) return RedirectToAction("Index", "ReactivesAttributes");

            if (error.Contains(Constants.DataBaseUniqueConstraintError))
                error = "В базе данных уже существует единица измерения с введёнными новыми названием и/или сокращением. " +
                        "Эти значения не должны повторяться.";

            return this.Error("Ошибка базы данных", "При изменении атрибута произошла ошибка: " + error);
        }

        /// <summary> Вход в инструмент по удалению атрибута (только администратор)</summary>
        /// <param name="code">Код атрибута</param>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Delete(char? code)
        {

            if (code == null)
                return this.Error("Системная ошибка",
                    "При загрузке инструмента удаления атрибута произошла ошибка: " +
                    "код атрибута не получен. Обратитесь к разработчику.");

            var name = _connector.GetValue(
                "select \"name\" from \"Reactives_Attributes\" " +
                $"where \"code\" = \'{code}\'", out var error);
            if (string.IsNullOrWhiteSpace(name))
                return this.Error("Ошибка базы данных",
                    "При загрузке инструмента удаления атрибута произошла ошибка. " + error +
                    "Возможно, атрибут с указанным кодом отсутствует в базе данных. Обратитесь к разработчику.");

            return View(new AttributeModel
            {
                Code = (char) code,
                Name = name
            });
        }

        /// <summary>Подтверждение удаления атрибута (только администратор)</summary>
        /// <param name="model">Модель атрибута</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Delete(AttributeModel model)
        {
            if (!_connector.TryReadData(
                "select \"LKKSN\", \"current_state_attribute_string\" as \"attributes\" from \"Reactives_Arrivals\" " +
                $"where \"current_state_attribute_string\" like \'%{model.Code}%\';",
                out var data, out var error))
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить данные о сериях ЛКК, в которых присутствует удаляемый атрибут произошла ошибка. " +
                    error);

            var request = new StringBuilder("begin; ");

            if (data != null)
                foreach (DataRow row in data.Rows)
                {
                    var newAttrs = row.GetValue("attributes").Replace(model.Code.ToString(), null);
                    var lkksn = row.GetValue("LKKSN");

                    request.Append
                    (
                        $"update \"Reactives_Arrivals\" set \"current_state_attribute_string\" = \'{newAttrs}\'; " +
                        "insert into \"Reactives_Changes\" (\"LKKSN\", \"created_at_date\", \"created_by\", \"record_comment\", \"attribute_string\") " +
                        $"values (\'{lkksn}\', \'{DateTime.Now.ToSqLiteString()}\', \'{this.GetUserId()}\', \'Атрибут {model.Code} удалён из базы данных\', \'{newAttrs}\'); "
                    );
                }

            request.Append($"delete from \"Reactives_Attributes\" where \"code\" = \'{model.Code}\'; commit;");

            var result = _connector.WriteData(request.ToString(), out error);
            if (result < 1)
                return this.Error("Ошибка базы данных",
                    "При попытке удаления атрибута произошла ошибка. " + error + " Атрибут не удалён.");

            return RedirectToAction("Index", "ReactivesAttributes");
        }
    }
}
