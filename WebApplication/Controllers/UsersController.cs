﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication.Models.Home;
using WebApplication.Models.Users;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;

namespace WebApplication.Controllers
{
    /// <summary> Управление пользователями </summary>
    [Authorize]
    public class UsersController : Controller
    {
        /// <summary> Соединитель с базой данных </summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструктор запрашивает сервисы </summary>
        /// <param name="connector">Соединитель с базой данных</param>
        public UsersController(IDbConnector connector)
            => _connector = connector;

        /// <summary> Просмотр всех пользователей </summary>
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Index()
        {
            var data = _connector.ReadData
            (
                "select u.\"Id\", u.\"login\", u.\"name\" as \"full_name\", g.\"name\" as \"group\" " +
                "from \"Users\" u " +
                "inner join \"Users_Groups\" g on g.\"Id\" = u.\"group_id\" " +
                "order by g.\"Id\", u.\"name\"",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Пользователей не обнаружено",
                    "В ходе поиска данных по пользователях не обнаружено ни одной записи. " + error +
                    " Обратитесь к разработчику.");

            var model = data.Rows.Cast<DataRow>()
                .Select(x => new UserInfoModel
                {
                    Id = int.TryParse(x["Id"]?.ToString()?.Trim(), out var i) ? i : -1,
                    Login = x["login"]?.ToString()?.Trim(),
                    Name = x["full_name"]?.ToString()?.Trim(),
                    Group = x["group"]?.ToString()?.Trim()
                });

            return View(model);
        }

        /// <summary> Первый заход на добавление пользователя </summary>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Add()
        {
            var list = GetGroups(out var error);

            if (list == null)
                return this.Error("Список групп не получен",
                    "При попытке загрузить инструмент добавления пользователя " +
                    "из базы данных не был получен список групп. " + error +
                    " Обратитесь к разработчику");

            return View(new UserAddModel {Groups = list});
        }

        /// <summary> Получает из базы данных список групп </summary>
        /// <returns>Номера и названия групп в перечислении для выпадающего списка</returns>
        private IEnumerable<SelectListItem> GetGroups(out string error)
        {
            var data = _connector.ReadData
            (
                "select \"Id\", \"name\" from \"Users_Groups\" " +
                "where \"name\" <> \"Создатель\" order by \"Id\";",
                out error
            );

            if (data == null || data.Rows.Count < 1)
                return null;

            return data.Rows.Cast<DataRow>()
                .Select(x => new SelectListItem(
                    x["name"]?.ToString()?.Trim(),
                    x["Id"]?.ToString()?.Trim(),
                    false));
        }

        /// <summary> Добавление пользователя - отправка модели </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Add(UserAddModel model)
        {
            string error;

            // заново подгружает список групп - он не возвращается из формы
            if (model.Groups == null || !model.Groups.Any())
            {
                var list = GetGroups(out error);

                if (list == null)
                    return this.Error("Список групп не получен",
                        "При попытке загрузить инструмент добавления пользователя " +
                        "из базы данных не был получен список групп. " + error +
                        " Обратитесь к разработчику");

                model.Groups = list;
            }

            // валидация данных
            if (!ModelState.IsValid) return View(model);

            if (model.SelectedGroup < 0)
            {
                ModelState.AddModelError("", "Группа пользователя не выбрана");
                return View(model);
            }

            var result = _connector.WriteData
            (
                "insert into \"Users\" (\"login\", \"name\", \"password\", \"group_id\") values " +
                $"(\'{model.Login.Trim()}\', \'{model.Name.Trim()}\', \'{model.Password.Trim()}\', {model.SelectedGroup});",
                out error
            );

            if (result < 1)
            {
                if (!error.Contains(Constants.DataBaseUniqueConstraintError))
                    return this.Error("Ошибка базы данных",
                        "При попытке добавить данные о новом пользователе в базу произошла ошибка. " + error +
                        " Обратитесь к разработчику.");

                ModelState.AddModelError("",
                    "Пользователей с указанным именем для входа уже существует. Выберите другой логин.");
                return View(model);

            }

            return RedirectToAction("ChangeResult", "Users", new UserChangeResultModel
            {
                Title = "Пользователь добавлен",
                Header = "Пользователь добавлен",
                Description = $"Пользователь \'{model.Name}\' успешно добавлен."
            });
        }

        /// <summary> (приватный) Возвращает результат изменения данных о пользователе</summary>
        /// <param name="model">Титул страницы</param>
        public IActionResult ChangeResult(UserChangeResultModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.Title)
                              || string.IsNullOrWhiteSpace(model.Header)
                              || string.IsNullOrWhiteSpace(model.Description))
                return this.Error("Модель данных не заполнена",
                    "При вызове метода \'Показать результаты изменения пользователя\' данные не заполнены." +
                    "Обратитесь к разработчику");

            return View(model);
        }

        /// <summary> Первый заход на изменение пользователя</summary>
        /// <param name="id">Идентификатор пользователя</param>
        [HttpGet]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
                return this.Error("Пользователь не выбран",
                    "При попытке загрузить инструмент изменения пользователя произошла " +
                    "ошибка из-за того, что номер пользователя, подлежащего изменению, " +
                    "не указан. Обратитесь к разработчику.");

            if (id == 1)
                return this.Error("Изменение корневого пользователя",
                    "При попытке загрузить инструмент изменения пользователя произошла " +
                    "ошибка из-за системного ограничения: изменение данных корневого " +
                    "пользователя является недопустимой операцией");

            var data = _connector.ReadData
            (
                "select u.\"login\", u.\"name\" as \"full_name\", g.\"name\" as \"group\" " +
                "from \"Users\" u " +
                "inner join \"Users_Groups\" g on g.\"Id\" = u.\"group_id\" " +
                $"where u.\"Id\" = {id};",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить инструмент изменения пользователя произошла " +
                    "ошибка базы данных. " + error + " Данные о пользователе не получены.");

            var name = data.Rows[0]["full_name"]?.ToString()?.Trim();
            var login = data.Rows[0]["login"]?.ToString()?.Trim();
            var group = data.Rows[0]["group"]?.ToString()?.Trim();

            var model = new UserEditModel
            {
                Id = (int) id,
                OldName = name,
                NewName = name,
                OldLogin = login,
                NewLogin = login,
                OldGroup = group
            };

            var groupList = GetGroups(out error);
            if (groupList == null)
                return this.Error("Ошибка базы данных",
                    "При попытке загрузить инструмент изменения пользователя произошла " +
                    "ошибка базы данных. " + error + " Данные о группах не получены.");

            var selectListItems = groupList as SelectListItem[] ?? groupList.ToArray();
            foreach (var item in selectListItems)
            {
                if (item.Text != group) continue;
                item.Selected = true;
                model.OldGroupNumber = int.TryParse(item.Value, out var i) ? i : -1;
                break;
            }

            model.Groups = selectListItems;

            return View(model);
        }

        /// <summary> Изменение пользователя - отправка модели </summary>
        /// <param name="model">Модель изменения пользователя</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Администратор, Создатель")]
        public IActionResult Edit(UserEditModel model)
        {
            string error;
            // валидация данных
            if (model.Groups == null || !model.Groups.Any())
            {
                var groups = GetGroups(out error).ToArray();
                if (!groups.Any())
                    return this.Error("Ошибка базы данных",
                        "При повторной загрузке инструмента изменения пользователя произошла " +
                        "ошибка. " + error + " Данные о группах пользователей не загружены.");

                model.Groups = groups;
            }

            if (!ModelState.IsValid) return View(model);

            if (model.Id == 1)
                return this.Error("Изменение корневого пользователя",
                    "При попытке загрузить инструмент изменения пользователя произошла " +
                    "ошибка из-за системного ограничения: изменение данных корневого " +
                    "пользователя является недопустимой операцией.");

            if (model.OldGroupNumber == model.NewGroupNumber
                && model.OldLogin == model.NewLogin
                && model.OldName == model.NewName
                && string.IsNullOrWhiteSpace(model.NewPassword))
            {
                ModelState.AddModelError("", "Вы не указали никаких новых данных. Данные не изменены.");
                return View(model);
            }

            // изменение данных в базе
            var builder = new StringBuilder("update \"Users\" set ");
            var isFirst = true;

            string AndWhere()
            {
                if (!isFirst) return ", ";
                isFirst = false;
                return " ";
            }

            var resultBuilder = new StringBuilder();
            string add;

            if (!string.IsNullOrWhiteSpace(model.NewLogin) && model.OldLogin != model.NewLogin)
            {
                add = AndWhere();
                builder.Append(add + $"\"login\" = '{model.NewLogin}'");
                resultBuilder.Append(add + "логин = " + model.NewLogin);
            }

            if (!string.IsNullOrWhiteSpace(model.NewName) && model.OldName != model.NewName)
            {
                add = AndWhere();
                builder.Append(add + $"\"name\" = '{model.NewLogin}'");
                resultBuilder.Append(add + "имя = " + model.NewLogin);
            }

            if (!string.IsNullOrWhiteSpace(model.NewPassword))
            {
                add = AndWhere();
                builder.Append(add + $"\"password\" = '{model.NewPassword}'");
                resultBuilder.Append(add + "новый пароль");
            }

            if (model.OldGroupNumber != model.NewGroupNumber)
            {
                add = AndWhere();
                builder.Append(add + $"\"group_id\" = '{model.NewGroupNumber}'");
                resultBuilder.Append(add + "новая группа");
            }

            builder.Append($" where \"id\" = {model.Id}");

            var result = _connector.WriteData(builder.ToString(), out error);
            if (result < 1)
            {

                if (error.Contains(Constants.DataBaseUniqueConstraintError))
                {
                    ModelState.AddModelError("",
                        "Пользователей с указанным именем для входа уже существует. Выберите другой логин.");
                    return View(model);
                }

                return this.Error("Ошибка базы данных",
                    $"При попытке обновить данные пользователя № {model.Id} ({model.OldName}) произошла " +
                    $"ошибка. {error} Обратитесь к разработчику");
            }

            return RedirectToAction("ChangeResult", "Users", new UserChangeResultModel
            {
                Title = "Успешное изменение",
                Header = "Пользователь изменён",
                Description = $"Данные о пользователе [{model.Id}: {model.OldName}] были успешно изменены. " +
                              $"Сводка: {resultBuilder}"
            });
        }

        /// <summary> Просмотр своего профиля </summary>
        public IActionResult MyProfile()
        {
            var id = this.GetUserId();
            if (id < 0)
                return this.Error("Ошибка аутентификации",
                    "Система аутентификации пользователя не обнаружила приемлемого номера пользователя " +
                    "в сохранённых данных. Просмотр профиля не возможен. Ваша сессия работы на сайте " +
                    "будет закрыта.", true);

            var data = _connector.ReadData
            (
                "select u.\"id\", u.\"login\", u.\"name\" as \"full_name\", g.\"name\" as \"group\" " +
                "from \"Users\" u inner join \"Users_Groups\" g on g.\"Id\" = u.\"group_id\" " +
                $"where u.\"id\" = {id};",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Пользователь не обнаружен",
                    "Пользователь с вашими идентификационными данными не обнаружен в базе данных. " +
                    error + "Ваша сессия работы на сайте будет закрыта.",
                    true);

            var model = new UserInfoModel
            {
                Id = id,
                Login = data.Rows[0]["login"]?.ToString()?.Trim(),
                Name = data.Rows[0]["full_name"]?.ToString()?.Trim(),
                Group = data.Rows[0]["group"]?.ToString()?.Trim(),
            };

            return View(model);
        }

        /// <summary> Первый заход в изменение своего профиля </summary>
        [HttpGet]
        public IActionResult ChangeMyPassword() => View();

        /// <summary> Внесение изменений в своём профиле </summary>
        /// <param name="model"> Модель изменения пароля </param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult ChangeMyPassword(ChangeMyPasswordModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var id = this.GetUserId();
            if (id < 0)
                return this.Error("Ошибка аутентификации",
                    "Система аутентификации пользователя не обнаружила приемлемого номера пользователя " +
                    "в сохранённых данных. Просмотр профиля не возможен. Ваша сессия работы на сайте " +
                    "будет закрыта.", true);

            var result = _connector.WriteData
            (
                $"update \"Users\" set \"password\" = \'{model.NewPassword}\' where \"Id\" = {id};",
                out var error
            );

            if (result < 0)
                return this.Error("Ошибка базы данных",
                    "При смене Вашего пароля произошла ошибка базы данных. " + error +
                    "Данные не были изменены. Обратитесь к разработчику");

            return RedirectToAction("ChangeResult", "Users", new UserChangeResultModel
            {
                Title = "Успешное изменение",
                Header = "Ваш пароль был успешно изменён",
                Description = "Ваш пароль для входа на сайт был успешно изменён. Текущая сессия Вашей работы " +
                              "с сайтом не будет прервана. В следующую сессию работы Вам необходимо будет вводить " +
                              "новый пароль."
            });
        }

        /// <summary> Первый заход на удаление пользователя </summary>
        /// <param name="id">Номер пользователя</param>
        [HttpGet]
        [Authorize(Roles = "Создатель")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
                return this.Error("Пользователь не выбран",
                    "При попытке загрузить инструмент удаления пользователя произошла " +
                    "ошибка из-за того, что удаляемый пользователь не был указан. Данные" +
                    " не изменены. Обратитесь к разработчику.");

            if (id == 1)
                return this.Error("Действие запрещено",
                    "При попытке загрузить инструмент удаления пользователя произошла " +
                    "ошибка из-за системных ограничений: удаление корневого пользователя " +
                    "запрещено. Данные не изменены.");

            var data = _connector.ReadData
            (
                "select u.\"name\" as \"full_name\", g.\"name\" as \"group\" " +
                "from \"Users\" u inner join \"Users_Groups\" g on g.\"Id\" = u.\"group_id\" " +
                $"where u.\"Id\" = {id};",
                out var error
            );

            if (data == null || data.Rows.Count < 1)
                return this.Error("Пользователь не обнаружен",
                    "Выбранный для удаления пользователь не обнаружен в базе данных. " + error);

            var model = new UserDeleteModel
            {
                UserId = (int) id,
                UserName = data.Rows[0]["full_name"]?.ToString()?.Trim(),
                UserGroup = data.Rows[0]["group"]?.ToString()?.Trim()
            };

            return View(model);
        }

        /// <summary> Удаляет пользователя </summary>
        /// <param name="model">Номер пользователя</param>
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        [Authorize(Roles = "Создатель")]
        public IActionResult Delete(UserDeleteModel model)
        {
            if (!ModelState.IsValid) return View(model);
            if (model.UserId == 1)
                return this.Error("Запрещённая операция",
                    "При попытке удаления пользователя произошла ошибка, вызванная " +
                    "системными ограничениями: удаление корневого пользователя запрещено.");

            var check = int.TryParse(_connector.GetValue
            (
                $"select count(\"Id\") from \"Users\" where \"Id\" = 1 and \"password\" = \'{model.RootPassword}\';",
                out var error
            ), out var i) && i == 1;

            if (!check)
            {
                ModelState.AddModelError("", "Введённый пароль не верен. " + error);
                return View(model);
            }

            var result = _connector.WriteData
            (
                $"delete from \"Users\" where \"Id\" = {model.UserId};",
                out error
            );

            if (result < 1)
                return this.Error("Пользователь не удалён", "Ни один пользователь не был удалён. " + error);

            return RedirectToAction("ChangeResult", "Users", new UserChangeResultModel
            {
                Title = "Успешное изменение",
                Header = "Пользователь удалён",
                Description = "Пользователь " + model.UserName + " успешно удалён."
            });
        }
    }
}
