﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.ReactivesValidUntil;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;

namespace WebApplication.Controllers
{
    /// <summary> Контроллер формирования статистики по срокам годности реактивов </summary>
    [Authorize]
    public class ReactivesValidUntilController : Controller
    {
        /// <summary> Сервис связи с базой данных</summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструктор запрашивает нужные сервисы </summary>
        /// <param name="connector">Сервис базы данных</param>
        public ReactivesValidUntilController(IDbConnector connector)
            => _connector = connector;

        /// <summary> Просмотр и фильтрация</summary>
        /// <param name="model">Модель данных</param>
        public IActionResult Index(ReactivesValidUntilIndexFilterModel model)
        {
            if (model == null) return View(new ReactivesValidUntilIndexFilterModel());

            if (model.DaysForward == null || model.DaysForward < 0) model.DaysForward = 40;

            var builder = new StringBuilder
            (
                "select " +
                    "s.\"Id\" as \"spec_id\", " +
                    "s.\"name\" as \"spec_name\", " +
                    "a.\"current_state_count\" as \"count\", " +
                    "a.\"start_state_mes_unit_name\" as \"mes_units\", " +
                    "a.\"LKKSN\" as \"lkksn\", " +
                    "a.\"current_state_valid_until_date\" as \"valid_until\" " +
                "from \"Reactives_Arrivals\" a " +
                    "left join \"Reactives_Specifications\" s on a.\"LKKSN_specification_id\" = s.\"Id\" " +
                "where (a.\"current_state_valid_forever\" = 0 or a.\"current_state_valid_forever\" is null) " +
                    $" and a.\"current_state_valid_until_date\" < '{DateTime.Today.AddDays((int)model.DaysForward):yyyy-MM-dd}' "
            );

            switch (model.StatesFilter)
            {
                case "no":
                    builder.Append(" and (a.\"current_state_is_written_off\" = 0 " +
                                   "or a.\"current_state_is_written_off\" is null) ");
                    break;
                case "yes": 
                    builder.Append(" and a.\"current_state_is_written_off\" = 1 ");
                    break;
            }

            builder.Append("order by a.\"current_state_valid_until_date\";");

            if (!_connector.TryReadData(builder.ToString(), out var data, out var error))
                return this.Error("Ошибка базы данных", "При загрузке данных произошла ошибка. " + error);

            model.Rows = data.Rows.Cast<DataRow>()
                .Select(x => new ReactivesValidUntilResultRowModel
                {
                    SpecificationId = x.GetInt("spec_id"),
                    SpecificationName = x.GetValue("spec_name"),
                    Count = x.GetDecimal("count"),
                    MesUnits = x.GetValue("mes_units"),
                    Lkksn = x.GetValue("lkksn"),
                    ValidUntil = x.GetDate("valid_until")
                });

            return View(model);
        }
    }
}
