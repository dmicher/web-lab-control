﻿using System.Data;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication.Models.ReactivesMoving;
using WebApplication.Tools.DbConnector;
using WebApplication.Tools.Project;

namespace WebApplication.Controllers
{
    /// <summary> Контроллер для статистики по движению реактивов </summary>
    [Authorize]
    public class ReactivesMovingController : Controller
    {
        /// <summary> Соединитель с базой данных </summary>
        private readonly IDbConnector _connector;

        /// <summary> Конструктор собирает сервисы </summary>
        /// <param name="connector">Сервмс вязи с базой данных</param>
        public ReactivesMovingController(IDbConnector connector)
            => _connector = connector;

        /// <summary> Первая фильтрация </summary>
        /// <param name="model">Модель фитров и результатов</param>
        public IActionResult Index(MovingIndexFilterModel model)
        {
            // перезагружает банные из базы для списков
            model ??= new MovingIndexFilterModel();

            if (!_connector.TryReadData("select \"Id\", \"name\" from \"Reactives_Specifications\";", out var data, out var error))
                return this.Error("Ошибка базы данных", 
                    "Движение реагентов, просмотр. При загрузке данных о спецификациях произошла ошибка. " + error);
            model.Specifications = data?.Rows.Cast<DataRow>()
                .Select(x => new SelectListItem(x.GetValue("name"), x.GetValue("Id"), false));

            if (!_connector.TryReadData("select \"code\", \"name\" from \"Reactives_Attributes\";", out data, out error))
                return this.Error("Ошибка базы данных", 
                    "Движение реагентов, просмотр. При загрузке данных об атрибутах произошла ошибка. " + error);
            model.Attributes = data?.Rows.Cast<DataRow>()
                .ToDictionary(x => x.GetValue("code")[0], x => x.GetValue("name"));
            model.FilterAttributesOn ??= model.Attributes?.ToDictionary(x => x.Key, x => false);
            model.FilterAttributesOff ??= model.Attributes?.ToDictionary(x => x.Key, x => false);

            if (!_connector.TryReadData("select \"Id\", \"name\" from \"Users\"", out data, out error))
                return this.Error("Ошибка базы данных", 
                    "Движение реагентов, просмотр. При загрузке данных о  произошла ошибка. " + error);
            model.Users = data?.Rows.Cast<DataRow>()
                .Select(x => new SelectListItem(x.GetValue("name"), x.GetValue("Id"), false));

            // получает данные по фильтрам от базы
            var builder = new StringBuilder
            (
                "select " +
                    "c.\"LKKSN\" as \"lkksn\", " +
                    "s.\"Id\" as \"spec_Id\", " +
                    "s.\"name\" as \"spec_name\", " +
                    "c.\"count_difference\" as \"difference\", " +
                    "m.\"short\" as \"mes_units\", " +
                    "c.\"record_comment\", " +
                    "u.\"name\" as \"user_name\", " +
                    "c.\"created_at_date\" as \"moving_datetime\" " +
                "from \"Reactives_Changes\" c " +
                    "inner join \"Reactives_Arrivals\" a on a.\"LKKSN\" = c.\"LKKSN\" " +
                    "inner join \"Reactives_Specifications\" s on a.\"LKKSN_specification_id\" = s.\"Id\" " +
                    "inner join \"Reactives_Mes_Units\" m on a.\"start_state_mes_unit_name\" = m.\"name\" " +
                    "left join \"Users\" u on u.\"Id\" = c.\"created_by\" " +
                "where c.\"count_difference\" is not null and c.\"count_difference\" <> 0 " +
                    $"and c.\"created_at_date\" between \'{model.FilterStartDate:yyyy-MM-dd}\' and \'{model.FilterEndDate:yyyy-MM-dd}\' "
            );

            if (!string.IsNullOrWhiteSpace(model.FilterInsertedSpecification))
                builder.Append($" and s.\"upper_name\" like \'%{model.FilterInsertedSpecification}%\' ");
            else if (model.FilterSelectedSpecification != null)
                builder.Append($" and s.\"Id\" = {model.FilterSelectedSpecification} ");

            if (model.FilterAttributesOn != null)
                foreach (var atrOn in model.FilterAttributesOn.Where(x => x.Value))
                    builder.Append($" and a.\"current_state_attribute_string\" like \'%{atrOn.Key}%\' ");

            if (model.FilterAttributesOff != null)
                foreach (var atrOff in model.FilterAttributesOff.Where(x => x.Value))
                    builder.Append(" and (a.\"current_state_attribute_string\" is null " +
                                   $"or a.\"current_state_attribute_string\" not like \'%{atrOff.Key}%\') ");

            if (model.FilterSelectedUser != null)
                builder.Append($" and u.\"Id\" = {model.FilterSelectedUser} ");

            builder.Append("order by c.\"created_at_date\";");

            if (!_connector.TryReadData(builder.ToString(), out data, out error))
                return this.Error("Ошибка базы данных", "Движение реактивов, просмотр. При загрузке данных по запросу произошла ошибка" + error);

            if (data == null || data.Rows.Count < 1) return View(model);

            model.Rows = data.Rows.Cast<DataRow>()
                .Select(x => new MovingIndexResultsRowModel
                {
                    Lkksn = x.GetValue("lkksn"),
                    SpecificationId = x.GetInt("spec_Id"),
                    SpecificationName =  x.GetValue("spec_name"),
                    Difference = x.GetDecimal("difference") ?? 0,
                    MesUnits = x.GetValue("mes_units"),
                    RecordComment = x.GetValue("record_comment"),
                    UserName = x.GetValue("user_name"),
                    MovingDateTime = x.GetDate("moving_datetime")
                });

            return View(model);
        }
    }
}
