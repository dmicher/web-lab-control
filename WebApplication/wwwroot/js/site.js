﻿// показывает или скрывает все элементы страницы по имени класса CSS на основе 
// значения checked в мастер-элементе
function ShowHideByClass(masterCheckboxId, slavesClassName) {
    var master = document.getElementById(masterCheckboxId);
    var slaves = document.getElementsByClassName(slavesClassName);
    var index;
    if (master.checked) {
        for (index = 0; index < slaves.length; ++index) {
            slaves[index].style.display = "block";
        }
    } else {
        for (index = 0; index < slaves.length; ++index) {
            slaves[index].style.display = "none";
        }
    }
}

// Показывает или скрывает элемент с указанным Id на основе значения checked мастер-элемента
function ShowHide(masterCheckboxId, slaveId) {
    var master = document.getElementById(masterCheckboxId);
    var slave = document.getElementById(slaveId);
    if (master.checked) {
        slave.style.display = "block";
    } else {
        slave.style.display = "none";
    }
}

// Делает (не)рабочим элемент с указанным Id на основе значения checked мастер-элемента
// флаг inver инвертирует поведение выбора и активации
function EnableDisable(masterCheckboxId, slaveId, invert) {
    var master = document.getElementById(masterCheckboxId);
    var slave = document.getElementById(slaveId);
    if (invert) {
        slave.disabled = !master.checked;
    } else {
        slave.disabled = master.checked;
    }
}