BEGIN TRANSACTION;
DROP TABLE IF EXISTS "Reactives_Providers";
CREATE TABLE IF NOT EXISTS "Reactives_Providers" (
	"Id"	INTEGER,
	"name"	TEXT,
	"comments"	TEXT,
	"created_at_date"	TEXT,
	"created_by"	INTEGER,
	"changed_at_date"	TEXT,
	"changed_by"	INTEGER,
	PRIMARY KEY("Id" AUTOINCREMENT),
	FOREIGN KEY("created_by") REFERENCES "Users"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	FOREIGN KEY("changed_by") REFERENCES "Users"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT "providers_name_unique_key" UNIQUE("name")
);
DROP TABLE IF EXISTS "Reactives_Producers";
CREATE TABLE IF NOT EXISTS "Reactives_Producers" (
	"Id"	INTEGER,
	"name"	TEXT,
	"comments"	TEXT,
	"created_at_date"	TEXT,
	"created_by"	INTEGER,
	"changed_at_date"	TEXT,
	"changed_by"	INTEGER,
	PRIMARY KEY("Id" AUTOINCREMENT),
	FOREIGN KEY("created_by") REFERENCES "Users"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	FOREIGN KEY("changed_by") REFERENCES "Users"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT "providers_name_unique_key" UNIQUE("name")
);
DROP TABLE IF EXISTS "Reactives_Specifications";
CREATE TABLE IF NOT EXISTS "Reactives_Specifications" (
	"Id"	INTEGER UNIQUE,
	"name"	TEXT,
	"upper_name"	TEXT,
	"created_at_date"	TEXT,
	"created_by"	INTEGER,
	"changed_at_date"	TEXT,
	"changed_by"	INTEGER,
	FOREIGN KEY("changed_by") REFERENCES "Users"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	FOREIGN KEY("created_by") REFERENCES "Users"("Id") ON UPDATE CASCADE ON DELETE SET NULL
);
DROP TABLE IF EXISTS "Users";
CREATE TABLE IF NOT EXISTS "Users" (
	"Id"	INTEGER,
	"login"	TEXT UNIQUE,
	"password"	TEXT,
	"name"	TEXT,
	"group_id"	INTEGER,
	PRIMARY KEY("Id"),
	FOREIGN KEY("group_id") REFERENCES "Users_Groups"("Id") ON UPDATE CASCADE ON DELETE RESTRICT
);
DROP TABLE IF EXISTS "Users_Groups";
CREATE TABLE IF NOT EXISTS "Users_Groups" (
	"Id"	INTEGER,
	"name"	TEXT,
	PRIMARY KEY("Id")
);
DROP TABLE IF EXISTS "Reactives_Arrivals";
CREATE TABLE IF NOT EXISTS "Reactives_Arrivals" (
	"LKKSN"	TEXT,
	"LKKSN_specification_id"	INTEGER,
	"LKKSN_delivery_num"	INTEGER,
	"LKKSN_date"	TEXT,
	"provider_id"	INTEGER,
	"producer_id"	INTEGER,
	"producer_reactive_name"	TEXT,
	"producer_reactive_SN"	TEXT,
	"producer_reactive_count"	NUMERIC,
	"producer_reactive_mes_unit_name"	TEXT,
	"start_state_count"	NUMERIC,
	"start_state_mes_unit_name"	TEXT,
	"current_state_count"	NUMERIC,
	"current_state_is_written_off"	INTEGER,
	"current_state_valid_forever"	INTEGER,
	"current_state_valid_until_date"	TEXT,
	"current_state_stored_at"	TEXT,
	"current_state_comments"	TEXT,
	"current_state_attribute_string"	TEXT,
	"created_at_date"	TEXT,
	"created_by"	INTEGER,
	"changed_at_date"	TEXT,
	"changed_by"	INTEGER,
	PRIMARY KEY("LKKSN"),
	FOREIGN KEY("changed_by") REFERENCES "Users"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	FOREIGN KEY("producer_id") REFERENCES "Reactives_Producers"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	FOREIGN KEY("created_by") REFERENCES "Users"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	FOREIGN KEY("producer_reactive_mes_unit_name") REFERENCES "Reactives_Mes_Units"("name") ON UPDATE CASCADE ON DELETE RESTRICT,
	FOREIGN KEY("provider_id") REFERENCES "Reactives_Providers"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	FOREIGN KEY("LKKSN_specification_id") REFERENCES "Reactives_Specifications"("Id") ON UPDATE CASCADE ON DELETE RESTRICT
);
DROP TABLE IF EXISTS "Reactives_Changes";
CREATE TABLE IF NOT EXISTS "Reactives_Changes" (
	"Id"	INTEGER,
	"LKKSN"	TEXT,
	"count_difference"	NUMERIC,
	"count_new_value"	NUMERIC,
	"is_written_off"	INTEGER,
	"valid_forever"	INTEGER,
	"valid_until_date"	TEXT,
	"stored_at"	TEXT,
	"attribute_string"	TEXT,
	"other_changes"	TEXT,
	"created_at_date"	TEXT,
	"created_by"	INTEGER,
	"record_comment"	TEXT,
	PRIMARY KEY("Id" AUTOINCREMENT),
	FOREIGN KEY("created_by") REFERENCES "Users"("Id") ON UPDATE CASCADE ON DELETE SET NULL,
	FOREIGN KEY("LKKSN") REFERENCES "Reactives_Arrivals"("LKKSN") ON UPDATE CASCADE ON DELETE CASCADE
);
DROP TABLE IF EXISTS "Reactives_Mes_Units";
CREATE TABLE IF NOT EXISTS "Reactives_Mes_Units" (
	"name"	TEXT,
	"short"	TEXT,
	"position"	INTEGER,
	PRIMARY KEY("name")
);
DROP TABLE IF EXISTS "DbRestore";
CREATE TABLE IF NOT EXISTS "DbRestore" (
	"Id"	INTEGER,
	"name"	TEXT,
	"size"	INTEGER,
	"created_at"	TEXT,
	"comment"	TEXT,
	PRIMARY KEY("Id" AUTOINCREMENT)
);
DROP TABLE IF EXISTS "Reactives_Attributes";
CREATE TABLE IF NOT EXISTS "Reactives_Attributes" (
	"code"	TEXT UNIQUE,
	"name"	TEXT,
	PRIMARY KEY("name")
);
INSERT INTO "Users" ("Id","login","password","name","group_id") VALUES (1,'root','goldhome','Технический пользователь',1);
INSERT INTO "Users_Groups" ("Id","name") VALUES (1,'Создатель'),
 (2,'Администратор'),
 (3,'Оператор'),
 (4,'Заблокированный');
INSERT INTO "Reactives_Mes_Units" ("name","short","position") VALUES ('единица','ед',0),
 ('набор','наб',1),
 ('грамм','г',10),
 ('килограмм','кг',11),
 ('миллилитр','мл',20),
 ('литр','л',21);
INSERT INTO "Reactives_Attributes" ("code","name") VALUES ('n','для научной деятельности'),
 ('c','сырьё потребителя'),
 ('p','прекурсор наркотических средств');
DROP INDEX IF EXISTS "Reactives_Arrivals_Stores_index";
CREATE INDEX IF NOT EXISTS "Reactives_Arrivals_Stores_index" ON "Reactives_Arrivals" (
	"current_state_stored_at"	ASC
);
DROP INDEX IF EXISTS "Reactives_Arrivals_LKKSN_date_index";
CREATE INDEX IF NOT EXISTS "Reactives_Arrivals_LKKSN_date_index" ON "Reactives_Arrivals" (
	"LKKSN_date"	ASC
);
DROP INDEX IF EXISTS "Reactives_Attributes_code_index";
CREATE INDEX IF NOT EXISTS "Reactives_Attributes_code_index" ON "Reactives_Attributes" (
	"code"	ASC
);
DROP INDEX IF EXISTS "Reactives_Changes_LKKSN_Id_index";
CREATE INDEX IF NOT EXISTS "Reactives_Changes_LKKSN_Id_index" ON "Reactives_Changes" (
	"LKKSN"	ASC,
	"Id"	ASC
);
DROP INDEX IF EXISTS "Reactives_Mes_Units_position_index";
CREATE INDEX IF NOT EXISTS "Reactives_Mes_Units_position_index" ON "Reactives_Mes_Units" (
	"position"	ASC
);
DROP INDEX IF EXISTS "Reactives_Producers_name_index";
CREATE INDEX IF NOT EXISTS "Reactives_Producers_name_index" ON "Reactives_Producers" (
	"name"	ASC
);
DROP INDEX IF EXISTS "Reactives_Providers_name_index";
CREATE INDEX IF NOT EXISTS "Reactives_Providers_name_index" ON "Reactives_Providers" (
	"name"	ASC
);
DROP INDEX IF EXISTS "Reactives_Specifications_Id_index";
CREATE INDEX IF NOT EXISTS "Reactives_Specifications_Id_index" ON "Reactives_Specifications" (
	"Id"	ASC
);
DROP INDEX IF EXISTS "Reactives_Specifications_name_index";
CREATE INDEX IF NOT EXISTS "Reactives_Specifications_name_index" ON "Reactives_Specifications" (
	"name"	ASC
);
DROP INDEX IF EXISTS "Reactives_Specifications_upper_name_index";
CREATE INDEX IF NOT EXISTS "Reactives_Specifications_upper_name_index" ON "Reactives_Specifications" (
	"upper_name"	ASC
);
DROP INDEX IF EXISTS "Users_Groups_Id_index";
CREATE INDEX IF NOT EXISTS "Users_Groups_Id_index" ON "Users_Groups" (
	"Id"	ASC
);
DROP INDEX IF EXISTS "Users_name_index";
CREATE INDEX IF NOT EXISTS "Users_name_index" ON "Users" (
	"name"	ASC
);
COMMIT;
